package persistencia;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.BufferedWriter;

public class CtrlHidato {
	
	public CtrlHidato() {
	}
	
	
	/*Guarda Hidatos proposats per l'usuari si no existeixen
	 */
	public void guardarHidato(String tipus,String topo,String adja,String id,String nomH,String nomAutor,int [][]tablero_original,int [][] tablero_mod){
		try {
			File aux  = new File("HidatosCreats");
			if (!aux.exists()) {
				aux.mkdirs();
			}
			else aux.delete();
			File aux2 = new File("HidatosCreats/" + nomH + ".txt");
			if (!aux2.exists()){
				aux2.createNewFile();
				FileWriter esc = new FileWriter("HidatosCreats/" + nomH + ".txt",true);
				BufferedWriter bw = new BufferedWriter(esc);
				bw.write(nomH);
				bw.append(' ');
			    bw.append(nomAutor);
			    bw.append(' ');
			    bw.append(id);
			    bw.newLine();
			    bw.append(' ');
			    bw.write("0");
			    bw.append(' ');
			    bw.write(tipus);
				bw.append(' ');
			    bw.append(adja);
			    bw.append(' ');
			    bw.append(topo);
			    bw.append(' ');
			    String nfiles = String.valueOf(tablero_original.length);
			    bw.append(nfiles);
			    bw.append(' ');
			    String ncol = String.valueOf(tablero_original[0].length);
			    bw.append(ncol);
			    
			    for(int i = 0; i < tablero_original.length; ++ i){
			    	bw.append(' ');
			    	bw.newLine();
			    	for(int j = 0; j < tablero_original[i].length; ++j){
			    		if(!((j+1) == tablero_original[i].length)) bw.write(tablero_original[i][j] + " ");
			    		else bw.write(tablero_original[i][j] + "");
			    		System.out.println(tablero_original[i][j]);			    	
			    		}
			    }
			    
			    bw.newLine();
			    
			    for(int i = 0; i < tablero_mod.length; ++ i){
			    	bw.append(' ');
			    	bw.newLine();
			    	for(int j = 0; j < tablero_mod[i].length; ++j){
			    		if(!((j+1) == tablero_mod[i].length)) bw.write(tablero_mod[i][j] + " ");
			    		else bw.write(tablero_mod[i][j] + "");
			    		System.out.println(tablero_mod[i][j]);			    	
			    		}
			    }
			    bw.flush();
				bw.close();
				esc.close();
			}
			
	} catch(IOException ex) {
		
		}
	}
		
	/*Guarda Partides  de l'usuari que ha creat durant la sessi�.
	 */
	public void guardarPartida(int temps,String tipus,String topo,String adja,String id,String nomH,String nomAutor,int [][]tablero_original,int [][] tablero_mod){
		try {
			File aux  = new File("PartidesGuardades");
			if (!aux.exists()) {
				aux.mkdirs();
			}
			else aux.delete();
			
			File aux3  = new File("PartidesGuardades/" + nomAutor);
			if (!aux3.exists()) {
				aux3.mkdirs();
			}
			File aux2 = new File("PartidesGuardades/" + nomAutor + "/" + id +".txt");
			if (!aux2.exists()){
				aux2.createNewFile();
				FileWriter esc = new FileWriter("PartidesGuardades/" + nomAutor + "/" + id +".txt",true);
				BufferedWriter bw = new BufferedWriter(esc);
				bw.write(nomH);
				bw.append(' ');
			    bw.append(nomAutor);
			    bw.append(' ');
			    bw.append(id);
			    bw.newLine();
			    bw.append(' ');
			    bw.append(String.valueOf(temps));
				bw.append(' ');
			    bw.append(tipus);
				bw.append(' ');
			    bw.append(adja);
			    bw.append(' ');
			    bw.append(topo);
			    bw.append(' ');
			    String nfiles = String.valueOf(tablero_original.length);
			    bw.append(nfiles);
			    bw.append(' ');
			    String ncol = String.valueOf(tablero_original[0].length);
			    bw.append(ncol);
			    
			    for(int i = 0; i < tablero_original.length; ++ i){
			    	bw.append(' ');
			    	bw.newLine();
			    	for(int j = 0; j < tablero_original[i].length; ++j){
			    		if(!((j+1) == tablero_original[i].length)) bw.write(tablero_original[i][j] + " ");
			    		else bw.write(tablero_original[i][j] + "");
			    		System.out.println(tablero_original[i][j]);			    	
			    		}
			    }
			    
			    bw.newLine();
			    
			    for(int i = 0; i < tablero_mod.length; ++ i){
			    	bw.append(' ');
			    	bw.newLine();
			    	for(int j = 0; j < tablero_mod[i].length; ++j){
			    		if(!((j+1) == tablero_mod[i].length)) bw.write(tablero_mod[i][j] + " ");
			    		else bw.write(tablero_mod[i][j] + "");
			    		System.out.println(tablero_mod[i][j]);			    	
			    		}
			    }
			    bw.flush();
				bw.close();
				esc.close();
			}
			
	} catch(IOException ex) {
		
		}
	}
	
	/*
	 * Carga tots els Hidatos Creats
	 * Retorna un ArrayList amb tots els hidatos Creats
	 */
	public ArrayList<String> cargarHidatosCreats(){
		ArrayList<String> Hidato = new ArrayList<String>();
		try{
			File folder = new File("HidatosCreats");
			if (folder.exists()){
				File[] listOfFiles = folder.listFiles();
		
				for (File file : listOfFiles) {
				    if (file.isFile()) {
				        System.out.println(file.getName());
				        BufferedReader br = new BufferedReader(new FileReader(file.getAbsolutePath()));
				        StringBuilder sb = new StringBuilder();
				        String line = br.readLine();
	
				        while (line != null) {
				            sb.append(line);
				            line = br.readLine();
				        }
				        String hid = sb.toString();
				        System.out.println(hid);
				        Hidato.add(hid);
				        br.close();
				    }
				}
		    }
		}
		catch(IOException ex) {
			
		}   
		return Hidato;
	}
	
	public ArrayList<String> cargarPartidesGuardades(String usuari){
		ArrayList<String> Hidato = new ArrayList<String>();
		try{
			File folder = new File("PartidesGuardades/" + usuari);
			if (folder.exists()){
				File[] listOfFiles = folder.listFiles();
		
				for (File file : listOfFiles) {
				    if (file.isFile()) {
				        System.out.println(file.getName());
				        BufferedReader br = new BufferedReader(new FileReader(file.getAbsolutePath()));
				        StringBuilder sb = new StringBuilder();
				        String line = br.readLine();
	
				        while (line != null) {
				            sb.append(line);
				            line = br.readLine();
				        }
				        String hid = sb.toString();
				        System.out.println(hid);
				        Hidato.add(hid);
				        br.close();
				    }
				}
		    }
		}
		catch(IOException ex) {
			
		}   
		return Hidato;
	}
}
