package persistencia;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class CtrlRanking {
	
	public CtrlRanking(){};

	
	/*
	 * Guarda els tres tipus de Ranking
	 */
	public void guardarRanking(ArrayList<String> facil, ArrayList<String> mitja,ArrayList<String> dificil){
		try {
			File aux  = new File("Ranking");
			if (!aux.exists()) {
				aux.mkdirs();
			}
			File fitfacil = new File("Ranking/facil.txt");
			if(fitfacil.exists()) fitfacil.delete();
			File fitmitja = new File("Ranking/mitja.txt");
			if(fitmitja.exists()) fitmitja.delete();
			File fitdificil = new File("Ranking/dificil.txt");
			if(fitdificil.exists()) fitdificil.delete();
			
			fitfacil.createNewFile();
			fitmitja.createNewFile();
			fitdificil.createNewFile();
			FileWriter esc = new FileWriter("Ranking/facil.txt",true);
			BufferedWriter bw = new BufferedWriter(esc);
			for(String i : facil){
				bw.write(i);
				bw.newLine();
				bw.append(" ");
			}
			bw.flush();
			
			FileWriter esc1 = new FileWriter("Ranking/mitja.txt",true);
			BufferedWriter bw2 = new BufferedWriter(esc1);
			for(String i : mitja){
				bw2.write(i);
				bw2.newLine();
				bw2.append(" ");
			}
			bw2.flush();
			
			FileWriter esc3= new FileWriter("Ranking/dificil.txt",true);
			BufferedWriter bw3 = new BufferedWriter(esc3);
			for(String i : dificil){
				bw3.write(i);
				bw3.newLine();
				bw3.append(" ");
			}
			bw3.flush();
			
			esc3.close();
			bw3.close();
			bw.close();
			esc.close();
			esc1.close();
			bw2.close();
		} catch(IOException ex) {
			
		}
	}
	
	public ArrayList<String> CargarRanking(){
		ArrayList<String> rankings = new ArrayList<String>();
		try {
			String fa,mit,dif;
			fa = CargarRankingDEP(0);
			mit =  CargarRankingDEP(1);
			dif = CargarRankingDEP(2);
	        rankings.add(fa);
	        rankings.add(mit);
	        rankings.add(dif);
		} catch(Exception ex) {
			
		}	
		
		return rankings;
	}
	
	
	public String CargarRankingDEP(int dif){
		String fa = "";
		try {
			File fitfacil = new File("Ranking/facil.txt");
			File fitmitja = new File("Ranking/mitja.txt");
			File fitdificil = new File("Ranking/dificil.txt");
			BufferedReader br;
			if(dif == 0) br = new BufferedReader(new FileReader(fitfacil));
			else if (dif == 1) br = new BufferedReader(new FileReader(fitmitja));
			else  br = new BufferedReader(new FileReader(fitdificil));
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();
	        while (line != null) {
	            sb.append(line);
	            line = br.readLine();
	        }
	        fa = sb.toString();
	        
	        if(dif == 0) if(fitfacil.exists()) fitfacil.delete();
			else if (dif == 1) if(fitmitja.exists()) fitmitja.delete();
			else  if(fitdificil.exists()) fitdificil.delete();
	        br.close();
		} catch(IOException ex) {
		
		}	
		return fa;
	
	}
	
}
