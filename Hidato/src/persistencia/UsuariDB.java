package persistencia;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import domini.Hidato.PartidaGuardada;

public class UsuariDB {

	public UsuariDB() {
		
	}
	
	/*Crea un usuari nou a la base de dades.
	 * */
	public boolean insertUsuari(String username, String password) {
		boolean afegit = false;
		try {
			File aux = new File("Usuaris/" + username);
			if (!aux.exists()) {
				aux.mkdirs();
				File aux2 = new File("Usuaris/" + username + "/password.txt");
				aux2.createNewFile();
				FileWriter esc = new FileWriter("Usuaris/" + username + "/password.txt",true);
				BufferedWriter bw = new BufferedWriter(esc);
				bw.write(password);
				bw.close();
				esc.close();
				afegit = true;
			} 
		} catch(IOException ex) {
			
		}
		return afegit;
	}
	/*
	public boolean afegirPartidaAMitges(PartidaGuardada partida) {
		
		return true;
	}
	*/
	
	/*Comprova si l'usuari existeix i si el password es correcte.
	 * */
	public boolean identificar(String username, String password) {
		boolean correcte = false;
		try {
			File aux = new File("Usuaris/" + username);
			if (aux.exists()) {
				FileReader fr = new FileReader("Usuaris/" + username + "/password.txt");
				BufferedReader br = new BufferedReader(fr);
				String psw =br.readLine();
				br.close();
		        fr.close();
		        if (psw.equals(password)) {
		        	correcte = true;
		        };
			}
		} catch(IOException ex) {
			
		}
	
		return correcte;
	}
	
	public void changePsw(String username, String newpsw) {
		try {
			File aux2 = new File("Usuaris/" + username + "/password.txt");
			aux2.delete();
			aux2.createNewFile();
			FileWriter esc = new FileWriter("Usuaris/" + username + "/password.txt",true);
			BufferedWriter bw = new BufferedWriter(esc);
			bw.write(newpsw);
			bw.close();
			esc.close();
		} catch(IOException ex) {
			
		}
	}
}
