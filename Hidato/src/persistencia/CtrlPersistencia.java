package persistencia;

import java.util.ArrayList;

public class CtrlPersistencia {
	
	private UsuariDB udb;
	private CtrlHidato ctrlhidato;
	private CtrlRanking ctrlrank;

	
	public CtrlPersistencia() {
		udb = new UsuariDB();
		ctrlhidato = new CtrlHidato();
		ctrlrank = new CtrlRanking();
	}
	
	
	///////////////////////////Usuaris
	
	/*Crea un usuari nou a la base de dades.
	 * */
	public boolean insertUsuari(String username, String password) {
		return udb.insertUsuari(username, password);
	}
	
	/*Comprova si l'usuari existeix i si el password es correcte.
	 * */
	public boolean identificar(String username, String password) {
		return udb.identificar(username, password);
	}
	
	/*cambia el password del ususi pel newpsw
	 * */
	public void changePsw(String username, String newpsw) {
		udb.changePsw(username, newpsw);
	}
	
	public void guardarHidatos(String tipus,String topo,String adja,String id,String nomH,String nomAutor,int [][] tablero_original,int [][] tablero_mod){
		ctrlhidato.guardarHidato(tipus,topo,adja,id,nomH,nomAutor,tablero_original,tablero_mod);
	}
	
	public void guardarPartida(int temps,String tipus,String topo,String adja,String id,String nomH,String nomAutor,int [][] tablero_original,int [][] tablero_mod){
		ctrlhidato.guardarPartida(temps,tipus,topo,adja,id,nomH,nomAutor,tablero_original,tablero_mod);
	}
	
	public ArrayList<String> CargarHidatosCreats(){
		return ctrlhidato.cargarHidatosCreats();
	}
	
	public ArrayList<String> CargarPartidesGuardades(String usuari){
		return ctrlhidato.cargarPartidesGuardades(usuari);
	}
	
	public void GuardarRanking(ArrayList<String> facil, ArrayList<String> mitja,ArrayList<String> dificil){
		ctrlrank.guardarRanking(facil, mitja, dificil);
	}
	public ArrayList<String> CargarRankings(){
		return ctrlrank.CargarRanking();
	}
}
