package presentacio;



import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.event.WindowStateListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;




public class VistaPrincipal extends JFrame {


	private CtrlPresentacio iCtrlPresentacio;
	
	/**
	 * Create the frame.
	 */
	public VistaPrincipal(CtrlPresentacio pCtrlPresentacio) {
		
		iCtrlPresentacio = pCtrlPresentacio;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 910, 512 );
		setContentPane(new LoginPanel(iCtrlPresentacio, this));
		this.setVisible(true);
		this.setResizable(false);
		
		this.addWindowListener(new WindowListener() {

			@Override
			public void windowActivated(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowClosed(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowClosing(WindowEvent arg0) {
				// TODO Auto-generated method stub
				iCtrlPresentacio.GuardarHidatosProposats();
				//iCtrlPresentacio.CargarPartidesGuardades();
				iCtrlPresentacio.GuardarRanking();
			}

			@Override
			public void windowDeactivated(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowDeiconified(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowIconified(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowOpened(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});

	}
	
	
	
/////////////////////Canvis de panels
	public void changePanel(JPanel newP) {
		this.getContentPane().removeAll();;
		this.setContentPane(newP);
		this.validate(); 
	}
	
	public void changeToRegister() {
		this.getContentPane().removeAll();;
		this.setContentPane(new RegisterPanel(iCtrlPresentacio, this));
		this.validate(); 
	}

	public void changeToLogin() {
		this.getContentPane().removeAll();;
		this.setContentPane(new LoginPanel(iCtrlPresentacio, this));
		this.validate(); 
	}
	
	public void changeMenu() {
		this.getContentPane().removeAll();;
		this.setContentPane(new MenuPanel(iCtrlPresentacio, this));
		this.validate(); 
	}
	
	public void changeJugar() {
		this.getContentPane().removeAll();;
		this.setContentPane(new JugarPanel(iCtrlPresentacio, this));
		this.validate(); 
	}
	
	public void changeConfiguracio() {
		this.getContentPane().removeAll();;
		this.setContentPane(new ConfiguracionPanel(iCtrlPresentacio, this));
		this.validate(); 
	}
	
	public void changePswPanel() {
		this.getContentPane().removeAll();;
		this.setContentPane(new ChangePswPanel(iCtrlPresentacio, this));
		this.validate(); 
	}
	
	public void changeRankingPanel() {
		this.getContentPane().removeAll();;
		this.setContentPane(new RankingPanel(iCtrlPresentacio, this));
		this.validate(); 
	}
	
	public void changeInstruccionsPanel() {
		this.getContentPane().removeAll();;
		this.setContentPane(new InstruccionsPanel(iCtrlPresentacio, this));
		this.validate(); 
	}
	
	public void changeProposarHidatoPanel() {
		ProposarHidatoPanel p = new ProposarHidatoPanel(iCtrlPresentacio, this);
		this.getContentPane().removeAll();;
		this.setContentPane(p);
		this.validate();
		
		
		this.addWindowStateListener(new WindowStateListener() {

			@Override
			public void windowStateChanged(WindowEvent arg0) {
				// TODO Auto-generated method stub
				p.updatee();
			}
			
		});
		JOptionPane.showMessageDialog(this, "1) Selecciona els parametres desitjades amb els camps de la dreta. \n 2) Fica numeros consecutius amb el boto esquerre del raoli fent click sobre una cel�la. \n 3) Reseteja cel�les, fica numeros, forats, elimina cel�les amb el boto dret del ratoli sobre una cel�la.\n 4) Com a minim ha d'haver el primer i el ultim numero.");

	}
	
	public void changeJocRapidConf() {
		this.getContentPane().removeAll();;
		this.setContentPane(new JocRapidConfPanel(iCtrlPresentacio, this));
		this.validate(); 
	}
	
	public void changeJocRapid(int[][] board,String tc, String top, String adj, String dif,int[][] t_guardat) {
		this.getContentPane().removeAll();;
		this.setContentPane(new JocRapidPanel(iCtrlPresentacio, this, board, tc,top, adj, dif, t_guardat));
		this.validate();
		JOptionPane.showMessageDialog(this, "1) Selecciona els parametres desitjades amb els camps de la dreta. \n 2) Fica numeros consecutius amb el boto esquerre del raoli fent click sobre una cel�la. \n 3) Reseteja cel�les i fica numeros amb el boto dret del ratoli sobre una cel�la.");

	}
	
	public void changeJocPersConf() {
		this.getContentPane().removeAll();;
		this.setContentPane(new JocPersConfPanel(iCtrlPresentacio, this));
		this.validate(); 

	}
	
	public void changeJocPropConf() {
		this.getContentPane().removeAll();;
		this.setContentPane(new JocProposatConfPanel(iCtrlPresentacio, this));
		this.validate(); 

	}
	
	public void changeJocPGConf() {
		this.getContentPane().removeAll();;
		this.setContentPane(new JocPGConfPanel(iCtrlPresentacio, this));
		this.validate(); 
		JOptionPane.showMessageDialog(this, "Les partides guardades no contaran pel raking.");


	}
}
