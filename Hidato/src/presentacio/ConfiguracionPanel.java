package presentacio;

import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class ConfiguracionPanel extends JPanel {

	private CtrlPresentacio iCtrlPresentacio;
	private VistaPrincipal iVistaPrincipal;
	/**
	 * Create the panel.
	 */
	public ConfiguracionPanel(CtrlPresentacio pCtrlPresentacio, VistaPrincipal pVistaPrincipal) {
		iCtrlPresentacio = pCtrlPresentacio;
		iVistaPrincipal = pVistaPrincipal;
		setLayout(null);
		this.setPreferredSize(new Dimension(900, 512));
		
		JLabel lblConfiguracio = new JLabel("Configuracio");
		lblConfiguracio.setHorizontalAlignment(SwingConstants.CENTER);
		lblConfiguracio.setFont(new Font("Tahoma", Font.PLAIN, 36));
		lblConfiguracio.setBounds(10, 54, 890, 76);
		add(lblConfiguracio);
		
	
		
		JButton btnNewButton = new JButton("Tancar sessio");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				iCtrlPresentacio.GuardarHidatosProposats();
				iCtrlPresentacio.GuardarPartidesGuardades();
				iVistaPrincipal.changeToLogin();
				
			}
		});
		btnNewButton.setBounds(364, 229, 171, 23);
		add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Tornar enrere");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				iVistaPrincipal.changeMenu();
			}
		});
		btnNewButton_1.setBounds(366, 358, 168, 23);
		add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Modificar contrasenya");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				iVistaPrincipal.changePswPanel();
			}
		});
		btnNewButton_2.setBounds(364, 266, 171, 23);
		add(btnNewButton_2);
		

	}
}
