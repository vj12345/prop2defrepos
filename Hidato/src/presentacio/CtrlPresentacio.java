package presentacio;

import java.util.ArrayList;
import java.util.List;

import domini.Sessio.CtrlSessio;

public class CtrlPresentacio {
	
	private CtrlSessio ctrlSessio;
	
	private VistaPrincipal vistaPrincipal;
	
	
	
	
///////////////////////Constructor i inicialitzacio
	public CtrlPresentacio() {
		ctrlSessio = new CtrlSessio();
		
		vistaPrincipal = new VistaPrincipal(this);
	}

	
	
	
////////////////////////usuaris
	//Inicailitza el usuari
	public boolean login(String username, String password) {
		return ctrlSessio.login(username, password);
	}
	//crea un usuari nou
	public boolean register(String username, String password) {
		return ctrlSessio.newUsuari(username, password);
	}
	//cambia la contrasenya oldpsw del usuari actual pel newpsw
	public boolean changePsw(String oldpsw, String newpsw) {
		return ctrlSessio.changePsw(oldpsw, newpsw);
	}
	//retorna el nom del usuari
	public String getUsername() {
		return ctrlSessio.getUsername();
	}


///////////////Manejament ranking
	//Retorna el ranking segons el tipus 1, 2 o 3 (facil, mitja, dificil) en format de lista de una lista format per dos strings(nom usuari i temps),
	public List<List<String>> getRanking(int tipus) {

		return ctrlSessio.getRanking(tipus);
	}
	
///////////////////PROPOSAR HIDATO
	
	//retorna true si s'ha pogut generar el hidato amb els parametres passats
	public boolean proposarHidato(int[][] tauler, String nomHid, String adjacencia, String tCela, int files, int columnes) {
		return ctrlSessio.proposarHidato(tauler, nomHid, adjacencia, tCela, files, columnes);
	}
	
	//comprova si el nom nomh ja existeix en els hidatos creats
	public boolean NomHidatoUsable(String nomh) {
		return ctrlSessio.NomHidatoUsable(nomh);
	}
	
	//primer s'ha de fer proposarHidato
	//Guarda el hidato proposat a la llista de hdiatos proposats
	public boolean guardarHidatoProposat() {
		return ctrlSessio.guardarHidatoProposat();
	}
	
	//primer s'ha de fer proposar hidato
	//retorna el tauler original.
	public int[][] obtenirSolu() {
		return ctrlSessio.obtenirSolu();
	}
	
	//retorna el tauler amb la solucio del tauler original.
	public int[][] obtenirSolu2() {
		return ctrlSessio.obtenirSolu2();
	}
	
	
/////////////////////////JOc rapoid
	
	public int[][] crearJocRapid(String tCela, String top, String adj, String dif) {
		return ctrlSessio.crearJocRapid(tCela, top, adj, dif);
	}
	
	//Comprova si el hidato que esta en el board te solucio, return true si te solucio.
	public boolean comprovarCor(int[][] board) {
		return ctrlSessio.comprovarCor(board);
	}
	
	//Retorna la solucio del hidato que esta en el baord.
	public int[][] getSoluJoc(int[][] board) {
		return ctrlSessio.obtenirSoluJoc(board);
	}
	
	//guarda la partida del usuari, el tauler guardat sera el board.
	public void guardarPartida(String id, int tempsGastat, int[][] board) {
		ctrlSessio.guardarPartida(id, tempsGastat, board);
	}
	
	//Comen�a el timer
	public void startTimer() {
		ctrlSessio.startTimer();
	}
	//Para el timer.
	public void endTiemr() {
		ctrlSessio.endTiemr();
	}
	//Retorna el temps que ha pasat des de que es va come�ar el timer fins que es va parar.
	public int getTimeElapse() {
		return ctrlSessio.getTimeElapse();
	}
	
	//afegiex una entrada al ranking amb dificultat dif i temps sec.
	public void afegirRanking(int dif,  int sec) {
		String user = this.getUsername();
		ctrlSessio.afegirRanking(dif, user, sec);
	}
	
	//Comprova que el id amb el que es vol guardar la paritda no estigui repet.
	public boolean nomidPartidaGuardadaUsable(String id) {
		return ctrlSessio.nomidPartidaGuardadaUsable(id);
	}
	
	//////////////////////////////JOC PERSONALITZAT 
	public int[][] crearJocRapid(String tCela, String top, String adj, String nomHid, int nCeles, int forats, int defectes) {
		return ctrlSessio.crearJocRapid(tCela, top,  adj, nomHid, nCeles, forats,defectes);
	}


//JOC PROPOSAT

	//retorna un arraylist on conte n vegades la combinacio de nom hidato + nom autor 
		// es igual al primer valor.
	public List<String> getHidatosProposats() {
		// TODO Auto-generated method stub
		return ctrlSessio.getHidatosProposats();
	}
	public int[][] crearJocProposat(int index) {
		return ctrlSessio.crearJocProposat(index);
	}
	//reotrna el tipus de cela del hidato proposat que esta a la posicio po del arraylist amb els identificadors de les partides guardades.
	//HEXAGONAL, QUADRADA, TRIANGULAR
	public String getTipusCelaHidatoProposat(int index) {
		// TODO Auto-generated method stub
		return ctrlSessio.getTipusCelaHidatoProposat(index);
	}

	//reotrna el tipus d adjacencia del hidato proposat que esta a la posicio po del arraylist amb els identificadors de les partides guardades.
		//COSTAT, COSTAT I VERTEX
	public String getAdjHidatoProposat(int index) {
		// TODO Auto-generated method stub
		return ctrlSessio.getAdjHidatoProposat(index);
	}


	public ArrayList<String> getAutorsHidatosProposats() {
		// TODO Auto-generated method stub
		return ctrlSessio.getAutorsHidatosProposats();
	}

	//retorna un index amb el quan s'identifica el hidato.
	public int findPos(String nomHidato) {
		// TODO Auto-generated method stub
		return ctrlSessio.findPos(nomHidato);
	}

	//Borra la partida guardada amb el index index.
	public boolean borrarPartidaProposat(int index) {
		return ctrlSessio.borrarPartidaProposat(index);
		
	}
	
	
	/////////////////////////JOC PARTIDA GUARDADA
	

	public int[][] crearJocGuardat(int pos) {
		// TODO Auto-generated method stub
		return ctrlSessio.crearJocGuardat(pos);
	}

	//Retorna un arrayList amb els identificadors de les partides guardades.
	public ArrayList<String> getHidatosGuardats() {
		// TODO Auto-generated method stub
		return ctrlSessio.getHidatosGuardats();
	}

	//reotrna el tipus de cela del hidato guardat que esta a la posicio po del arraylist amb els identificadors de les partides guardades.
	//HEXAGONAL, QUADRADA, TRIANGULAR

	public String getTipusCelaHidatoGuardat(int pos) {
		// TODO Auto-generated method stub
		return ctrlSessio.getTipusCelaHidatoGuardat(pos);
	}


	//reotrna el tipus d adjacencia del hidato guardat que esta a la posicio po del arraylist amb els identificadors de les partides guardades.
	//COSTAT, COSTAT I VERTEX
	public String getAdjHidatoGuardat(int pos) {
		// TODO Auto-generated method stub
		return ctrlSessio.getAdjHidatoGuardat(pos);
	}

	//retorna el tauelr guardat del hidato guardat que s'ha creat en el crearJocGuardat
	public int[][] getTaulerGuardat() {
		// TODO Auto-generated method stub
		return ctrlSessio.getTaulerGuardat();

	}
	
	//borra el hidato guardat que esta a la posicio i del arraylist amb els identificadors de les partides guardades.
	public void borrarPartidaGuardada(int i) {
		ctrlSessio.borrarPartidaGuardada(i);
	}
	
	//////////////////////////PERSISTENCIA

	void GuardarHidatosProposats(){
		ctrlSessio.GuardarHidatosCreats();
	}
	void GuardarPartidesGuardades(){
		ctrlSessio.GuardarPartidesGuardades();
	}
	void CargarHidatosCreats(){
		ctrlSessio.CargarHidatosCreats();
	}
	
	void CargarPartidesGuardades(){
		ctrlSessio.CargarPartidesGuardades();
	}
	
	void GuardarRanking(){
		ctrlSessio.GuardarRanking();
	}
	void CargarRankings(){
		ctrlSessio.CargarRanking();
	}

	
}