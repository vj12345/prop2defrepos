package presentacio;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class QuadBoardPanel extends BoardPanel {


    private int costat;
    
    
	/**
	 * Create the panel.
	 */
	public QuadBoardPanel(int width, int height, int costat) {
		super(width, height, costat,costat);

        this.costat = costat*2;
	}
	
	public QuadBoardPanel(int[][]solu,int width, int height, int costat, JPanel parent, int[][] t_guardat) {
		super(solu,width, height, costat,costat, parent, t_guardat);

        this.costat = costat*2;
	}
	 @Override
	    protected void paintComponent(Graphics g) {
	        super.paintComponent(g);
	        Graphics2D g2d = (Graphics2D) g;
	        
	        for (int i = 0; i < width; i++) {
	            for (int j = 0; j < height; j++) {
	            	
	            	if (super.getBoard()[j][i] == -2) {
	            		Polygon aux = buildPolygon(i,j);
		                g2d.setColor(Color.BLACK);
		                g2d.fillPolygon(aux);
		                g2d.drawPolygon(aux);
		                
	            	}
	            	else if (super.getBoard()[j][i] == -3 && super.editor) {
	            		Polygon aux = buildPolygon(i,j);
		                g2d.setColor(Color.RED);
		                g2d.fillPolygon(aux);
		                g2d.drawPolygon(aux);
		                
	            	}
	            	else {
	            		if ((!super.editor && super.getBoard()[j][i] != -3) || super.editor) {
	            			if(super.ficatsPerDefecte.contains(super.getBoard()[j][i])) {
	            				Polygon aux = buildPolygon(i,j);
				                g2d.setColor(Color.GRAY);
				                g2d.fillPolygon(aux);
				                g2d.drawPolygon(aux);
	            			}
	            			else {
			            		Polygon aux = buildPolygon(i,j);
				                g2d.setColor(Color.orange);
				                g2d.fillPolygon(aux);
				                g2d.drawPolygon(aux);
	            			}
	            		}
		                
	            	}
	            	if ((!super.editor && super.getBoard()[j][i] != -3) || super.editor) {
		            	g2d.setStroke(new BasicStroke(2));
		            	Polygon p = buildPolygon(i,j);
		            	g2d.setColor(Color.CYAN);
		            	//g2d.fillPolygon(p);
		                g2d.drawPolygon(p);
	            	}
	            }
	        }
	    }
	    
	    public void paint(Graphics g) {
	    	super.paint(g);
	        Graphics2D g2d = (Graphics2D) g;
	     
	        g2d.setStroke(new BasicStroke(3));
	    	g2d.setColor(Color.GREEN);
	        g2d.drawPolygon(buildPolygon(a, b));
	        g2d.setColor(Color.BLACK);
	        //g2d.drawPolygon(buildHexagon(sa, sb));

	       
	    }


	    @Override
	    public Dimension getPreferredSize() {
	        int panelWidth =        width * costat;

	        int panelHeight =  height * costat;
	        return new Dimension(panelWidth, panelHeight);
	    }
	 
	    Polygon buildPolygon(int column, int row) {
	        Polygon quadrat = new Polygon();
	        Point origin = tileToPixel(column, row);
	      
	        quadrat.addPoint(origin.x, origin.y);
	        quadrat.addPoint(origin.x, origin.y + costat);
	        quadrat.addPoint(origin.x + costat, origin.y + costat);
	        quadrat.addPoint(origin.x + costat, origin.y);
	        
	      
	        return quadrat;
	    }
	 
	    Point tileToPixel(int column, int row) {
	        Point pixel = new Point();
	        pixel.x = (column * costat);
	        pixel.y = (row * costat);
	        return pixel;
	    }
	 
	    Point pixelToTile(int x, int y) {
	   
	        Point p = new Point(x / costat, y / costat);
	        Point r = new Point(x % costat, y % costat);
	        
	        return new Point(p.x, p.y);
	    }
	 
	  
	    
}
