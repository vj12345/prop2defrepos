package presentacio;

import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MenuPanel extends JPanel {
	
	private CtrlPresentacio iCtrlPresentacio;
	private VistaPrincipal iVistaPrincipal;

	/**
	 * Create the panel.
	 */
	public MenuPanel(CtrlPresentacio pCtrlPresentacio, VistaPrincipal pVistaPrincipal) {
		iCtrlPresentacio = pCtrlPresentacio;
		iVistaPrincipal = pVistaPrincipal;
		this.setPreferredSize(new Dimension(910, 512));
		setLayout(null);
		
		JLabel lblHidato = new JLabel("Hidato");
		lblHidato.setHorizontalAlignment(SwingConstants.CENTER);
		lblHidato.setFont(new Font("Tahoma", Font.PLAIN, 36));
		lblHidato.setBounds(10, 54, 890, 76);
		add(lblHidato);
		
		JButton btnNewButton = new JButton("Jugar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				iVistaPrincipal.changeJugar();
			}
		});
		btnNewButton.setBounds(374, 158, 161, 23);
		add(btnNewButton);
		
		JButton btnNewButton_3 = new JButton("Proposar Hidato");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				iVistaPrincipal.changeProposarHidatoPanel();

			}
		});
		btnNewButton_3.setBounds(374, 192, 161, 23);
		add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("Ranking");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				iVistaPrincipal.changeRankingPanel();
			}
		});
		btnNewButton_4.setBounds(374, 226, 161, 23);
		add(btnNewButton_4);
		
		JButton btnNewButton_5 = new JButton("Instruccions");
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				iVistaPrincipal.changeInstruccionsPanel();
			}
		});
		btnNewButton_5.setBounds(374, 260, 161, 23);
		add(btnNewButton_5);
		
		JButton btnNewButton_6 = new JButton("Configuracio");
		btnNewButton_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				iVistaPrincipal.changeConfiguracio();
			}
		});
		btnNewButton_6.setBounds(374, 294, 161, 23);
		add(btnNewButton_6);
		
		JButton btnNewButton_7 = new JButton("Sortir");
		btnNewButton_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				iCtrlPresentacio.GuardarHidatosProposats();
				iCtrlPresentacio.GuardarPartidesGuardades();
				iCtrlPresentacio.GuardarRanking();
				System.exit(0);
			}
		});
		btnNewButton_7.setBounds(374, 358, 161, 23);
		add(btnNewButton_7);
		
	}
}
