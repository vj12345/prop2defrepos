package presentacio;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public abstract class BoardPanel extends JPanel {
	public  int width; // Number of columns

    public int height; // Number of rows

    JPanel parent = null;

    boolean editor;
    
    //columna i fila en negreta
    int a = -3;
    int b = -3;
    /*
    //cela seleccionat de la lista
    int sa = -3;
    int sb = -3;
    */
    int actual = 1;
    
 
    private JLabel[][] num;
    int [][] board;
   
    ArrayList<Integer> numerosFicats;
    ArrayList<Integer> ficatsPerDefecte;
    ArrayList<Integer> borrats;
    
    ArrayList<Integer> ficatsUsuari;
    
    int offsetx;
    int offsety;
    JLabel view;
    
 
    public void setA(int a) {
		this.a = a;
	}

	public void setB(int b) {
		this.b = b;
	}

	/**
	 * Create the panel.
	 */
	//per proposar hidato
	public BoardPanel(int width, int height, int offsetx, int offsety) {
		editor = true;
		
		this.width = width;
        this.height = height;
        
		view = new JLabel();
		this.offsetx = offsetx;
		this.offsety = offsety;
		numerosFicats = new ArrayList<Integer>();
		ficatsPerDefecte = new ArrayList<Integer>();

		num = new JLabel[width][height];
		board = new int[height][width];
		
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[0].length; j++) {
				board[i][j] = -1;
			}
		}
		

        
      

       
	}
	
	public ArrayList<Integer> getFicatsPerDefecte() {
		return ficatsPerDefecte;
	}

	//per mostrar la solucio o per jugar
	public BoardPanel(int [][] solu, int width, int height, int offsetx, int offsety, JPanel parent, int[][] t_guardat) {
		editor = false;
		numerosFicats = new ArrayList<Integer>();
		ficatsPerDefecte = new ArrayList<Integer>();
		for(int i = 0 ; i < solu.length; i++) {
			for(int j = 0; j < solu[0].length; j++) {
				if (solu[i][j] != -1 && solu[i][j] != -2 && solu[i][j] != -3) {
					numerosFicats.add(solu[i][j]);
					ficatsPerDefecte.add(solu[i][j]);
				}
			}
		}
		
		this.width = width;
        this.height = height;
        
		view = new JLabel();
		this.offsetx = offsetx;
		this.offsety = offsety;
		
		num = new JLabel[width][height];
		board = new int[height][width];
		
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[0].length; j++) {
				board[i][j] = solu[i][j];
				if (t_guardat != null) {
					if (!numerosFicats.contains(t_guardat[i][j]) && t_guardat[i][j] != -1 && t_guardat[i][j] != -2 && t_guardat[i][j] != -3) {
						board[i][j] = t_guardat[i][j];
						numerosFicats.add(t_guardat[i][j]);
					}
				}
			}
		}
		
       this.parent = parent;
	}
	
	 @Override
	    protected void paintComponent(Graphics g) {
	        super.paintComponent(g);

	    }
	    
	    public void paint(Graphics g) {
	    	super.paint(g);
 
	    }


	    @Override
	    public abstract Dimension getPreferredSize();
	 
	  

		public int[][] getBoard() {
			return board;
		}

		abstract Polygon buildPolygon(int column, int row);
	 
	    abstract Point tileToPixel(int column, int row);
	 
	    abstract Point pixelToTile(int x, int y);
	 
	    public boolean tileIsWithinBoard(Point coordinates) {
	        int column = coordinates.x;
	        int row = coordinates.y;
	        return (column >= 0 && column < width) && (row >= 0 && row < height);
	    }

	    //afegeix un numero consecutiu a pos x, y del tauler si la pos x,y esta buida.
	    public void afegirNum(int x, int y ) {
	    	actual = 1;
	    	actualitzarNumActual();
	    	System.out.println("actual : " + actual);
	    	if (board[y][x] == -1) {
		    	Point cor = tileToPixel(x, y);
		    	JLabel n;
		    
		    		n = new JLabel(Integer.toString(actual));
		    		board[y][x] = actual;
			  


		    	n.setBounds(cor.x+offsetx, cor.y+offsety, 20, 20);
		    	n.setFont(new Font("Tahoma", Font.PLAIN, 16));
		    	
		    	num[x][y] = n;

		    	this.add(n);
		    		
		    	System.out.println("num ficat: " + actual);
		   
		    	numerosFicats.add(actual);
		    	//mapejat[x][y] = true;
	    	}
	    }
	    
	    public void ficarLabels() {
	    	for(int i = 0; i < board.length; i++) {
	    		for (int j = 0 ; j < board[0].length; j++) {
	    			if (board[i][j] != -1 && board[i][j] != -2 && board[i][j] != -3) {
	    				JLabel n = new JLabel(Integer.toString(board[i][j]));
	    				Point cor = tileToPixel(j, i);
	    				n.setBounds(cor.x+offsetx, cor.y+offsety, 20, 20);
	    		    	n.setFont(new Font("Tahoma", Font.PLAIN, 16));
	    		    	num[j][i] = n;

	    		    	this.add(n);
	    		    		
	    				
	    			}
	    		}
	    	}
	    }
	    //fa reset de la cela
	    public void borrarNum(int x, int y) {
	    	if (board[y][x] != -1 && board[y][x] != -2 && board[y][x] != -3) {
	    		if (numerosFicats.contains(board[y][x])) {
	    			numerosFicats.remove((Integer)board[y][x]);
	    			System.out.println("board222: " + board[y][x]);

	    			this.remove(num[x][y]);
	    			actual = 1;
		    		actualitzarNumActual();
		    		
	    		}
	    		
	    		
	    	//	mapejat[x][y] = false;
	    	}
	    	board[y][x] = -1;
    		
	    }
	    
	    //retorna true si s'ha pogut agegir el numero en la cela x,y.
	    public void ficarNumPersonalitzat(int x, int y, int numero) {
	    	if (ficatsPerDefecte.contains(numero)) {
	    		JOptionPane.showMessageDialog(parent, "No es pot ficar un numero igual a un numero donat per defecte.", "ERROR", JOptionPane.ERROR_MESSAGE);
	    	}
	    	else {
		    	if (board[y][x] != -1 && board[y][x] != -2 && board[y][x] != -3) {
		    		borrarNum( x,  y);
		    	}
		    	
		    		numerosFicats.add(numero);
		    		
		    		actual = 1;
		    		actualitzarNumActual();
		    		
			    	Point cor = tileToPixel(x, y);
			    	JLabel n;
	
			    		n = new JLabel(Integer.toString(numero));
			    		board[y][x] = numero;
				
	
	
			    	n.setBounds(cor.x+offsetx, cor.y+offsety, 20, 20);
			    	n.setFont(new Font("Tahoma", Font.PLAIN, 16));
			    	
			    	num[x][y] = n;
	
			    	this.add(n);
			    	
			    	//mapejat[x][y] = true;
	    	}
	    	
	    }
	    
	    private void actualitzarNumActual() {
	    	if (conteActual()) {
	    	
	    		actual++;
	    		actualitzarNumActual();
	    	}
	    }
	    private boolean conteActual() {
	    	for (int i = 0; i < numerosFicats.size(); i++) {
	    		if (numerosFicats.get(i) == actual) {
	    			return true;
	    		}
	    	}
	    	return false;
	    }
	    
	    public void preview(int x, int y) {
	    	actual = 1;
	    	actualitzarNumActual();
	    	this.remove(view);
	    	if (tileIsWithinBoard(new Point(x,y))) {
		    	view = new JLabel();
		    	if (board[y][x] == -1) {
		    		
		    			view.setText(Integer.toString(actual));
		    		
		    	
		    		view.setEnabled(false);
		    		Point cor = tileToPixel(x, y);
		    		view.setBounds((int)cor.x+offsetx, (int)cor.y+offsety, 20, 20);
			    	view.setFont(new Font("Tahoma", Font.PLAIN, 16));
			    	this.add(view);
		    	}
	    	}
	    }
	    
	    
	    public void ficarForat(int x, int y) {
	    	borrarNum(x,y);
	    	board[y][x] = -2;
	    }
 
	    public void eliminarCela(int x, int y) {
	    	borrarNum(x,y);
	    	board[y][x] = -3;
	    }
	    
	    
	  //comprova si un string es un integer
		public boolean isInteger(String s) {
			try {
				int x = Integer.parseInt(s);
				if (x <= 0) {
					return false;
				}
			} catch(NumberFormatException e) {
				return false;
			} catch (NullPointerException e) {
				return false;
			}
			return true;
		}
		
		//comprova si el board esta tot ple
		public boolean totPle() {
			for (int i  = 0; i < board.length; i++) {
				for (int j = 0 ; j < board[0].length; j++) {
					if (board[i][j] == -1) {
						return false;
					}
				}
			}
			return true;
		}
		
}
