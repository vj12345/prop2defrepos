package presentacio;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.SwingConstants;

public class JocRapidConfPanel extends JPanel {
	private CtrlPresentacio iCtrlPresentacio;
	private VistaPrincipal iVistaPrincipal;
	
	String tipusCela;
	String top;
	JPanel auto;
	/**
	 * Create the panel.
	 */
	public JocRapidConfPanel(CtrlPresentacio pCtrlPresentacio, VistaPrincipal pVistaPrincipal) {
		iCtrlPresentacio = pCtrlPresentacio;
		iVistaPrincipal = pVistaPrincipal;
		auto = this;
		
		this.setPreferredSize(new Dimension(910, 512));
		setLayout(null);
		
		JLabel lblTipusDeCela = new JLabel("Tipus de cela :");
		lblTipusDeCela.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTipusDeCela.setBounds(327, 174, 92, 28);
		add(lblTipusDeCela);
		
		JLabel lblAdjacencia = new JLabel("Adjacencia :");
		lblAdjacencia.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblAdjacencia.setBounds(327, 218, 92, 28);
		add(lblAdjacencia);
		
		JLabel lblFiles = new JLabel("Topologia :");
		lblFiles.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblFiles.setBounds(327, 266, 92, 28);
		add(lblFiles);
		
		JLabel lblColumnes = new JLabel("Dificultat :");
		lblColumnes.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblColumnes.setBounds(327, 305, 92, 28);
		add(lblColumnes);
	
	
		JComboBox<String> comboBox_2 = new JComboBox<String>();
		comboBox_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (comboBox_2.getSelectedItem().equals("HEXAGONAL")) {
					
					
					tipusCela = "HEXAGONAL";
					
					
				
				}
				else if (comboBox_2.getSelectedItem().equals("QUADRADA")) {
					tipusCela = "QUADRADA";
				
					
				}
				else if (comboBox_2.getSelectedItem().equals("TRIANGULAR")) {
					tipusCela = "TRIANGULAR";
				
				}
			}
		});
		comboBox_2.addItem("TRIANGULAR");
		comboBox_2.addItem("QUADRADA");
		comboBox_2.addItem("HEXAGONAL");
		comboBox_2.setBounds(476, 180, 121, 20);
		add(comboBox_2);
		
		JComboBox<String> comboBox_1 = new JComboBox<String>();
		comboBox_1.addItem("COSTAT");
		comboBox_1.addItem("COSTAT I VERTEX");
		comboBox_1.setBounds(476, 224, 121, 20);
		add(comboBox_1);
		
		
		JComboBox<String> comboBox_top = new JComboBox<String>();
		comboBox_top.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (comboBox_top.getSelectedItem().equals("QUADRADA")) {
					top = "QUADRADA";
				
					
				}
				else if (comboBox_top.getSelectedItem().equals("TRIANGULAR")) {
					top = "TRIANGULAR";
				
				}
			}
		});
		comboBox_top.addItem("TRIANGULAR");
		comboBox_top.addItem("QUADRADA");
		comboBox_top.setBounds(476, 272, 121, 20);
		add(comboBox_top);
		
		
		JComboBox<String> comboBox_dif = new JComboBox<String>();
		comboBox_dif.addItem("FACIL");
		comboBox_dif.addItem("MITJA");
		comboBox_dif.addItem("DIFICIL");
		comboBox_dif.setBounds(476, 311, 121, 20);
		add(comboBox_dif);
		
		JButton btnComenar = new JButton("Comen\u00E7ar");
		btnComenar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tipusCela = (String) comboBox_2.getSelectedItem();
				top = (String) comboBox_top.getSelectedItem();
				
				int[][] tauler = null;
				if (tipusCela.equals("TRIANGULAR")) {
					if (!top.equals("TRIANGULAR")) {
						JOptionPane.showMessageDialog(auto, "Per cel�les triangulars nomes esta disponible la topologai triangular", "Error", JOptionPane.ERROR_MESSAGE);
					}
					else {
						tauler = iCtrlPresentacio.crearJocRapid(tipusCela, top, (String)comboBox_1.getSelectedItem(), (String)comboBox_dif.getSelectedItem());
						for(int i = 0 ; i < tauler.length; i++) {
							for (int j = 0 ; j < tauler[0].length; j++) {
								System.out.print(tauler[i][j] + " ");
							}
							System.out.println();

						}
						
						iVistaPrincipal.changeJocRapid(tauler, tipusCela ,top, (String)comboBox_1.getSelectedItem(), (String)comboBox_dif.getSelectedItem(), null);
						
					}
					
				}
				else if (tipusCela.equals("QUADRADA")) {
					if (!top.equals("QUADRADA")) {
						JOptionPane.showMessageDialog(auto, "Per cel�les quadrades nomes esta disponible la topologai quadrada", "Error", JOptionPane.ERROR_MESSAGE);
					}
					else {
						tauler = iCtrlPresentacio.crearJocRapid(tipusCela, top, (String)comboBox_1.getSelectedItem(), (String)comboBox_dif.getSelectedItem());
						for(int i = 0 ; i < tauler.length; i++) {
							for (int j = 0 ; j < tauler[0].length; j++) {
								System.out.print(tauler[i][j] + " ");
							}
							System.out.println();

						}
						
						iVistaPrincipal.changeJocRapid(tauler, tipusCela ,top, (String)comboBox_1.getSelectedItem(), (String)comboBox_dif.getSelectedItem(), null);
						
					}
					
				}
				else {
					if (!top.equals("QUADRADA")) {
						JOptionPane.showMessageDialog(auto, "Per cel�les hexagonals nomes esta disponible la topologai quadrada", "Error", JOptionPane.ERROR_MESSAGE);
					}
					else {
						tauler = iCtrlPresentacio.crearJocRapid(tipusCela, top, (String)comboBox_1.getSelectedItem(), (String)comboBox_dif.getSelectedItem());
						for(int i = 0 ; i < tauler.length; i++) {
							for (int j = 0 ; j < tauler[0].length; j++) {
								System.out.print(tauler[i][j] + " ");
							}
							System.out.println();

						}
						
						iVistaPrincipal.changeJocRapid(tauler, tipusCela ,top, (String)comboBox_1.getSelectedItem(), (String)comboBox_dif.getSelectedItem(), null);
						
					}
					
				}
	
			}
		});
		btnComenar.setBounds(476, 385, 121, 23);
		add(btnComenar);
		
		JButton btnSortir = new JButton("Sortir");
		btnSortir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				iVistaPrincipal.changeJugar();
			}
		});
		btnSortir.setBounds(327, 385, 121, 23);
		add(btnSortir);
		
		JLabel lblConfiguracioJocRapid = new JLabel("Configuracio joc rapid");
		lblConfiguracioJocRapid.setHorizontalAlignment(SwingConstants.CENTER);
		lblConfiguracioJocRapid.setFont(new Font("Tahoma", Font.PLAIN, 28));
		lblConfiguracioJocRapid.setBounds(271, 53, 368, 69);
		add(lblConfiguracioJocRapid);
	}
}
