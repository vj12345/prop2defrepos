package presentacio;

import java.awt.Dimension;
import java.awt.Point;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.JButton;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.ActionEvent;

public class JocRapidPanel extends JPanel {
	private CtrlPresentacio iCtrlPresentacio;
	private VistaPrincipal iVistaPrincipal;
	
	JPanel auto;
	
	int dificultat;
	String dif;
	
	Point pantL;
    Point pantR;
    
    boolean solucioMostrat;
    int[][]boardori;
    
    BoardPanel solu;
	/**
	 * Create the panel.
	 */
	public JocRapidPanel(CtrlPresentacio pCtrlPresentacio, VistaPrincipal pVistaPrincipal, int[][] board, String tipusCela ,String top, String adj, String dif, int[][]t_guardat) {
		auto = this;
		solucioMostrat = false;
		this.dif = dif;
		this.boardori = board;
		System.out.println("DIFICULTAT: " + dif);
		if (dif.equals("FACIL")) {
			dificultat = 0;
		}
		else if (dif.equals("MITJA")) {
			dificultat = 1;
		}
		else if (dif.equals("NONE")) {
			dificultat = -1;
		}
		else {
			dificultat = 2;
		}
		
		iCtrlPresentacio = pCtrlPresentacio;
		iVistaPrincipal = pVistaPrincipal;
		this.setPreferredSize(new Dimension(910, 512));
		setLayout(null);
		
		JButton btnSortirSenseGuardar = new JButton("Sortir sense guardar");
		btnSortirSenseGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int resp = JOptionPane.showConfirmDialog(auto, "Estas segur de sortir?\n(Es perdra el hidato proposat fins ara.)", "Missatge de confirmacio", JOptionPane.YES_NO_OPTION);
				if (resp == JOptionPane.YES_OPTION) {
					iVistaPrincipal.changeMenu();
				}
			}
		});
		btnSortirSenseGuardar.setBounds(598, 105, 148, 23);
		add(btnSortirSenseGuardar);
		
		JButton btnGuardarISortir = new JButton("Guardar i sortir");
		btnGuardarISortir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String respuesta = JOptionPane.showInputDialog(auto, "Introduiex un identificador per identificar el hidato (min. 3 caracters, les partides guardades no contaran pel ranking) : ", " ",JOptionPane.INFORMATION_MESSAGE);

				if (respuesta != null) {
					if (respuesta.length() < 3) {
						JOptionPane.showMessageDialog(auto, "Com a minim ha de tenr 3 caracters.", "ERROR", JOptionPane.ERROR_MESSAGE);
					}
					else if (iCtrlPresentacio.nomidPartidaGuardadaUsable(respuesta))  {
						iCtrlPresentacio.endTiemr();
						int el = iCtrlPresentacio.getTimeElapse();
						
						iCtrlPresentacio.guardarPartida(respuesta, el, solu.getBoard());
						iCtrlPresentacio.GuardarPartidesGuardades();
						JOptionPane.showMessageDialog(auto, "Hidato guardat correctament amb l'id: " + respuesta, "INFORMACIO", JOptionPane.INFORMATION_MESSAGE);
						iVistaPrincipal.changeMenu();
					}
					else {
						JOptionPane.showMessageDialog(auto, "Ja tens una partida guardada amb aquest identificador, intenta guardar amb un altre identificador.", "ERROR", JOptionPane.ERROR_MESSAGE);

					}
				}
			}
		});
		btnGuardarISortir.setBounds(598, 184, 148, 23);
		add(btnGuardarISortir);
		
		JButton btnAjuda = new JButton("Ajuda");
		btnAjuda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				help();
			}
		});
		btnAjuda.setBounds(598, 255, 148, 23);
		add(btnAjuda);
		
		JButton btnAjuda2 = new JButton("Mostrar solucio");
		btnAjuda2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] opts = {"Acceptar", "Cancelar"};
				int sel = JOptionPane.showOptionDialog(auto, "Estas segur de voler veure la solucio?\n En cas afirmatiu la puntuacio no es contara pel ranking.", "Informacio", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, opts, opts[0]);
				if (sel == 0) {
					BoardPanel sol;
					solucioMostrat = true;
					if (tipusCela.equals("HEXAGONAL")) {
						sol = new HexBoardPanel(boardori, boardori[0].length, boardori.length, 30, auto, iCtrlPresentacio.getSoluJoc(boardori));
						
					}
					else if (tipusCela.equals("QUADRADA")) {
						sol = new QuadBoardPanel(boardori,  boardori[0].length, boardori.length, 30, auto,  iCtrlPresentacio.getSoluJoc(boardori));

					}
					else {
						sol = new TriBoardPanel(boardori,  boardori[0].length, boardori.length, 30, auto,  iCtrlPresentacio.getSoluJoc(boardori));

					}
					
					sol.setBounds(83, 122,  sol.getPreferredSize().width,sol.getPreferredSize().height);
					auto.add(sol);
					sol.setLayout(null);
					sol.ficarLabels();
					sol.repaint();
					String[] opt = {"Acceptar"};
					int sela = JOptionPane.showOptionDialog(auto, sol, "Solucio", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, opt, opt[0]);
			
				}
			}
		});
		btnAjuda2.setBounds(598, 326, 148, 23);
		add(btnAjuda2);
		
		
		if (tipusCela.equals("HEXAGONAL")) {
			solu = new HexBoardPanel(board, board[0].length, board.length, 30, this, t_guardat);
		}
		else if (tipusCela.equals("QUADRADA")) {
			solu = new QuadBoardPanel(board, board[0].length, board.length, 30, this, t_guardat);

		}
		else {
			solu = new TriBoardPanel(board, board[0].length, board.length, 30, this, t_guardat);

		}
		solu.setBounds(83, 62,  solu.getPreferredSize().width,solu.getPreferredSize().height);
		add(solu);
		solu.setLayout(null);
		solu.ficarLabels();
		solu.repaint();
		
		//comen�ar el contador
		iCtrlPresentacio.startTimer();
		
		
		
	    solu.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mousePressed(MouseEvent me) {
				// TODO Auto-generated method stub
				if (SwingUtilities.isLeftMouseButton(me)) {
					pantL = solu.pixelToTile(me.getX(), me.getY());

				}
				else if (SwingUtilities.isRightMouseButton(me)) {
					pantR = solu.pixelToTile(me.getX(), me.getY());
				}
			}

			@Override
			public void mouseReleased(MouseEvent me) {
				// TODO Auto-generated method stub
				if (SwingUtilities.isLeftMouseButton(me)) {
					Point pact = solu.pixelToTile(me.getX(), me.getY());
					if (pantL.x == pact.x && pantL.y == pact.y) {
						if (solu.tileIsWithinBoard(pact)) {
							   
							if (board[pact.y][pact.x] == -1) {
								solu.afegirNum(pact.x, pact.y);
								comprovaFinish();
								}
			        		
			        	}
			        	
					}
				}
				else if (SwingUtilities.isRightMouseButton(me)) {
					Point pact2 = solu.pixelToTile(me.getX(), me.getY());
					if (pantR.x == pact2.x && pantR.y == pact2.y) {
						if (solu.tileIsWithinBoard(pact2)) {
							//solu.borrarNum(pact2.x, pact2.y);
							if (board[pact2.y][pact2.x] == -1) {
							accioClickDret(pact2.x, pact2.y);
							comprovaFinish();
							}

						}
					}
				}
				repaint();
			}
			
		});
		solu.addMouseMotionListener(new MouseMotionListener() {

			@Override
			public void mouseDragged(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseMoved(MouseEvent me) {
				// TODO Auto-generated method stub
				Point tileCoordinates = solu.pixelToTile(me.getX(), me.getY());
				int x = tileCoordinates.x;
				int y = tileCoordinates.y;
	            if (solu.tileIsWithinBoard(tileCoordinates) && board[y][x] == -1) {
	            	solu.setA(tileCoordinates.x);
	            	solu.setB(tileCoordinates.y); 
	            	
	            	solu.preview(tileCoordinates.x, tileCoordinates.y);
	            } else {
	            	solu.setA(-3);
	            	solu.setB(-3); 
	            	
	            	
	            }
	            
	        	

	        	repaint();

			}
			
		});
	}
	
	public void accioClickDret(int tilex, int tiley) {
		String[] options = {"Resetejar cel�la", "Ficar Numero", "Cancelar"};
		int seleccio = JOptionPane.showOptionDialog(this, "Tria l'acci� que vols efectuar sobre la cel�la :", "Selecci� d'acci�", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
		if (seleccio == 0) {
		
				solu.borrarNum(tilex, tiley);

			
			
		}
		else if (seleccio == 1)
		{
			
		
		
				String respuesta = JOptionPane.showInputDialog(this, "Introduiex el numero que vols ficar : ", " ",JOptionPane.INFORMATION_MESSAGE);
				if (solu.isInteger(respuesta)) {
			
						solu.ficarNumPersonalitzat(tilex, tiley, Integer.parseInt(respuesta));
	
					
				}
				else if (respuesta != null) {
					JOptionPane.showMessageDialog(this, "Nomes es pot ficar numeros enters.", "Error", JOptionPane.WARNING_MESSAGE);
				}
			
		}

	}
	
	//comprova si ha omplert tot, si esta tot omplert i correcte s'acaba la partida, sino no fa res
	private void comprovaFinish() {
		if (solu.totPle()) {
			if (iCtrlPresentacio.comprovarCor(solu.getBoard())) {
				iCtrlPresentacio.endTiemr();
				Integer el = iCtrlPresentacio.getTimeElapse();
				if (dificultat != -1 && !solucioMostrat) {
				iCtrlPresentacio.afegirRanking(dificultat, el);
				JOptionPane.showMessageDialog(this, "Felicitats, has resolt el hidato de dificultat " + dif + ".\n\nHas tardat: "+ el.toString() + " segons.", " ", JOptionPane.INFORMATION_MESSAGE);

				}
				else {
				JOptionPane.showMessageDialog(this, "Felicitats, has resolt el hidato de dificultat " + dif + ".\n\nHas tardat: "+ el.toString() + " segons." + "\nAquest temps no es tindra em compte pel ranking.", " ", JOptionPane.INFORMATION_MESSAGE);
				}
				iVistaPrincipal.changeMenu();

			}
		}
	}
	
	
	private void help() {
		if (iCtrlPresentacio.comprovarCor(solu.getBoard())) {
			
			JOptionPane.showMessageDialog(this, "El que has omplert fins ara esta tot correcte.", " ", JOptionPane.INFORMATION_MESSAGE);

		}
		else {
			JOptionPane.showMessageDialog(this, "El que has omplert fins ara no es del tot correcte", " ", JOptionPane.ERROR_MESSAGE);

		}
	}
	
}
