package presentacio;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;

public class JugarPanel extends JPanel {
	
	private CtrlPresentacio iCtrlPresentacio;
	private VistaPrincipal iVistaPrincipal;

	/**
	 * Create the panel.
	 */
	public JugarPanel(CtrlPresentacio pCtrlPresentacio, VistaPrincipal pVistaPrincipal) {
		iCtrlPresentacio = pCtrlPresentacio;
		iVistaPrincipal = pVistaPrincipal;
		setLayout(null);
		this.setPreferredSize(new Dimension(910, 512));
		
		JButton btnNewButton = new JButton("Joc rapid");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				iVistaPrincipal.changeJocRapidConf();
			}
		});
		btnNewButton.setBounds(374, 162, 161, 23);
		add(btnNewButton);
		
		JButton btnJocPersonalitzat = new JButton("Joc personalitzat");
		btnJocPersonalitzat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				iVistaPrincipal.changeJocPersConf();
			}
		});
		btnJocPersonalitzat.setBounds(374, 199, 161, 23);
		add(btnJocPersonalitzat);
		
		JButton btnNewButton_1 = new JButton("Hidato proposat");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				iVistaPrincipal.changeJocPropConf(); 
			}
		});
		btnNewButton_1.setBounds(374, 233, 161, 23);
		add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Partides guardades");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				iVistaPrincipal.changeJocPGConf();
			}
		});
		btnNewButton_2.setBounds(374, 268, 161, 23);
		add(btnNewButton_2);
		
		JLabel lblJugar = new JLabel("Jugar");
		lblJugar.setHorizontalAlignment(SwingConstants.CENTER);
		lblJugar.setFont(new Font("Tahoma", Font.PLAIN, 36));
		lblJugar.setBounds(10, 54, 890, 76);
		add(lblJugar);
		
		JButton btnTronarEnrere = new JButton("Tornar enrere");
		btnTronarEnrere.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				iVistaPrincipal.changeMenu();
			}
		});
		btnTronarEnrere.setBounds(374, 358, 161, 23);
		add(btnTronarEnrere);

	}

}
