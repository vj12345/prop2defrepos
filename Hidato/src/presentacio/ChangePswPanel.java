package presentacio;

import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ChangePswPanel extends JPanel {
	private CtrlPresentacio iCtrlPresentacio;
	private VistaPrincipal iVistaPrincipal;
	private JPasswordField passwordField;
	private JTextField textField;
	private JPasswordField passwordField_1;
	/**
	 * Create the panel.
	 */
	public ChangePswPanel(CtrlPresentacio pCtrlPresentacio, VistaPrincipal pVistaPrincipal) {
		iCtrlPresentacio = pCtrlPresentacio;
		iVistaPrincipal = pVistaPrincipal;
		this.setPreferredSize(new Dimension(910, 512));
		setLayout(null);
		
		JLabel lblModificarContrasenya = new JLabel("Modificar contrasenya");
		lblModificarContrasenya.setFont(new Font("Tahoma", Font.PLAIN, 36));
		lblModificarContrasenya.setBounds(261, 45, 387, 67);
		add(lblModificarContrasenya);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(487, 206, 127, 20);
		add(passwordField);
		
		textField = new JTextField();
		textField.setBounds(487, 156, 127, 20);
		add(textField);
		textField.setColumns(10);
		
		JLabel lblContrasenyaActual = new JLabel("Contrasenya actual :");
		lblContrasenyaActual.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblContrasenyaActual.setBounds(261, 154, 145, 20);
		add(lblContrasenyaActual);
		
		JLabel lblContrasenyaNova = new JLabel("Contrasenya nova :");
		lblContrasenyaNova.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblContrasenyaNova.setBounds(261, 204, 145, 20);
		add(lblContrasenyaNova);
		
		JLabel lblConfirmarContrasenya = new JLabel("Confirmar contrasenya :");
		lblConfirmarContrasenya.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblConfirmarContrasenya.setBounds(261, 250, 155, 20);
		add(lblConfirmarContrasenya);
		
		passwordField_1 = new JPasswordField();
		passwordField_1.setBounds(487, 252, 127, 20);
		add(passwordField_1);
		
		JButton btnNewButton = new JButton("Acceptar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String oldpsw = textField.getText();
				String newpsw = new String(passwordField.getPassword());
				String confirmpsw = new String(passwordField_1.getPassword());
				if (textField.getText().isEmpty()) {
					JOptionPane.showMessageDialog(iVistaPrincipal, "Contrasenya actual buida");
				}
				else if (newpsw.length() == 0) {
					JOptionPane.showMessageDialog(iVistaPrincipal, "Contrasenya nova buida");
				}
				else if (!newpsw.equals(confirmpsw)) {
					JOptionPane.showMessageDialog(iVistaPrincipal, "Les contrasenyes noves no coincideixen");
				}
				else {
					boolean valid = iCtrlPresentacio.changePsw(oldpsw, newpsw);
					if (valid) {
						 JOptionPane.showMessageDialog(iVistaPrincipal, "La contrasenya s'ha mofificat correctament");
						 iVistaPrincipal.changeConfiguracio();;
					}
					else {
						 JOptionPane.showMessageDialog(iVistaPrincipal, "La contrasenya actual es incorrecte");
					}
				}
			}
		});
		btnNewButton.setBounds(525, 329, 89, 23);
		add(btnNewButton);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				iVistaPrincipal.changeConfiguracio();
			}
		});
		btnCancelar.setBounds(261, 329, 89, 23);
		add(btnCancelar);
		

	}
}
