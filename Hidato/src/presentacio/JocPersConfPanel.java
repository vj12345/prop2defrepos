package presentacio;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.JTextField;

public class JocPersConfPanel extends JPanel {
	private CtrlPresentacio iCtrlPresentacio;
	private VistaPrincipal iVistaPrincipal;
	
	String tipusCela;
	String top;
	JPanel auto;
	private JTextField NomHIdTextField;
	private JTextField ForatsTextField;
	private JTextField ValorsDonatsTextField;
	/**
	 * Create the panel.
	 */
	public JocPersConfPanel(CtrlPresentacio pCtrlPresentacio, VistaPrincipal pVistaPrincipal) {
		iCtrlPresentacio = pCtrlPresentacio;
		iVistaPrincipal = pVistaPrincipal;
		auto = this;
		
		this.setPreferredSize(new Dimension(910, 512));
		setLayout(null);
		
		JLabel lblTipusDeCela = new JLabel("Tipus de cela :");
		lblTipusDeCela.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTipusDeCela.setBounds(310, 285, 92, 28);
		add(lblTipusDeCela);
		
		JLabel lblAdjacencia = new JLabel("Adjacencia :");
		lblAdjacencia.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblAdjacencia.setBounds(310, 316, 92, 28);
		add(lblAdjacencia);
		
		JLabel lblFiles = new JLabel("Topologia :");
		lblFiles.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblFiles.setBounds(310, 345, 92, 28);
		add(lblFiles);
	
	
		JComboBox<String> comboBox_2 = new JComboBox<String>();
		comboBox_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (comboBox_2.getSelectedItem().equals("HEXAGONAL")) {
					
					
					tipusCela = "HEXAGONAL";
					
					
				
				}
				else if (comboBox_2.getSelectedItem().equals("QUADRADA")) {
					tipusCela = "QUADRADA";
				
					
				}
				else if (comboBox_2.getSelectedItem().equals("TRIANGULAR")) {
					tipusCela = "TRIANGULAR";
				
				}
			}
		});
		comboBox_2.addItem("TRIANGULAR");
		comboBox_2.addItem("QUADRADA");
		comboBox_2.addItem("HEXAGONAL");
		comboBox_2.setBounds(476, 291, 121, 20);
		add(comboBox_2);
		
		JComboBox<String> comboBox_1 = new JComboBox<String>();
		comboBox_1.addItem("COSTAT");
		comboBox_1.addItem("COSTAT I VERTEX");
		comboBox_1.setBounds(476, 322, 121, 20);
		add(comboBox_1);
		
		
		JComboBox<String> comboBox_top = new JComboBox<String>();
		comboBox_top.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (comboBox_top.getSelectedItem().equals("QUADRADA")) {
					top = "QUADRADA";
				
					
				}
				else if (comboBox_top.getSelectedItem().equals("TRIANGULAR")) {
					top = "TRIANGULAR";
				
				}
			}
		});
		comboBox_top.addItem("TRIANGULAR");
		comboBox_top.addItem("QUADRADA");
		comboBox_top.setBounds(476, 351, 121, 20);
		add(comboBox_top);
		
		JComboBox<Integer> comboBoxCeles = new JComboBox<Integer>();
		comboBoxCeles.addItem(9);
		comboBoxCeles.addItem(16);
		comboBoxCeles.addItem(25);
		
		comboBoxCeles.setBounds(476, 209, 121, 20);
		add(comboBoxCeles);
		
		JButton btnComenar = new JButton("Comen\u00E7ar");
		btnComenar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tipusCela = (String) comboBox_2.getSelectedItem();
				top = (String) comboBox_top.getSelectedItem();
				String nomhid = NomHIdTextField.getText();
				Integer numCeles = (Integer) comboBoxCeles.getSelectedItem();
				int forats = 0;
				int omplertes = 0;

				
				int[][] tauler = null;
				if (nomhid.length() < 3) {
					JOptionPane.showMessageDialog(auto, "El nom del hidato com a minim ha de tenir 3 caracters.", "Error", JOptionPane.ERROR_MESSAGE);

				}
				else if ((tipusCela.equals("TRIANGULAR")) && (numCeles == 25) && comboBox_1.getSelectedItem().equals("COSTAT")) {
					JOptionPane.showMessageDialog(auto, "Ho sentim, pero per problemes tencnics de moment no esta disponible els hidatos triangulars de 25 cel�les amb adjacencia de costat.", "Error", JOptionPane.ERROR_MESSAGE);

				}
				else if (!((JocPersConfPanel) auto).isInteger(ForatsTextField.getText())) {
				
					JOptionPane.showMessageDialog(auto, "EL camp forats ha de ser en enter major que o igual a 0.", "Error", JOptionPane.ERROR_MESSAGE);

				}
				else if (Integer.parseInt(ForatsTextField.getText()) < 0) {
					
					JOptionPane.showMessageDialog(auto, "EL camp forats ha de ser en enter major que o igual a 0.", "Error", JOptionPane.ERROR_MESSAGE);

				}
				else if (!((JocPersConfPanel) auto).isInteger((ValorsDonatsTextField.getText()))) {
					JOptionPane.showMessageDialog(auto, "EL camp # valors doants ha de ser en enter major que o igual a 2.", "Error", JOptionPane.ERROR_MESSAGE);

				}
				else if (Integer.parseInt(ValorsDonatsTextField.getText()) < 2) {
					JOptionPane.showMessageDialog(auto, "EL camp # valors donats ha de ser en enter major que o igual a 2.", "Error", JOptionPane.ERROR_MESSAGE);

				}
				else if (tipusCela.equals("TRIANGULAR") && top.equals("TRIANGULAR") && comboBox_1.getSelectedItem().equals("COSTAT") && Integer.parseInt(ForatsTextField.getText()) < 3 ) {
					JOptionPane.showMessageDialog(auto, "Per les celes triangulars amb adjacencia de costat com a minim ha de tenir 3 forats.", "Error", JOptionPane.ERROR_MESSAGE);

				}
				else if (Integer.parseInt((ValorsDonatsTextField.getText())) < 2) {
					JOptionPane.showMessageDialog(auto, "Com a a minim ha d'haver 2 numeros omplertes.", "Error", JOptionPane.ERROR_MESSAGE);
					
				}
				else if ((Integer.parseInt(ForatsTextField.getText())+Integer.parseInt((ValorsDonatsTextField.getText()))) >= numCeles) {
					JOptionPane.showMessageDialog(auto, "Hi ha massa forats o numeros omplertes.", "Error", JOptionPane.ERROR_MESSAGE);

				}
				
				else if (tipusCela.equals("TRIANGULAR")) {
					forats = Integer.parseInt(ForatsTextField.getText());
					omplertes = Integer.parseInt(ValorsDonatsTextField.getText());
					if (!top.equals("TRIANGULAR")) {
						JOptionPane.showMessageDialog(auto, "Per cel�les triangulars nomes esta disponible la topologia triangular", "Error", JOptionPane.ERROR_MESSAGE);
					}
					else {
						tauler = iCtrlPresentacio.crearJocRapid(tipusCela, top, (String)comboBox_1.getSelectedItem(), nomhid, numCeles, forats, omplertes);
						for(int i = 0 ; i < tauler.length; i++) {
							for (int j = 0 ; j < tauler[0].length; j++) {
								System.out.print(tauler[i][j] + " ");
							}
							System.out.println();

						}
						String dif = calculDif(numCeles, forats, omplertes);
					
						
						iVistaPrincipal.changeJocRapid(tauler, tipusCela ,top, (String)comboBox_1.getSelectedItem(), dif, null);
						
					}
					
				}
				else if (tipusCela.equals("QUADRADA")) {
					forats = Integer.parseInt(ForatsTextField.getText());
					omplertes = Integer.parseInt(ValorsDonatsTextField.getText());
					if (!top.equals("QUADRADA")) {
						JOptionPane.showMessageDialog(auto, "Per cel�les quadrades nomes esta disponible la topologai quadrada", "Error", JOptionPane.ERROR_MESSAGE);
					}
					else {
						tauler = iCtrlPresentacio.crearJocRapid(tipusCela, top, (String)comboBox_1.getSelectedItem(), nomhid, numCeles, forats, omplertes);
						for(int i = 0 ; i < tauler.length; i++) {
							for (int j = 0 ; j < tauler[0].length; j++) {
								System.out.print(tauler[i][j] + " ");
							}
							System.out.println();

						}
						
						String dif = calculDif(numCeles, forats, omplertes);
					
						
						iVistaPrincipal.changeJocRapid(tauler, tipusCela ,top, (String)comboBox_1.getSelectedItem(), dif, null);
					}
					
				}
				else {
					forats = Integer.parseInt(ForatsTextField.getText());
					omplertes = Integer.parseInt(ValorsDonatsTextField.getText());
					if (!top.equals("QUADRADA")) {
						JOptionPane.showMessageDialog(auto, "Per cel�les hexagonals nomes esta disponible la topologai quadrada", "Error", JOptionPane.ERROR_MESSAGE);
					}
					else {
						tauler = iCtrlPresentacio.crearJocRapid(tipusCela, top, (String)comboBox_1.getSelectedItem(), nomhid, numCeles, forats, omplertes);
						for(int i = 0 ; i < tauler.length; i++) {
							for (int j = 0 ; j < tauler[0].length; j++) {
								System.out.print(tauler[i][j] + " ");
							}
							System.out.println();

						}
						
						String dif = calculDif(numCeles, forats, omplertes);
					
						
						iVistaPrincipal.changeJocRapid(tauler, tipusCela ,top, (String)comboBox_1.getSelectedItem(), dif, null);
					}
					
				}
	
			}
		});
		btnComenar.setBounds(476, 397, 121, 23);
		add(btnComenar);
		
		JButton btnSortir = new JButton("Sortir");
		btnSortir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				iVistaPrincipal.changeJugar();
			}
		});
		btnSortir.setBounds(328, 397, 121, 23);
		add(btnSortir);
		
		JLabel lblConfiguracioJocRapid = new JLabel("Configuracio joc personalitzat");
		lblConfiguracioJocRapid.setHorizontalAlignment(SwingConstants.CENTER);
		lblConfiguracioJocRapid.setFont(new Font("Tahoma", Font.PLAIN, 28));
		lblConfiguracioJocRapid.setBounds(258, 53, 393, 69);
		add(lblConfiguracioJocRapid);
		
		JLabel lblNomHidato = new JLabel("Nom hidato :");
		lblNomHidato.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNomHidato.setBounds(310, 180, 92, 19);
		add(lblNomHidato);
		
		NomHIdTextField = new JTextField();
		NomHIdTextField.setBounds(476, 181, 121, 20);
		add(NomHIdTextField);
		NomHIdTextField.setColumns(10);
		
		
		
		JLabel lblNumeroCelles = new JLabel("Numero cel\u00B7les :");
		lblNumeroCelles.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNumeroCelles.setBounds(310, 210, 109, 14);
		add(lblNumeroCelles);
		
		JLabel lblForats = new JLabel("Forats :");
		lblForats.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblForats.setBounds(310, 235, 92, 14);
		add(lblForats);
		
		ForatsTextField = new JTextField();
		ForatsTextField.setBounds(476, 234, 122, 20);
		add(ForatsTextField);
		ForatsTextField.setColumns(10);
		
		JLabel lblValorsDonats = new JLabel("# Valors donats :");
		lblValorsDonats.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblValorsDonats.setBounds(310, 260, 110, 14);
		add(lblValorsDonats);
		
		ValorsDonatsTextField = new JTextField();
		ValorsDonatsTextField.setBounds(476, 260, 121, 20);
		add(ValorsDonatsTextField);
		ValorsDonatsTextField.setColumns(10);
	}
	
	  //comprova si un string es un integer
			public boolean isInteger(String s) {
				try {
					int x = Integer.parseInt(s);
					if (x < 0) {
						return false;
					}
				} catch(NumberFormatException e) {
					return false;
				} catch (NullPointerException e) {
					return false;
				}
				return true;
			}
	private String calculDif(int ct, int f, int omp) {
		if ((float)(f+omp)/ct < 0.3) {
			return "DIFICIL";
		}
		else if ((float)(f+omp)/ct < 0.6) {
			return "MITJA";
		}
		return "FACIL";
	}
}
