package presentacio;

import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;

public class RankingPanel extends JPanel {
	private CtrlPresentacio iCtrlPresentacio;
	private VistaPrincipal iVistaPrincipal;
	private JTable table;
	private JTable table_1;
	private JTable table_2;
	/**
	 * Create the panel.
	 */
	public RankingPanel(CtrlPresentacio pCtrlPresentacio, VistaPrincipal pVistaPrincipal) {
		iCtrlPresentacio = pCtrlPresentacio;
		iVistaPrincipal = pVistaPrincipal;
		this.setPreferredSize(new Dimension(910, 512));
		setLayout(null);
		
		JLabel lblRanking = new JLabel("Ranking");
		lblRanking.setHorizontalAlignment(SwingConstants.CENTER);
		lblRanking.setFont(new Font("Tahoma", Font.PLAIN, 36));
		lblRanking.setBounds(10, 11, 890, 76);
		add(lblRanking);
		
		JLabel lblEasy = new JLabel("Easy");
		lblEasy.setHorizontalAlignment(SwingConstants.CENTER);
		lblEasy.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblEasy.setBounds(132, 98, 60, 23);
		add(lblEasy);
		
		JLabel lblMedium = new JLabel("Medium");
		lblMedium.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblMedium.setHorizontalAlignment(SwingConstants.CENTER);
		lblMedium.setBounds(425, 98, 60, 23);
		add(lblMedium);
		
		JLabel lblNewLabel = new JLabel("Dificult");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(716, 98, 60, 23);
		add(lblNewLabel);
		

		crearTablaRanking();
		
		
		JButton btnTornarEnrere = new JButton("Tornar enrere");
		btnTornarEnrere.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				iVistaPrincipal.changeMenu();
			}
		});
		btnTornarEnrere.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnTornarEnrere.setBounds(393, 426, 124, 23);
		add(btnTornarEnrere);

	}
	
	private void crearTablaRanking() {
		//ranking facil

		JScrollPane scrollPane =  new JScrollPane();
		scrollPane.setBounds(66, 132, 200, 272);
		add(scrollPane);
		
		table = crearTabla(1);
		scrollPane.setViewportView(table);
		
		//ranking mitja

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(355, 132, 200, 272);
		add(scrollPane_1);
		
		table_1 = crearTabla(2);
		scrollPane_1.setViewportView(table_1);
		
		//ranking dificil

		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(641, 132, 200, 272);
		add(scrollPane_2);
		
		table_2 = crearTabla(3);
		scrollPane_2.setViewportView(table_2);

	}
	
	private JTable crearTabla(int tipus) {
		List<List<String>> aux =  iCtrlPresentacio.getRanking(tipus);

		
		DefaultTableModel modelo3 = new DefaultTableModel(){

		    @Override
		    public boolean isCellEditable(int row, int column) {
		       //all cells false
		       return false;
		    }
		};
		modelo3.addColumn("#");
		modelo3.addColumn("Usuari");
		modelo3.addColumn("Puntuacio");
		
		JTable table_3 = new JTable();
	
		
		for (int i = 0; i < aux.size(); i++) {
			modelo3.addRow(new Object[] {Integer.toString(i+1), aux.get(i).get(0), aux.get(i).get(1)});
		}
		
		table_3.setModel(modelo3);
		table_3.getColumnModel().getColumn(0).setMaxWidth(20);
		table_3.getColumnModel().getColumn(0).setResizable(false);
		table_3.getColumnModel().getColumn(1).setMaxWidth(80);
		table_3.getColumnModel().getColumn(1).setResizable(false);
		table_3.getColumnModel().getColumn(2).setResizable(false);
		table_3.getTableHeader().setReorderingAllowed(false);
		return table_3;
	}
}
