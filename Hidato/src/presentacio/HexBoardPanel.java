package presentacio;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class HexBoardPanel extends BoardPanel {


    private int hexSide; // Side of the hexagon

    private int hexOffset; // Distance from left horizontal vertex to vertical axis

    private int hexApotheme; // Apotheme of the hexagon = radius of inscribed circumference

    private int hexRectWidth; // Width of the circumscribed rectangle

    private int hexRectHeight; // Height of the circumscribed rectangle

    private int hexGridWidth;  // hexOffset + hexSide (b + s)
    
    
 

	/**
	 * Create the panel.
	 */
	public HexBoardPanel(int width, int height, int hexSide) {
		
		
		super(width, height, (int) (hexSide * Math.cos(Math.PI / 6)),(int) (hexSide * Math.sin(Math.PI / 6)));
		
        this.hexSide = hexSide;
        hexApotheme = (int) (hexSide * Math.cos(Math.PI / 6));
        hexOffset = (int) (hexSide * Math.sin(Math.PI / 6));
        hexGridWidth = hexOffset + hexSide;
        hexRectWidth = 2 * hexOffset + hexSide;
        hexRectHeight = 2 * hexApotheme;
	}
	
	public HexBoardPanel(int[][] solu, int width, int height, int hexSide, JPanel parent, int[][] t_guardat) {
		super(solu,width, height, (int) (hexSide * Math.cos(Math.PI / 6)),(int) (hexSide * Math.sin(Math.PI / 6)), parent, t_guardat);
		
        this.hexSide = hexSide;
        hexApotheme = (int) (hexSide * Math.cos(Math.PI / 6));
        hexOffset = (int) (hexSide * Math.sin(Math.PI / 6));
        hexGridWidth = hexOffset + hexSide;
        hexRectWidth = 2 * hexOffset + hexSide;
        hexRectHeight = 2 * hexApotheme;
	}
	
	 @Override
	    protected void paintComponent(Graphics g) {
	        super.paintComponent(g);
	        Graphics2D g2d = (Graphics2D) g;
	        
	        for (int i = 0; i < width; i++) {
	            for (int j = 0; j < height; j++) {
	            	
	            	if (super.getBoard()[j][i] == -2) {
	            		Polygon aux = buildPolygon(i,j);
		                g2d.setColor(Color.BLACK);
		                g2d.fillPolygon(aux);
		                g2d.drawPolygon(aux);
		                
	            	}
	            	else if (super.getBoard()[j][i] == -3 && super.editor) {
	            		Polygon aux = buildPolygon(i,j);
		                g2d.setColor(Color.RED);
		                g2d.fillPolygon(aux);
		                g2d.drawPolygon(aux);
		                
	            	}
	            	else {
	            		if ((!super.editor && super.getBoard()[j][i] != -3) || super.editor) {
	            			if(super.ficatsPerDefecte.contains(super.getBoard()[j][i])) {
	            				Polygon aux = buildPolygon(i,j);
				                g2d.setColor(Color.GRAY);
				                g2d.fillPolygon(aux);
				                g2d.drawPolygon(aux);
	            			}
	            			else {
			            		Polygon aux = buildPolygon(i,j);
				                g2d.setColor(Color.orange);
				                g2d.fillPolygon(aux);
				                g2d.drawPolygon(aux);
	            			}
	            		}
		                
	            	}
	            	
	            	if ((!super.editor && super.getBoard()[j][i] != -3) || super.editor) {
	            		g2d.setStroke(new BasicStroke(2));
		            	Polygon p = buildPolygon(i,j);
		            	g2d.setColor(Color.CYAN);
		            	//g2d.fillPolygon(p);
		                g2d.drawPolygon(p);
	            	}
	                
	            }
	        }
	    }
	    
	    public void paint(Graphics g) {
	    	super.paint(g);
	        Graphics2D g2d = (Graphics2D) g;
	     
	        g2d.setStroke(new BasicStroke(3));
	    	g2d.setColor(Color.GREEN);
	        g2d.drawPolygon(buildPolygon(a, b));
	        g2d.setColor(Color.BLACK);
	        //g2d.drawPolygon(buildHexagon(sa, sb));

	       
	    }


	    @Override
	    public Dimension getPreferredSize() {
	        int panelWidth =        width * hexRectHeight + hexApotheme + 1;

	        int panelHeight =  height * hexGridWidth + hexOffset +1;
	        return new Dimension(panelWidth, panelHeight);
	    }
	 
	    Polygon buildPolygon(int column, int row) {
	        Polygon hex = new Polygon();
	        Point origin = tileToPixel(column, row);
	        hex.addPoint(origin.x + hexApotheme, origin.y);
	        hex.addPoint(origin.x + hexRectHeight, origin.y+hexOffset);
	        hex.addPoint(origin.x + hexRectHeight, origin.y + hexGridWidth);
	        hex.addPoint(origin.x + hexApotheme, origin.y + hexRectWidth);
	        hex.addPoint(origin.x, origin.y + hexGridWidth);
	        hex.addPoint(origin.x, origin.y + hexOffset);
	        return hex;
	    }
	 
	    Point tileToPixel(int column, int row) {
	        Point pixel = new Point();
	        if (row%2 != 0) pixel.x = hexRectHeight * column + hexApotheme;
	        else {
	        	pixel.x = hexRectHeight * column;
	        }
	        pixel.y = hexGridWidth * row;
	        return pixel;
	    }
	 
	    Point pixelToTile(int x, int y) {
	        double hexRise = (double) hexOffset / (double)  hexApotheme;
	        Point p = new Point(x / hexRectHeight, y / hexGridWidth);
	        Point r = new Point(x % hexRectHeight, y % hexGridWidth);
	        if (p.y % 2 == 0) { //odd row
	        	if (r.y < -hexRise*r.x+hexOffset) {
	        		return new Point (p.x-1, p.y-1);
	        	}
	        	else if (r.y < hexRise*r.x-hexOffset) {
	        		return new Point (p.x, p.y-1);
	        	}
	        	else {
	        		return new Point (p.x, p.y);
	        	}
	            
	        } else { //even row
	        	if (r.y < hexRise*r.x && r.y < -hexRise*r.x+2*hexOffset) {
	        		return new Point(p.x, p.y-1);
	        	}
	        	else if (r.x < hexApotheme) {
	        		return new Point(p.x-1, p.y);
	        	}
	     
	        }
	        return new Point(p.x, p.y);
	    }
	 
	
	    
}
