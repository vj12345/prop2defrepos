package presentacio;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.font.TextAttribute;
import java.util.HashMap;
import java.util.Map;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPasswordField;
import javax.swing.SwingConstants;

public class LoginPanel extends JPanel {
	
	private CtrlPresentacio iCtrlPresentacio;
	private VistaPrincipal iVistaPrincipal;
	
	private JTextField textField;
	private JPasswordField passwordField;

	/**
	 * Create the panel.
	 */
	public LoginPanel(CtrlPresentacio pCtrlPresentacio, VistaPrincipal pVistaPrincipal) {
		
		iCtrlPresentacio = pCtrlPresentacio;
		iVistaPrincipal = pVistaPrincipal;
		
		setLayout(null);
		this.setPreferredSize(new Dimension(910,512));
		
		JLabel lblLogin = new JLabel("Login");
		lblLogin.setHorizontalAlignment(SwingConstants.CENTER);
		lblLogin.setBounds(10, 54, 890, 76);
		lblLogin.setFont(new Font("Tahoma", Font.PLAIN, 36));
		add(lblLogin);
		
		JLabel lblUsername = new JLabel("Usuari :");
		lblUsername.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblUsername.setBounds(320, 172, 97, 19);
		add(lblUsername);
		
		textField = new JTextField();
		textField.setBounds(427, 173, 129, 20);
		add(textField);
		textField.setColumns(10);
		
		JLabel lblPassword = new JLabel("Contrasenya :");
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPassword.setBounds(320, 229, 97, 19);
		add(lblPassword);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(427, 230, 129, 20);
		add(passwordField);
		
		JButton btnEntrar = new JButton("Entrar");
		btnEntrar.setBounds(448, 261, 90, 23);
		btnEntrar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnEntrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String user = textField.getText();
				String psw = new String(passwordField.getPassword());
				if (textField.getText().isEmpty()) {
					JOptionPane.showMessageDialog(iVistaPrincipal, "El camp usuari no pot ser buit");
				}
				else if (psw.length() == 0) {
					JOptionPane.showMessageDialog(iVistaPrincipal, "La contraseny no pot ser buida");
				}
				else {
					boolean valid = iCtrlPresentacio.login(user, psw);
					if (valid) {
						 JOptionPane.showMessageDialog(iVistaPrincipal, "Benvingut, " + user);
						 iCtrlPresentacio.CargarHidatosCreats();
						 iCtrlPresentacio.CargarPartidesGuardades();
						 iCtrlPresentacio.CargarRankings();
						 iVistaPrincipal.changeMenu();
					}
					else {
						 JOptionPane.showMessageDialog(iVistaPrincipal, "Usuari o contrasenya incorrecte");
					}
				}
				
			}
		});
		add(btnEntrar);
		
		JLabel lblNewAccount = new JLabel("Crear un compte nou");
		lblNewAccount.setBounds(437, 295, 176, 14);
		lblNewAccount.setForeground(Color.BLUE);
		lblNewAccount.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent arg0) {
				// TODO Auto-generated method stub
				iVistaPrincipal.changeToRegister();
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub
				Font font = lblNewAccount.getFont();
				Map<TextAttribute, Object> attributes = new HashMap<>(font.getAttributes());
				attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
				lblNewAccount.setFont(font.deriveFont(attributes));
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub
				 Font font = lblNewAccount.getFont();
				 Map<TextAttribute, Object> attributes = new HashMap<>(font.getAttributes());
				 attributes.put(TextAttribute.UNDERLINE, -1);
				 lblNewAccount.setFont(font.deriveFont(attributes));
			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});
		add(lblNewAccount);
		
		

	}

}
