package presentacio;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.JComboBox;

public class JocPGConfPanel extends JPanel {
	private CtrlPresentacio iCtrlPresentacio;
	private VistaPrincipal iVistaPrincipal;
	private JTable table_1;
	private JScrollPane scrollPane_1;
	/**
	 * Create the panel.
	 */
	public JocPGConfPanel(CtrlPresentacio pCtrlPresentacio, VistaPrincipal pVistaPrincipal) {
		iCtrlPresentacio = pCtrlPresentacio;
		iVistaPrincipal = pVistaPrincipal;
		this.setPreferredSize(new Dimension(910, 512));
		setLayout(null);
		
		JLabel lblRanking = new JLabel("Partides Guardades");
		lblRanking.setHorizontalAlignment(SwingConstants.CENTER);
		lblRanking.setFont(new Font("Tahoma", Font.PLAIN, 36));
		lblRanking.setBounds(10, 11, 890, 76);
		add(lblRanking);
		
		JButton btnTornarEnrere = new JButton("Sortir");
		btnTornarEnrere.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				iVistaPrincipal.changeJugar();
			}
		});
		btnTornarEnrere.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnTornarEnrere.setBounds(393, 426, 124, 23);
		add(btnTornarEnrere);
		
		 scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(120, 128, 479, 272);
		add(scrollPane_1);
		table_1 = crearTabla();
		scrollPane_1.setViewportView(table_1);
		
		JButton btnJugar = new JButton("Jugar");
		btnJugar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (table_1.getSelectedRow() > -1) {
					System.out.println("selected row: " + table_1.getSelectedRow());
					int[][] tauler = iCtrlPresentacio.crearJocGuardat(table_1.getSelectedRow());
					String tipusCela = iCtrlPresentacio.getTipusCelaHidatoGuardat(table_1.getSelectedRow());
					//String adj = iCtrlPresentacio.getAdjHidatoGuardat(table_1.getSelectedRow());
					int[][] guardat = iCtrlPresentacio.getTaulerGuardat();
					String dif = "NONE";
					iVistaPrincipal.changeJocRapid(tauler, tipusCela ,"NONE", "NONE", dif, guardat);
				}
			}
		});
		btnJugar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnJugar.setBounds(660, 182, 89, 23);
		add(btnJugar);
		
		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (table_1.getSelectedRow() > -1) {
					iCtrlPresentacio.borrarPartidaGuardada(table_1.getSelectedRow());
					table_1 = crearTabla();
					scrollPane_1.setViewportView(table_1);

				}
			}
		});
		btnBorrar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnBorrar.setBounds(660, 292, 89, 23);
		add(btnBorrar);
		

		
		
		table_1.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {
				// TODO Auto-generated method stub
				/*
				if (table_1.getSelectedRow() > -1) {
					System.out.println("selected row: " + table_1.getSelectedRow());
					int[][] tauler = iCtrlPresentacio.crearJocGuardat(table_1.getSelectedRow());
					String tipusCela = iCtrlPresentacio.getTipusCelaHidatoGuardat(table_1.getSelectedRow());
					//String adj = iCtrlPresentacio.getAdjHidatoGuardat(table_1.getSelectedRow());
					int[][] guardat = iCtrlPresentacio.getTaulerGuardat();
					iVistaPrincipal.changeJocRapid(tauler, tipusCela ,"NONE", "NONE", "NULL", guardat);
				}
				*/
			}
			
		});
		
		
	}
	
	private JTable crearTabla() {
		//List<List<String>> aux =  iCtrlPresentacio.getRanking(tipus);

		List<String> aux = iCtrlPresentacio.getHidatosGuardats();
		DefaultTableModel modelo3 = new DefaultTableModel(){

		    @Override
		    public boolean isCellEditable(int row, int column) {
		       //all cells false
		       return false;
		    }
		};
		modelo3.addColumn("#");
		modelo3.addColumn("Id Hidato");
		modelo3.addColumn("Tipus Cel�la");
		modelo3.addColumn("Adjacencia");
		
		JTable table_3 = new JTable();
	
		//modelo3.addRow(new Object[] {"1","cxczdasd","123"});
	
		for (int i = 0; i < aux.size(); i++) {
			
			modelo3.addRow(new Object[] {Integer.toString(i+1), aux.get(i), iCtrlPresentacio.getTipusCelaHidatoGuardat(i), iCtrlPresentacio.getAdjHidatoGuardat(i)});
		
			
		}
			
	/*
		
		for (int i = 0; i < aux.size(); i++) {
			modelo3.addRow(new Object[] {Integer.toString(i+1), aux.get(i).get(0), aux.get(i).get(1)});
		}
		*/
		table_3.setModel(modelo3);
		table_3.getColumnModel().getColumn(0).setMaxWidth(20);
		table_3.getColumnModel().getColumn(0).setResizable(false);
		table_3.getColumnModel().getColumn(1).setWidth(80);
		table_3.getColumnModel().getColumn(1).setResizable(false);
		table_3.getColumnModel().getColumn(2).setWidth(100);
		table_3.getColumnModel().getColumn(2).setResizable(false);
		table_3.getColumnModel().getColumn(3).setResizable(false);
		table_3.getTableHeader().setReorderingAllowed(false);
		return table_3;
	}
	/*
	private String caculDif(int[][] tab) {
		int ct = 0;
		int f = 0;
		int omp = 0;
		for (int i = 0; i < tab.length; i++) {
			for (int j = 0; j < tab[0].length; j++) {
				if (tab[i][j] != -3) {
					ct++;
					if (tab[i][j] == -2) {
						f++;
					}
					else if (tab[i][j] != -1) {
						omp++;
					}
				}
			}
		}
		if ((float)(f+omp)/ct < 0.3) {
			return "DIFICIL";
		}
		else if ((float)(f+omp)/ct < 0.6) {
			return "MITJA";
		}
		return "FACIL";
	}
	*/
}
