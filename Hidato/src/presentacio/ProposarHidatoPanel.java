package presentacio;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;


import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.awt.event.ItemEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;

public class ProposarHidatoPanel extends JPanel {
	private CtrlPresentacio iCtrlPresentacio;
	private VistaPrincipal iVistaPrincipal;
	private JTextField textField;
	
	
	BoardPanel map = new TriBoardPanel(0,0,0);
	

	
	int fila = 0;
	int col = 0;
	
	String nomHidato;
	String adjacencia;
	String tipusCela;

	
	JPanel auto;

	/**
	 * Create the panel.
	 */
	public ProposarHidatoPanel(CtrlPresentacio pCtrlPresentacio, VistaPrincipal pVistaPrincipal) {
		auto = this;

		iCtrlPresentacio = pCtrlPresentacio;
		iVistaPrincipal = pVistaPrincipal;
		
		this.setPreferredSize(new Dimension(910, 512));
		setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Nom Hidato :");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(666, 107, 92, 28);
		add(lblNewLabel);
		
		JLabel lblAdjacencia = new JLabel("Adjacencia :");
		lblAdjacencia.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblAdjacencia.setBounds(666, 163, 92, 28);
		add(lblAdjacencia);
		
		JLabel lblTipusDeCela = new JLabel("Tipus de cela :");
		lblTipusDeCela.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTipusDeCela.setBounds(666, 219, 92, 28);
		add(lblTipusDeCela);
		
		JLabel lblFiles = new JLabel("Files :");
		lblFiles.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblFiles.setBounds(666, 270, 92, 28);
		add(lblFiles);
		
		JLabel lblColumnes = new JLabel("Columnes :");
		lblColumnes.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblColumnes.setBounds(666, 314, 92, 28);
		add(lblColumnes);
		
		textField = new JTextField();
		textField.setBounds(762, 113, 121, 20);
		add(textField);
		textField.setColumns(10);	
		
		

		JComboBox<String> comboBox_1 = new JComboBox<String>();
		comboBox_1.addItem("COSTAT");
		comboBox_1.addItem("COSTAT I VERTEX");
		comboBox_1.setBounds(762, 169, 121, 20);
		add(comboBox_1);
		
		JComboBox<String> comboBox_2 = new JComboBox<String>();
		comboBox_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (comboBox_2.getSelectedItem().equals("HEXAGONAL")) {
					
					
					tipusCela = "HEXAGONAL";
					drawHexBoard();
					
				
				}
				else if (comboBox_2.getSelectedItem().equals("QUADRADA")) {
					tipusCela = "QUADRADA";
					drawQuadBoard();
					
				}
				else if (comboBox_2.getSelectedItem().equals("TRIANGULAR")) {
					tipusCela = "TRIANGULAR";
					drawTriBoard();
				}
			}
		});
		comboBox_2.addItem("TRIANGULAR");
		comboBox_2.addItem("QUADRADA");
		comboBox_2.addItem("HEXAGONAL");
		comboBox_2.setBounds(762, 225, 121, 20);
		add(comboBox_2);
		
		JComboBox<Integer> comboBox_3 = new JComboBox<Integer>();
		comboBox_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				fila = (int) comboBox_3.getSelectedItem();
				if (comboBox_2.getSelectedItem().equals("HEXAGONAL")) {
					
				
					drawHexBoard();
				
				}
				else if (comboBox_2.getSelectedItem().equals("QUADRADA")) {
					drawQuadBoard();
				}
				else if (comboBox_2.getSelectedItem().equals("TRIANGULAR")) {
					drawTriBoard();
				}
				
			}
		});
		comboBox_3.addItem(2);
		comboBox_3.addItem(3);
		comboBox_3.addItem(4);
		comboBox_3.addItem(5);
		comboBox_3.setBounds(762, 276, 121, 20);
		add(comboBox_3);
		
		JComboBox<Integer> comboBox_4 = new JComboBox<Integer>();
		comboBox_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				col = (int) comboBox_4.getSelectedItem();
				
				if (comboBox_2.getSelectedItem().equals("HEXAGONAL")) {
					
					
					
					drawHexBoard();
				
				}
				else if (comboBox_2.getSelectedItem().equals("QUADRADA")) {
					drawQuadBoard();
				}
				else if (comboBox_2.getSelectedItem().equals("TRIANGULAR")) {
					drawTriBoard();
				}

			}
		});
		comboBox_4.addItem(2);
		comboBox_4.addItem(3);
		comboBox_4.addItem(4);
		comboBox_4.addItem(5);
		comboBox_4.setBounds(762, 320, 121, 20);
		add(comboBox_4);
		
		JButton btnNewButton = new JButton("Validar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if (textField.getText().isEmpty()) {
					JOptionPane.showMessageDialog(auto, "El nom del hidato no pot ser buida.", "Error", JOptionPane.ERROR_MESSAGE);
				}
				else if (textField.getText().length() < 3) {
					JOptionPane.showMessageDialog(auto, "El nom del hidato ha de tenir com a minim 3 caracters.", "Error", JOptionPane.ERROR_MESSAGE);

				}
				else if (totOmplert()) {
					JOptionPane.showMessageDialog(auto, "El hidato almenys ha de tenir una cela per omplir.", "Error", JOptionPane.ERROR_MESSAGE);

				}
				else if (!iCtrlPresentacio.NomHidatoUsable(textField.getText())) {
					JOptionPane.showMessageDialog(auto, "Ja existeix un hidato amb aquest nom.", "Error", JOptionPane.ERROR_MESSAGE);

				}
				else { 
					if (iCtrlPresentacio.proposarHidato(map.getBoard(), textField.getText(), (String) comboBox_1.getSelectedItem(), tipusCela, fila, col)) {
						String[] options = {"Mostrar solucio", "Guardar i tornar al menu principal","Guardar i jugar", "Fer modificacions"};
						int seleccio = JOptionPane.showOptionDialog(auto, "L'hidato proposat es valid.\n\nSelecciona l'accio que vols fer :", "Hidato proposat correctament", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);
						if (seleccio == 0) {
							BoardPanel solu;
							if (tipusCela.equals("HEXAGONAL")) {
								solu = new HexBoardPanel(iCtrlPresentacio.obtenirSolu(), col, fila, 30, auto, iCtrlPresentacio.obtenirSolu2());
								
							}
							else if (tipusCela.equals("QUADRADA")) {
								solu = new QuadBoardPanel(iCtrlPresentacio.obtenirSolu(), col, fila, 30, auto, iCtrlPresentacio.obtenirSolu2());

							}
							else {
								solu = new TriBoardPanel(iCtrlPresentacio.obtenirSolu(), col, fila, 30, auto, iCtrlPresentacio.obtenirSolu2());

							}
							
							solu.setBounds(83, 122,  solu.getPreferredSize().width,solu.getPreferredSize().height);
							auto.add(solu);
							solu.setLayout(null);
							solu.ficarLabels();
							solu.repaint();
							String[] opt = {"Fer Modificacions", "Guardar i tornar al menu principal"};
							int sela = JOptionPane.showOptionDialog(auto, solu, "Solucio", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, opt, opt[0]);
							if (sela == 1) {
								if (iCtrlPresentacio.guardarHidatoProposat()) {
									String[] opts = {"Acceptar"};
									int sel = JOptionPane.showOptionDialog(auto, "Hidato guardat correctament", "Informacio", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, opts, opts[0]);
									if (sel == 0) {
										iVistaPrincipal.changeMenu();
									}
								}
								else {
									JOptionPane.showMessageDialog(auto, "No s'ha pogut guardar el hidato creat", "Error", JOptionPane.ERROR_MESSAGE);

								}
							}
							//JOptionPane.showMessageDialog(auto, solu, "Error", JOptionPane.ERROR_MESSAGE);
							//JDialog f = new JDialog(auto, "caca");
							//f.setVisible(true);
						}
						else if (seleccio == 1) {
							if (iCtrlPresentacio.guardarHidatoProposat()) {
								String[] opts = {"Acceptar"};
								int sel = JOptionPane.showOptionDialog(auto, "Hidato guardat correctament", "Informacio", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, opts, opts[0]);
								if (sel == 0) {
									iVistaPrincipal.changeMenu();
								}
							}
							else {
								JOptionPane.showMessageDialog(auto, "No s'ha pogut guardar el hidato creat", "Error", JOptionPane.ERROR_MESSAGE);

							}
						}
						else if (seleccio == 2) {
							if (iCtrlPresentacio.guardarHidatoProposat()) {
								String[] opts = {"Acceptar"};
								int sel = JOptionPane.showOptionDialog(auto, "Hidato guardat correctament, Fes click en el boto aceptar per comen�ar a jugar.", "Informacio", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, opts, opts[0]);
								if (sel == 0) {
									int[][] tauler = iCtrlPresentacio.crearJocProposat(-1);
									String tipusCela = iCtrlPresentacio.getTipusCelaHidatoProposat(-1);
									//String adj = iCtrlPresentacio.getAdjHidatoProposat(table_1.getSelectedRow());
									iVistaPrincipal.changeJocRapid(tauler, tipusCela ,"NONE", "NONE", "NONE", null);
								}
							}
							else {
								JOptionPane.showMessageDialog(auto, "No s'ha pogut guardar el hidato creat", "Error", JOptionPane.ERROR_MESSAGE);

							}
						}
					}
					
					else {
						JOptionPane.showMessageDialog(auto, "No es pot crear un hidato amb aquestes cararcteristiques.", "Error", JOptionPane.ERROR_MESSAGE);

					}
				}
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnNewButton.setBounds(666, 399, 215, 38);
		add(btnNewButton);
		
		JButton btnSortir = new JButton("Sortir");
		btnSortir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int resp = JOptionPane.showConfirmDialog(auto, "Estas segur de sortir?\n(Es perdra el hidato proposat fins ara.)", "Missatge de confirmacio", JOptionPane.YES_NO_OPTION);
				if (resp == JOptionPane.YES_OPTION) {
					iVistaPrincipal.changeMenu();
				}
			}
		});
		btnSortir.setBounds(55, 414, 89, 23);
		add(btnSortir);
		
		
		

		

		

	}
	
	public void accioClickDret(int tilex, int tiley) {
		String[] options = {"Resetejar cel�la", "Ficar Numero","Ficar Forat", "Eliminar cel�la", "Cancelar"};
		int seleccio = JOptionPane.showOptionDialog(this, "Tria l'acci� que vols efectuar sobre la cel�la :", "Selecci� d'acci�", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
		if (seleccio == 0) {
		
				map.borrarNum(tilex, tiley);

			
			
		}
		else if (seleccio == 1)
		{
			
		
		
				String respuesta = JOptionPane.showInputDialog(this, "Introduiex el numero que vols ficar : ", " ",JOptionPane.INFORMATION_MESSAGE);
				if (map.isInteger(respuesta)) {
			
						map.ficarNumPersonalitzat(tilex, tiley, Integer.parseInt(respuesta));
	
					
				}
				else if (respuesta != null){
					JOptionPane.showMessageDialog(this, "Nomes es pot ficar numeros enters.", "Error", JOptionPane.WARNING_MESSAGE);
				}
			
		}
		else if (seleccio == 2) {
			
			map.ficarForat(tilex, tiley);
		}
		else if (seleccio == 3) {
			map.eliminarCela(tilex, tiley);
		}
	}

	Point pantL;
    Point pantR;
	private void drawHexBoard() {
	
		remove(map);
		//remove(map3);
		map = new HexBoardPanel(col,fila,30);
		map.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mousePressed(MouseEvent me) {
				// TODO Auto-generated method stub
				if (SwingUtilities.isLeftMouseButton(me)) {
					pantL = map.pixelToTile(me.getX(), me.getY());

				}
				else if (SwingUtilities.isRightMouseButton(me)) {
					pantR = map.pixelToTile(me.getX(), me.getY());
				}
			}

			@Override
			public void mouseReleased(MouseEvent me) {
				// TODO Auto-generated method stub
				if (SwingUtilities.isLeftMouseButton(me)) {
					Point pact = map.pixelToTile(me.getX(), me.getY());
					if (pantL.x == pact.x && pantL.y == pact.y) {
						if (map.tileIsWithinBoard(pact)) {
							   
			        		
			        		map.afegirNum(pact.x, pact.y);
			        	}
			        	
					}
				}
				else if (SwingUtilities.isRightMouseButton(me)) {
					Point pact2 = map.pixelToTile(me.getX(), me.getY());
					if (pantR.x == pact2.x && pantR.y == pact2.y) {
						if (map.tileIsWithinBoard(pact2)) {
							//map.borrarNum(pact2.x, pact2.y);
							accioClickDret(pact2.x, pact2.y);

						}
					}
				}
				repaint();
			}
			
		});
		map.addMouseMotionListener(new MouseMotionListener() {

			@Override
			public void mouseDragged(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseMoved(MouseEvent me) {
				// TODO Auto-generated method stub
				Point tileCoordinates = map.pixelToTile(me.getX(), me.getY());
				
	            if (map.tileIsWithinBoard(tileCoordinates)) {
	            	map.setA(tileCoordinates.x);
	            	map.setB(tileCoordinates.y); 
	            	
	            	map.preview(tileCoordinates.x, tileCoordinates.y);
	            } else {
	            	map.setA(-3);
	            	map.setB(-3); 
	            	
	            	
	            }
	            
	        	

	        	repaint();

			}
			
		});
		map.setBounds(83, 62,  map.getPreferredSize().width,map.getPreferredSize().height);
		add(map);
		map.repaint();
		
	}
	
private void drawTriBoard() {
		
		
		remove(map);
		//remove(map3);
		map = new TriBoardPanel(col,fila,30);
		map.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mousePressed(MouseEvent me) {
				// TODO Auto-generated method stub
				if (SwingUtilities.isLeftMouseButton(me)) {
					pantL = map.pixelToTile(me.getX(), me.getY());

				}
				else if (SwingUtilities.isRightMouseButton(me)) {
					pantR = map.pixelToTile(me.getX(), me.getY());
				}
			}

			@Override
			public void mouseReleased(MouseEvent me) {
				// TODO Auto-generated method stub
				if (SwingUtilities.isLeftMouseButton(me)) {
					Point pact = map.pixelToTile(me.getX(), me.getY());
					if (pantL.x == pact.x && pantL.y == pact.y) {
						if (map.tileIsWithinBoard(pact)) {
							   
			        		
			        		map.afegirNum(pact.x, pact.y);
			        	}
			        	
					}
				}
				else if (SwingUtilities.isRightMouseButton(me)) {
					Point pact2 = map.pixelToTile(me.getX(), me.getY());
					if (pantR.x == pact2.x && pantR.y == pact2.y) {
						if (map.tileIsWithinBoard(pact2)) {
							//map.borrarNum(pact2.x, pact2.y);
							accioClickDret(pact2.x, pact2.y);
						}
					}
				}
				repaint();
			}
			
		});
		map.addMouseMotionListener(new MouseMotionListener() {

			@Override
			public void mouseDragged(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseMoved(MouseEvent me) {
				// TODO Auto-generated method stub
				Point tileCoordinates = map.pixelToTile(me.getX(), me.getY());
				
	            if (map.tileIsWithinBoard(tileCoordinates)) {
	            	map.setA(tileCoordinates.x);
	            	map.setB(tileCoordinates.y); 
	            	
	            	
	            } else {
	            	map.setA(-3);
	            	map.setB(-3); 
	            	
	            	
	            }
	            map.preview(tileCoordinates.x, tileCoordinates.y);
	        	

	        	repaint();

			}
			
		});
		map.setBounds(83, 62,  map.getPreferredSize().width,map.getPreferredSize().height);
		add(map);
		map.repaint();
		
	}
private void drawQuadBoard() {
	
	remove(map);
	remove(map);
	//remove(map3);
	map = new QuadBoardPanel(col,fila,30);
	map.addMouseListener(new MouseListener() {

		@Override
		public void mouseClicked(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseEntered(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mousePressed(MouseEvent me) {
			// TODO Auto-generated method stub
			if (SwingUtilities.isLeftMouseButton(me)) {
				pantL = map.pixelToTile(me.getX(), me.getY());

			}
			else if (SwingUtilities.isRightMouseButton(me)) {
				pantR = map.pixelToTile(me.getX(), me.getY());
			}
		}

		@Override
		public void mouseReleased(MouseEvent me) {
			// TODO Auto-generated method stub
			if (SwingUtilities.isLeftMouseButton(me)) {
				Point pact = map.pixelToTile(me.getX(), me.getY());
				if (pantL.x == pact.x && pantL.y == pact.y) {
					if (map.tileIsWithinBoard(pact)) {
						   
		        		
		        		map.afegirNum(pact.x, pact.y);
		        	}
		        	
				}
			}
			else if (SwingUtilities.isRightMouseButton(me)) {
				Point pact2 = map.pixelToTile(me.getX(), me.getY());
				if (pantR.x == pact2.x && pantR.y == pact2.y) {
					if (map.tileIsWithinBoard(pact2)) {
						accioClickDret(pact2.x, pact2.y);
					}
				}
			}
			repaint();
		}
		
	});
	map.addMouseMotionListener(new MouseMotionListener() {

		@Override
		public void mouseDragged(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseMoved(MouseEvent me) {
			// TODO Auto-generated method stub
			Point tileCoordinates = map.pixelToTile(me.getX(), me.getY());
			
            if (map.tileIsWithinBoard(tileCoordinates)) {
            	map.setA(tileCoordinates.x);
            	map.setB(tileCoordinates.y); 
            	
            	
            } else {
            	map.setA(-3);
            	map.setB(-3); 
            	
            	
            }
            
            map.preview(tileCoordinates.x, tileCoordinates.y);

        	repaint();

		}
		
	});
	
	
	map.setBounds(83, 62,  map .getPreferredSize().width,map.getPreferredSize().height);
	add(map);
	map.repaint();
	
}

	
	//funcio que fa que no es mogui els labels quan fa un canvi de window
	public void updatee() {
		map.setLayout(null);
		//map.ficarLabels();
		//map.repaint();
	}
	private boolean totOmplert() {
		int[][] b = map.getBoard();
		for (int i = 0; i < b.length; i++) {
			for (int j = 0; j < b[0].length; j++) {
				if (b[i][j] == -1) {
					return false;
				}
			}
		}
		return true;
	}
}
