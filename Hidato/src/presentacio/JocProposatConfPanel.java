package presentacio;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.JComboBox;

public class JocProposatConfPanel extends JPanel {
	private CtrlPresentacio iCtrlPresentacio;
	private VistaPrincipal iVistaPrincipal;
	private JTable table_1;
	private JScrollPane scrollPane_1;
	JPanel auto;
	/**
	 * Create the panel.
	 */
	public JocProposatConfPanel(CtrlPresentacio pCtrlPresentacio, VistaPrincipal pVistaPrincipal) {
		auto = this;
		iCtrlPresentacio = pCtrlPresentacio;
		iVistaPrincipal = pVistaPrincipal;
		this.setPreferredSize(new Dimension(910, 512));
		setLayout(null);
		
		JLabel lblRanking = new JLabel("Joc Proposat");
		lblRanking.setHorizontalAlignment(SwingConstants.CENTER);
		lblRanking.setFont(new Font("Tahoma", Font.PLAIN, 36));
		lblRanking.setBounds(10, 11, 890, 76);
		add(lblRanking);
		
		JButton btnTornarEnrere = new JButton("Sortir");
		btnTornarEnrere.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				iVistaPrincipal.changeJugar();
			}
		});
		btnTornarEnrere.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnTornarEnrere.setBounds(393, 426, 124, 23);
		add(btnTornarEnrere);
		
		 scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(56, 139, 479, 272);
		add(scrollPane_1);
		table_1 = crearTabla("NONE");
		scrollPane_1.setViewportView(table_1);
		
		JLabel lblFiltrarPerAutor = new JLabel("Filtrar per Autor :");
		lblFiltrarPerAutor.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblFiltrarPerAutor.setBounds(587, 134, 104, 23);
		add(lblFiltrarPerAutor);
		
		
		
		JComboBox<String> comboBox = new JComboBox<String>();
		comboBox.setBounds(714, 137, 136, 20);
		comboBox.addItem("NONE");
		ArrayList<String> afegits = iCtrlPresentacio.getAutorsHidatosProposats();
		for (String a : afegits) {
			comboBox.addItem(a);
		}
		comboBox.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent arg0) {
				// TODO Auto-generated method stub
				cambiTable((String) comboBox.getSelectedItem());
			}
			
		});
		add(comboBox);
		
		
		table_1.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {
				// TODO Auto-generated method stub
				if (table_1.getSelectedRow() > -1) {
					/*
					System.out.println("selected row: " + table_1.getSelectedRow());
					int pos = iCtrlPresentacio.findPos((String) table_1.getModel().getValueAt(table_1.getSelectedRow(), 1));
					int[][] tauler = iCtrlPresentacio.crearJocProposat(pos);
					String tipusCela = iCtrlPresentacio.getTipusCelaHidatoProposat(pos);
					//String adj = iCtrlPresentacio.getAdjHidatoProposat(table_1.getSelectedRow());
					iVistaPrincipal.changeJocRapid(tauler, tipusCela ,"NONE", "NONE", "NULL", null);
					*/
				}
			}
			
		});
		
		
		JButton btnJugar = new JButton("Jugar");
		btnJugar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (table_1.getSelectedRow() > -1) {
					System.out.println("selected row: " + table_1.getSelectedRow());
					int pos = iCtrlPresentacio.findPos((String) table_1.getModel().getValueAt(table_1.getSelectedRow(), 1));
					int[][] tauler = iCtrlPresentacio.crearJocProposat(pos);
					String tipusCela = iCtrlPresentacio.getTipusCelaHidatoProposat(pos);
					//String adj = iCtrlPresentacio.getAdjHidatoProposat(table_1.getSelectedRow());
					iVistaPrincipal.changeJocRapid(tauler, tipusCela ,"NONE", "NONE", "NULL", null);
				}
			}
		});
		btnJugar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnJugar.setBounds(660, 182, 89, 23);
		add(btnJugar);
		
		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (table_1.getSelectedRow() > -1) {
					int pos = iCtrlPresentacio.findPos((String) table_1.getModel().getValueAt(table_1.getSelectedRow(), 1));
					
					if (iCtrlPresentacio.borrarPartidaProposat(pos)) {
					table_1 = crearTabla((String)comboBox.getSelectedItem());
					scrollPane_1.setViewportView(table_1);
					}
					else {
						JOptionPane.showMessageDialog(auto, "Nomes es pot borrar hidatos creats pel propi usuari.");
					}
					

				}
			}
		});
		btnBorrar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnBorrar.setBounds(660, 292, 89, 23);
		add(btnBorrar);
		
		
	}
	
	private void cambiTable(String nom) {
		table_1 = crearTabla(nom);
		table_1.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {
				// TODO Auto-generated method stub
				if (table_1.getSelectedRow() > -1) {
					/*
					System.out.println("selected row: " + table_1.getSelectedRow());
					int pos = iCtrlPresentacio.findPos((String) table_1.getModel().getValueAt(table_1.getSelectedRow(), 1));
					int[][] tauler = iCtrlPresentacio.crearJocProposat(pos);
					String tipusCela = iCtrlPresentacio.getTipusCelaHidatoProposat(pos);
					//String adj = iCtrlPresentacio.getAdjHidatoProposat(table_1.getSelectedRow());
					iVistaPrincipal.changeJocRapid(tauler, tipusCela ,"NONE", "NONE", "NULL", null);
					*/
				}
			}
			
		});
		scrollPane_1.setViewportView(table_1);

	}
	
	private JTable crearTabla(String nom) {
		//List<List<String>> aux =  iCtrlPresentacio.getRanking(tipus);

		List<String> aux = iCtrlPresentacio.getHidatosProposats();
		DefaultTableModel modelo3 = new DefaultTableModel(){

		    @Override
		    public boolean isCellEditable(int row, int column) {
		       //all cells false
		       return false;
		    }
		};
		modelo3.addColumn("#");
		modelo3.addColumn("Nom hidato");
		modelo3.addColumn("Autor");
		modelo3.addColumn("Tipus Cel�la");
		modelo3.addColumn("Adjacencia");
		
		JTable table_3 = new JTable();
	
		//modelo3.addRow(new Object[] {"1","cxczdasd","123"});
		int x = 0;
		for (int i = 0; i < aux.size(); i = i+2) {
			if (aux.get(i+1).equals(nom) || nom.equals("NONE")) {
			modelo3.addRow(new Object[] {Integer.toString(i+1), aux.get(i), aux.get(i+1), iCtrlPresentacio.getTipusCelaHidatoProposat(x), iCtrlPresentacio.getAdjHidatoProposat(x)});
			x++;
			}
			
		}
			
	/*
		
		for (int i = 0; i < aux.size(); i++) {
			modelo3.addRow(new Object[] {Integer.toString(i+1), aux.get(i).get(0), aux.get(i).get(1)});
		}
		*/
		table_3.setModel(modelo3);
		table_3.getColumnModel().getColumn(0).setMaxWidth(20);
		table_3.getColumnModel().getColumn(0).setResizable(false);
		table_3.getColumnModel().getColumn(1).setWidth(80);
		table_3.getColumnModel().getColumn(1).setResizable(false);
		table_3.getColumnModel().getColumn(2).setWidth(100);
		table_3.getColumnModel().getColumn(2).setResizable(false);
		table_3.getColumnModel().getColumn(3).setWidth(100);
		table_3.getColumnModel().getColumn(3).setResizable(false);
		table_3.getColumnModel().getColumn(4).setResizable(false);
		table_3.getTableHeader().setReorderingAllowed(false);
		return table_3;
	}
}
