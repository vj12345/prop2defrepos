package presentacio;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class TriBoardPanel extends BoardPanel {

    private int base;
    private int altura;
    

    /*
    //cela seleccionat de la lista
    int sa = -3;
    int sb = -3;
    */



	/**
	 * Create the panel.
	 */
	public TriBoardPanel(int width, int height, int base) {
		
		
		super(width, height, 22,12);
        this.base = base*2;

        altura = (int) ((this.base/2)*Math.tan(Math.PI/3));
	}
	
	public TriBoardPanel(int[][] solu, int width, int height ,int base, JPanel parent, int[][] t_guardat) {
		super(solu,width, height, 22,12, parent, t_guardat);
		this.base = base*2;
		altura = (int) ((this.base/2)*Math.tan(Math.PI/3));
	}
	
	
	 @Override
	    protected void paintComponent(Graphics g) {
	        super.paintComponent(g);
	        Graphics2D g2d = (Graphics2D) g;
	        System.out.println("widht: " + width + " height: " + height);
	        System.out.println("boardwidth: " + super.getBoard().length + " boarhei: " + super.getBoard()[0].length);

	        for (int i = 0; i < width; i++) {
	            for (int j = 0; j < height; j++) {
	            	
	            	if (super.getBoard()[j][i] == -2) {
	            		Polygon aux = buildPolygon(i,j);
		                g2d.setColor(Color.BLACK);
		                g2d.fillPolygon(aux);
		                g2d.drawPolygon(aux);
		                
	            	}
	            	else if (super.getBoard()[j][i] == -3 && super.editor) {
	            		Polygon aux = buildPolygon(i,j);
		                g2d.setColor(Color.RED);
		                g2d.fillPolygon(aux);
		                g2d.drawPolygon(aux);
		                
	            	}
	            	else {
	            		if ((!super.editor && super.getBoard()[j][i] != -3) || super.editor) {
	            			if(super.ficatsPerDefecte.contains(super.getBoard()[j][i])) {
	            				Polygon aux = buildPolygon(i,j);
				                g2d.setColor(Color.GRAY);
				                g2d.fillPolygon(aux);
				                g2d.drawPolygon(aux);
	            			}
	            			else {
			            		Polygon aux = buildPolygon(i,j);
				                g2d.setColor(Color.orange);
				                g2d.fillPolygon(aux);
				                g2d.drawPolygon(aux);
	            			}
	            		}
		                
	            	}
	            	
	            	
	                if ((!super.editor && super.getBoard()[j][i] != -3) || super.editor) {
	                g2d.setStroke(new BasicStroke(2));
	            	Polygon p = buildPolygon(i,j);
	            	g2d.setColor(Color.CYAN);
	            	//g2d.fillPolygon(p);
	                g2d.drawPolygon(p);
	                }
	                
	            }
	        }
	    }
	    
	    public void paint(Graphics g) {
	    	super.paint(g);
	        Graphics2D g2d = (Graphics2D) g;
	     
	        g2d.setStroke(new BasicStroke(3));
	    	g2d.setColor(Color.GREEN);
	        g2d.drawPolygon(buildPolygon(a, b));
	        g2d.setColor(Color.BLACK);
	        //g2d.drawPolygon(buildHexagon(sa, sb));

	       
	    }


	    @Override
	    public Dimension getPreferredSize() {
	        int panelWidth =        width * base + 1;

	        int panelHeight =  height * altura +1;
	        return new Dimension(panelWidth, panelHeight);
	    }
	 
	    Polygon buildPolygon(int column, int row) {
	        Polygon tri = new Polygon();
	        Point origin = tileToPixel(column, row);
	        if ((row % 2 == 0 && column % 2 == 0) || (row % 2 != 0 && column % 2 != 0)) {
		        tri.addPoint(origin.x+base/2, origin.y);
		        tri.addPoint(origin.x, origin.y + altura);
		        tri.addPoint(origin.x + base, origin.y + altura);
	        }
	        else {
	        	tri.addPoint(origin.x, origin.y);
		        tri.addPoint(origin.x + base, origin.y);
		        tri.addPoint(origin.x + base/2, origin.y + altura);
	        }
	        return tri;
	    }
	 
	    Point tileToPixel(int column, int row) {
	        Point pixel = new Point();
	        pixel.x = column * base/2;
	        pixel.y = row * altura;
	        return pixel;
	    }
	 
	    Point pixelToTile(int x, int y) {
	    
	        Point p = new Point(x / (base/2), y / altura);
	        Point r = new Point(x % (base/2), y % altura);
	        
	        if ((p.y % 2 == 0 && p.x % 2 == 0) || (p.y % 2 != 0 && p.x % 2 != 0)) { //triangles amb punta a dalt
	        	if (r.y < ((-altura*1.0/(base/2)*r.x) + altura)) {
	        		return new Point (p.x-1, p.y);
	        	}
	        	else {
	        		return new Point (p.x, p.y);
	        	}
	            
	        } else { // triangles amb punta a baix

	        	if (r.y > (altura*1.0/(base/2) *r.x)) {
	        		return new Point(p.x-1, p.y);
	        	}
	        	else {
	        		return new Point(p.x, p.y);
	        	}
	     
	        }
	    }
	 

 
	    
}
