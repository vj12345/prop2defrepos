package presentacio;

import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingConstants;
import javax.swing.JButton;

public class InstruccionsPanel extends JPanel {
	private CtrlPresentacio iCtrlPresentacio;
	private VistaPrincipal iVistaPrincipal;
	/**
	 * Create the panel.
	 */
	public InstruccionsPanel(CtrlPresentacio pCtrlPresentacio, VistaPrincipal pVistaPrincipal) {
		iCtrlPresentacio = pCtrlPresentacio;
		iVistaPrincipal = pVistaPrincipal;
		setLayout(null);
		this.setPreferredSize(new Dimension(910, 512));
		
		JLabel lblInstruccions = new JLabel("Instruccions");
		lblInstruccions.setHorizontalAlignment(SwingConstants.CENTER);
		lblInstruccions.setFont(new Font("Tahoma", Font.PLAIN, 36));
		lblInstruccions.setBounds(10, 54, 890, 76);
		add(lblInstruccions);
		
		JLabel lblJaHoSaps = new JLabel("<html>Hidato �s un joc individual creat per Gyora Benedek, un matem�tic israeli�.<br>"
				+ "Es parteix d�un tauler de cel�les. El tauler pot tenir qualsevol forma compatible amb<br>"
				+ "el tipus de cel�la i estar� dividit en cel�les del mateix tipus. <br><br>"
				+ "Les cel�les negres son �forats�.<br><br>"
				+ "Donats uns quants nombres inicials ja col�locats, l'objectiu �s completar el tauler<br>"
				+ "amb una s�rie de nombres consecutius adjacents entre ells.</html>");
		lblJaHoSaps.setVerticalAlignment(SwingConstants.TOP);
		lblJaHoSaps.setBounds(97, 162, 700, 400);
		add(lblJaHoSaps);
		
		JButton btnTronarEnrere = new JButton("Tornar enrere");
		btnTronarEnrere.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				iVistaPrincipal.changeMenu();
			}
		});
		btnTronarEnrere.setBounds(374, 390, 161, 23);
		add(btnTronarEnrere);

	}

}
