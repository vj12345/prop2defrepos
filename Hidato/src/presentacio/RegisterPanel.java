package presentacio;

import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;

import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JButton;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.FormSpecs;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class RegisterPanel extends JPanel {

	private CtrlPresentacio iCtrlPresentacio;
	private VistaPrincipal iVistaPrincipal;
	
	private JTextField textField;
	private JPasswordField passwordField;
	private JPasswordField passwordField_1;
	private JLabel lblFasdfasdfasdfasdfasdf;
	/**
	 * Create the panel.
	 */
	public RegisterPanel(CtrlPresentacio pCtrlPresentacio, VistaPrincipal pVistaPrincipal) {
		iCtrlPresentacio = pCtrlPresentacio;
		iVistaPrincipal = pVistaPrincipal;
		
		this.setPreferredSize(new Dimension(910,512));
		setLayout(null);
		textField = new JTextField();
		textField.setBounds(480, 156, 129, 20);
		add(textField);
		textField.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(480, 209, 129, 20);
		add(passwordField);
		
		passwordField_1 = new JPasswordField();
		passwordField_1.setBounds(480, 264, 129, 20);
		add(passwordField_1);
		
		lblFasdfasdfasdfasdfasdf = new JLabel("Registrar");
		lblFasdfasdfasdfasdfasdf.setHorizontalAlignment(SwingConstants.CENTER);
		lblFasdfasdfasdfasdfasdf.setBounds(10, 54, 890, 76);
		lblFasdfasdfasdfasdfasdf.setFont(new Font("Tahoma", Font.PLAIN, 36));
		add(lblFasdfasdfasdfasdfasdf);
		
		JLabel lblNewLabel = new JLabel("Usuari :");
		lblNewLabel.setBounds(284, 156, 138, 20);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Contrasenya :");
		lblNewLabel_1.setBounds(284, 209, 138, 20);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Confirmar contrasenya :");
		lblNewLabel_2.setBounds(284, 264, 160, 20);
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		add(lblNewLabel_2);
		
		JButton btnAccept = new JButton("Acceptar");
		btnAccept.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnAccept.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String user = textField.getText();
				String psw = new String(passwordField.getPassword());
				String psw1 = new String(passwordField_1.getPassword());
				if (textField.getText().isEmpty()) {
					JOptionPane.showMessageDialog(iVistaPrincipal, "El camp usuari no pot ser buit");
				}
				else if (psw.length() == 0) {
					JOptionPane.showMessageDialog(iVistaPrincipal, "La contrasenya no pot ser buida");
				}
				else if (!psw.equals(psw1)) {
					JOptionPane.showMessageDialog(iVistaPrincipal, "Les contrasenyes no coincideixen");
				}
				else {
					boolean valid = iCtrlPresentacio.register(user, psw);
					if (valid) {
						 JOptionPane.showMessageDialog(iVistaPrincipal, "El registre s'ha completat correctament");
						 iVistaPrincipal.changeToLogin();
					}
					else {
						 JOptionPane.showMessageDialog(iVistaPrincipal, "L'usuari ja existeix");
					}
				}
			}
		});
		btnAccept.setBounds(519, 335, 90, 23);
		add(btnAccept);
		
		JButton btnNewButton = new JButton("Enrere");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				iVistaPrincipal.changeToLogin();
			}
		});
		btnNewButton.setBounds(325, 335, 90, 23);
		add(btnNewButton);
		
	}
}
