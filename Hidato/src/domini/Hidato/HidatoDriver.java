package domini.Hidato;
import domini.Hidato.Hidato.Topologia;
import domini.Hidato.Hidato.Adjacencia;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;



public class HidatoDriver {
	
	private void printHid(int [][] hid) {
		for (int i = 0; i < hid.length; i++) {
			for (int j = 0; j < hid[0].length; j++) {
				System.out.print(hid[i][j] + " ");
			}
			System.out.println();
		}
	}
	/*
Q,CA,5,5 
#,#,1,#,# 
#,?,*,?,# 
8,?,?,?,3 
#,?,11,*,# 
#,#,?,#,# 
*/
	private int [][]tablero = {{-3,-3,1,-3,-3},
			{-3,-1,-2,-1,-3},
			{8,-1,-1,-1,3},
			{-3,-1,11,-2,-3},
			{-3,-3,-1,-3,-3}};
	
	/*
H,C,4,3 
#,*,? 
?,?,* 
1,?,8 
?,?,#
	 * */
	private int [][]tablero2 = {
			{-3,-1,-1},
			{-1,-1,-1},
			{1,-1,8},
			{-1,-1,-3}};
	
	/*
T,C,5,7 
#,#,#,#,#,#,# 
#,#,#,*,#,#,# 
#,#,?,1,?,?,? 
#,?,?,16,?,?,? 
*.?,?,7,?,?,* 
**/
	
	private int [][] tablero3 = {
			{-3,-3,-3,-3,-3,-3,-3},
			{-3,-3,-3,-2,-3,-3,-3},
			{-3,-3,-1,1,-1,-1,-1},
			{-3,-1,-1,16,-1,-1,-1},
			{-2,-1,-1,7,-1,-1,-2},
	};
	
	private int [][] tablero4 = {
			{1,-1,35,-1,-1,-1,-1,30,-1,-1},
			{-1,-1,-1,-1,-1,-1,60,-1,-1,-1},
			{-1,-1,65,-1,-1,-1,-1,80,-1,-1},
			{-1,-1,-1,85,-1,95,-1,-1,-1,25},
			{5,40,-1,-1,-1,-1,-1,-1,55,-1},
			{-1,-1,-1,-1,100,-1,-1,-1,-1,-1},
			{-1,-1,-1,-1,-1,90,-1,-1,-1,-1},
			{-1,-1,70,-1,-1,-1,-1,75,-1,-1},
			{-1,-1,45,-1,-1,-1,-1,50,-1,20},
			{10,-1,-1,-1,-1,15,-1,-1,-1,-1},
	};
	
	private int [][] t5 = {
			{1,-1,-1,-3,-1},
			{-1,-1,-1,-3,-1},
			{-1,-1,-1,-3,-1},
			{-1,-1,-1,-3,-1},
			{-1,-1,-1,-3,25}

	};
	private int [][] t6 = {
			{1,-1,-1,-1,-1},
			{-1,-1,-1,-1,-1},
			{-1,-1,-1,-1,-1},
			{1,-1,-1,-1,-1},
			{-1,-1,-1,-1,25}

	};
	
	
	
	public void testGenerator() {
/*
		// per provar el generator
		HidatoH t = new HidatoH(Adjacencia.C, "cc", Topologia.QUADRAT, 0, 25, 2);
		if (t.generator()) {
			System.out.println("Hidato generat:");
			printHid(t.getTablero());
			System.out.println("Solucio:");
			printHid(t.getTablero_solucionado());	
		}
		else {
			System.out.println("no s'ha pugut generar");
		}
		
		*/
		try{
			Scanner linea=new Scanner(System.in);
			int opcio;
			System.out.println("Benvolgut al test del generator");
			
			HidatoQ hq;
			HidatoT ht;
			HidatoH hh;
			String  nomh, cela;
			int nceles, nforats, ndefecte;
			Adjacencia adj;
			Topologia top = Topologia.OTHERS;
				
				System.out.println("Introduiu nom Hidato:");
				nomh = linea.nextLine();
				System.out.println("Selecciona el tipu de cela:");
				System.out.println("Selecciona la opció que vols efectuar amb el numero");
				System.out.println("1 -> Celes triangulars");
				System.out.println("2 -> Celes quadrades");
				System.out.println("3 -> Celes hexagonals");
				opcio = linea.nextInt();
				linea.nextLine();
				
				if(opcio == 1) {
					cela = "T";
				}
				else if(opcio == 2) {
					cela = "Q";
				}
				else if (opcio == 3) {
					cela = "H";
				}
				else {
					System.out.println("format tipus cela incorrecte ha de ser " + "1" +"(triangle), " + "2" +" (quadrat), o " + "3" + "(hexagonal)");
					return;
				}
				
				System.out.println("Introduiu tipus de adjacencia (C, CA):");
				System.out.println("Selecciona la opció que vols efectuar amb el numero");
				System.out.println("1 -> Adjacencia de costat");
				System.out.println("2 -> Adjacencia de costat i vertex");
			
				opcio = linea.nextInt();
				linea.nextLine();
				
				if(opcio == 1) {
					adj = Adjacencia.C;
				}
				else if(opcio == 2) {
					adj = Adjacencia.CA;
				}
				else {
					System.out.println("format tipus adjacencia incorrecte ha de ser " + "C" +"(costat), " + "CA" +" (costat i aresta)");
					return;
				}
				
				
				if (cela == "T") {
					System.out.println("Introduiu tipus de topologia:");
					System.out.println("1 -> topologia triangular");
					opcio = linea.nextInt();
					linea.nextLine();
					if (opcio != 1) {
						System.out.println("de moment per les celes triangulars nomes tenim topologia triangular");
						return;
					}
					else {
						top = Topologia.TRIANGLE;
					}
				}
				else if (cela == "Q") {
					System.out.println("Introduiu tipus de topologia:");
					System.out.println("1 -> topologia quadrada");
					opcio = linea.nextInt();
					linea.nextLine();
					if (opcio != 1) {
						System.out.println("de moment per les celes quadrades nomes tenim topologia quadrada");
						return;
					}
					else {
						top = Topologia.QUADRAT;
					}
				}
				else if (cela == "H") {
					System.out.println("Introduiu tipus de topologia:");
					System.out.println("1 -> topologia quadrada");
					opcio = linea.nextInt();
					linea.nextLine();
					if (opcio != 1) {
						System.out.println("de moment per les celes hexagonals nomes tenim topologia quadrada");
						return;
					}
					else {
						top = Topologia.QUADRAT;
					}
				}
				
				System.out.println("Introduiu el numero de celes totals (minim 9), si es topologia triangular o"
						+ "cuadrada l'arrel quadrada del numero de celes totals ha de ser enter, per exemple, 4,"
						+ "9, 16, 25, 36,...: ");
				nceles = linea.nextInt();
				if (nceles < 9) {
					System.out.println("el nimbre minim de cels ha de ser 9");
					return; 
				}
				linea.nextLine();
				
				System.out.println("Introduiu el numero de numeros per defecte (minim 2): ");
				ndefecte = linea.nextInt();
				if (ndefecte < 2) {
					System.out.println("el nmmbre minim de numeros ha de ser 2 (inicial i final)");
					return; 
				}
				linea.nextLine();
				
				System.out.println("Introduiu el numero de numeros de forats(maxim 1 per cada 9 celes, excepte"
						+ " si son celes triangulars amb topologia triangular i adjacencia de costat, llavors el minim es 3, maxim es"
						+ "3 + 1 per cada 9 celes): ");
				nforats = linea.nextInt();
				if (nforats > nceles/9+3) {
					System.out.println("masses forats");
					return; 
				}
				linea.nextLine();
				
				if (cela == "Q") {
					hq = new HidatoQ(adj, nomh, top, nforats, nceles, ndefecte);
					if (hq.generator()) {
						System.out.println("Hidato generat: ");
						printHid(hq.getTablero());
						
						System.out.println("Solucio:  ");
						printHid(hq.getTablero_solucionado());

						
					}
					else {
						
						System.out.println("No s'ha pogut generar");
					
					}
				}
				else if (cela == "T") {
					ht = new HidatoT(adj, nomh,top, nforats, nceles, ndefecte);
					if (ht.generator()) {
						System.out.println("Hidato generat: ");
						printHid(ht.getTablero());
						System.out.println("Solucio:  ");
						printHid(ht.getTablero_solucionado());

					}
					else {
						
						System.out.println("No s'ha pogut generar");
					
					}
				}
				else if (cela == "H") {
					hh = new HidatoH(adj, nomh, top, nforats, nceles, ndefecte);
					if (hh.generator()) {
						System.out.println("Hidato generat:");
						printHid(hh.getTablero());
						
						System.out.println("Solucio:  ");
						printHid(hh.getTablero_solucionado());

					}
					else {
						
						System.out.println("No s'ha pogut generar");
					
					}
				}
			
				
			
		}
		catch (Exception e) {
			System.out.println("Hi ha hagut una excepció en el solver");
		}
		
	}
	
	public void testSolver() {
		
		/*Nou hidato amb adjacencia de costat i celes triangulars que esta en el tauler 3
		 * per provar altres hidatos s'ha de cambiar el HidatoT per 
		 * HidatoQ si es de celes quadrades, HidatoH si es de celes hexagonals
		 * cambiar el tablero3 pel taurell corresponen que el podra trobar 
		 * a dins de testHidato.txt
		 * Per cambiar de adjacencia a costat i vertex cal cambiar Adjacencia.C per
		 * Adjacencia.CA
		 * */
		/*
		HidatoT hid = new HidatoT(Adjacencia.CA, tablero3, "hid1", Topologia.OTHERS);
		if (hid.solver()) {
			System.out.println("siisfdfsdf");
			printHid(hid.getTablero_solucionado());
			
			System.out.println("ori");
			printHid(hid.getTablero());


		}
		else {
			
			System.out.println("nooo");
			printHid(hid.getTablero_solucionado());
		}
		*/
		
		try{
			Scanner linea=new Scanner(System.in);
			int opcio;
			System.out.println("Benvolgut al test del solver");
			
			HidatoQ hq;
			HidatoT ht;
			HidatoH hh;
			String  nomh, cela;
			Adjacencia adj;
			Topologia top = Topologia.OTHERS;
			
				System.out.println("Introduiu nom Hidato:");
				nomh = linea.nextLine();
				System.out.println("Selecciona el tipu de cela:");
				System.out.println("Selecciona la opció que vols efectuar amb el numero");
				System.out.println("1 -> Celes triangulars");
				System.out.println("2 -> Celes quadrades");
				System.out.println("3 -> Celes hexagonals");
				opcio = linea.nextInt();
				linea.nextLine();
				
				if(opcio == 1) {
					cela = "T";
				}
				else if(opcio == 2) {
					cela = "Q";
				}
				else if (opcio == 3) {
					cela = "H";
				}
				else {
					System.out.println("format tipus cela incorrecte ha de ser " + "1" +"(triangle), " + "2" +" (quadrat), o " + "3" + "(hexagonal)");
					return;
				}
				
				System.out.println("Introduiu tipus de adjacencia (C, CA):");
				System.out.println("Selecciona la opció que vols efectuar amb el numero");
				System.out.println("1 -> Adjacencia de costat");
				System.out.println("2 -> Adjacencia de costat i vertex");
			
				opcio = linea.nextInt();
				linea.nextLine();
				
				if(opcio == 1) {
					adj = Adjacencia.C;
				}
				else if(opcio == 2) {
					adj = Adjacencia.CA;
				}
				else {
					System.out.println("format tipus adjacencia incorrecte ha de ser " + "C" +"(costat), " + "CA" +" (costat i aresta)");
					return;
				}
				
				
				if (cela == "T") {
					System.out.println("Introduiu tipus de topologia:");
					System.out.println("1 -> topologia triangular");
					opcio = linea.nextInt();
					linea.nextLine();
					if (opcio != 1) {
						System.out.println("de moment per les celes triangulars nomes tenim topologia triangular");
						return;
					}
					else {
						top = Topologia.TRIANGLE;
					}
				}
				else if (cela == "Q") {
					System.out.println("Introduiu tipus de topologia:");
					System.out.println("1 -> topologia quadrada");
					opcio = linea.nextInt();
					linea.nextLine();
					if (opcio != 1) {
						System.out.println("de moment per les celes quadrades nomes tenim topologia quadrada");
						return;
					}
					else {
						top = Topologia.QUADRAT;
					}
				}
				else if (cela == "H") {
					System.out.println("Introduiu tipus de topologia:");
					System.out.println("1 -> topologia quadrada");
					opcio = linea.nextInt();
					linea.nextLine();
					if (opcio != 1) {
						System.out.println("de moment per les celes hexagonals nomes tenim topologia quadrada");
						return;
					}
					else {
						top = Topologia.QUADRAT;
					}
				}
				
				System.out.println("Introduiu el numero de files del tauler (>2):");
				int fila = linea.nextInt();
				linea.nextLine();
				System.out.println("Introduiu el numero de columnes del tauler (>2):");
				int columna = linea.nextInt();
				linea.nextLine();
				System.out.println("Els # son -3, els * son -2, els _ son -1, el tauler ha de estar en un format correcte,"
						+ "i com a minim te que tenir el valor inicial i final");
				int [][] tauler = new int [fila] [columna];
				for (int i = 0; i < fila ; i++) {
					for (int j = 0; j < columna; j++) {
						System.out.println("Introduiu l'element de la posicio [" + i +" ][" + j +"] del tauler");
						tauler[i][j] = linea.nextInt();
						linea.nextLine();
					}
				}
				System.out.println("El tauler introduit es el seguent: "); 
				printHid(tauler);
				if (cela == "Q") {
					hq = new HidatoQ(adj, tauler, nomh, top);
					if (hq.solver()) {
						System.out.println("Hidato solucionat: ");
						printHid(hq.getTablero_solucionado());
					}
					else {
						
						System.out.println("Hidato sense solucio");
					
					}
				}
				else if (cela == "T") {
					ht = new HidatoT(adj, tauler, nomh, top);
					if (ht.solver()) {
						System.out.println("Hidato solucionat: ");
						printHid(ht.getTablero_solucionado());
					}
					else {
						
						System.out.println("Hidato sense solucio");
					
					}
				}
				else if (cela == "H") {
					hh = new HidatoH(adj, tauler, nomh, top);
					if (hh.solver()) {
						System.out.println("Hidato solucionat:");
						printHid(hh.getTablero_solucionado());
					}
					else {
						
						System.out.println("Hidato sense solucio");
					
					}
				}
			
				
			
		}
		catch (Exception e) {
			System.out.println("Hi ha hagut una excepció en el solver");
		}
		
		
	}
	
	public static void main(String[] args) {
		
		
		// TODO Auto-generated method stub
		HidatoDriver h = new HidatoDriver();
		try{
			Scanner linea=new Scanner(System.in);
			int opcio;
			System.out.println("Benvolgut al Driver del Hidato");
			while(true){
				System.out.println("Selecciona la opció que vols efectuar amb el numero");
				System.out.println("1 -> Provar Solver");
				System.out.println("2 -> Provar generator");
				opcio = linea.nextInt();
				linea.nextLine();
				if(opcio == 1) {
					h.testSolver();
				}
				if(opcio == 2) {
					h.testGenerator();
				}
				
			}
		}
		catch (Exception e) {
			System.out.println("Hi ha hagut una excepció en el driver hidato");
		}
	}

	private static int[] random(int n) {
		int [] v = new int[n]; 
		Random rnd = new Random();
		ArrayList<Integer> jaPosats = new ArrayList<Integer>(n);
		for (int i = 0; i < n; i++) {
			Integer x = rnd.nextInt(n);
			if (!jaPosats.contains(x)) {
				v[i] = x;
				jaPosats.add(x);
			}
			else {
				i--;
			}
		}
		return v;
	}
}
