package domini.Hidato;

import java.util.ArrayList;
import java.util.Random;
import java.lang.Math;

/* IMPORTANT: La explicació més detallada esta en la classe HidatoT, que es semblant a aquesta.
 * */

public class HidatoQ extends Hidato{
	
	
	/**
	 * Metode constructor que utilitza el constructor de la superclasse
	 * @param adj
	 * @param taurell
	 * @param nomH
	 * @param t
	 */
	public HidatoQ(Adjacencia adj, int [][]taurell, String nomH, Topologia t) {
		super(adj, taurell, nomH, t);
	}
	
	/**
	 * Metode constructor que utilitza el constructor de la superclasse
	 * @param adj
	 * @param nomH
	 * @param t
	 * @param forats
	 * @param celes_totals
	 * @param co
	 */
	public HidatoQ(Adjacencia adj, String nomH, Topologia t, int forats, int celes_totals, int co) {
		super(adj, nomH, t, forats, celes_totals, co);
		
	}
	
	/**
	 * Metode que busca el punt d'inici i utilitzant un vector amb els valors que es necessiten intenta solucionar l'hidato
	 * cridant a solverC
	 */
	public boolean solver() {

		
		row = super.tablero_solucionado.length;
		col = super.tablero_solucionado[0].length;
		
		ArrayList<Integer> list = new ArrayList<Integer>(row*col);
		start = new int[] {-1,-1};

		for(int i = 0; i < row; i++) {
			 for(int j = 0; j < col; j++) {
				 if (super.tablero_solucionado[i][j] != -2 && super.tablero_solucionado[i][j] != -1 && super.tablero_solucionado[i][j] != -3) {
					 list.add(super.tablero_solucionado[i][j]);
					 if (super.tablero_solucionado[i][j] == 1) {
						 start = new int[] {i, j};
					 }
				 }
			 }
		}
		
		if (start[0] == -1 || start[1] == -1) {
			return false;
		}
		
		list.sort(null);
		given = new int[list.size()];
		
		for (int i = 0; i < given.length; i++) {
			given[i] = (int) list.get(i);
		}
		if (given[given.length-1] < (super.getnCeles()-super.getForats())) {
			return false;
		}
		
		return solverC(start[0],start[1],1,0);
	}
	
	/**
	 * Metode que resol l'hidato guardat en el tablero_solucionado de forma recursiva aplicant dfs amb backtracking.
	 * @param x
	 * @param y
	 * @param valor
	 * @param givenpos
	 * @return boolea per indicar si s'ha creat amb exit o no
	 */
	private boolean solverC(int x, int y, int valor, int givenpos) {
		
	
		if (valor > given[given.length-1]) {
			return true;
		}
		
		if ( x < 0 || x > row-1 || y > col-1 || y < 0 || tablero_solucionado[x][y] == -2 || tablero_solucionado[x][y] == -3) {
			return false;
		}
		
		if (super.tablero_solucionado[x][y] == -1 && valor >= given[givenpos]) {
			return false;
		}
		
		if (super.tablero_solucionado[x][y] != -1 && super.tablero_solucionado[x][y] != valor) {
			return false;
		}
	
		if (super.tablero_solucionado[x][y] == given[givenpos]) {
			givenpos++;
		}
		
		int valor_ant = super.tablero_solucionado[x][y];
		super.tablero_solucionado[x][y] = valor;
		/*Per les celes quadrades, en l'adjacencia de costat nomes te d' adjacent les celes dels quatre costats,
		 * i per l'adjacencia de costat i vertex te de adjacent les celes de les 8 direccions.
		 * */
		if (adj == Adjacencia.C) {
			for (int i = -1; i < 2; i++) {
				if (solverC(x+i, y, valor+1, givenpos)) {
					return true;
				}
			}
	
			for (int i = -1; i < 2; i++) {
				if (solverC(x, y+i, valor+1, givenpos)) {
					return true;
				}
			}
		}
		
		else {
			for (int i = -1; i < 2; i++) {
				for (int j = -1; j < 2; j++) {
					if (solverC(x+i, y+j, valor+1, givenpos)) {
						return true;
					}
				}
			}
		}
		

		super.tablero_solucionado[x][y] = valor_ant;
		
		return false;
	}

	/**
	 * Metode que genera a partir de valors aleatoris i dfs amb backtracking
	 */
	public boolean generator() {
		if (!generarTauler()) {
			return false;
		}
		if (nCasellesOmplertes < 2) {
			return false;
		}
		if (generatorQ()) {
			tablero = new int[row][col];
			for (int i = 0; i < row; i++) {
				for (int j = 0; j < col; j++) {
					tablero[i][j] = tablero_solucionado[i][j];
				}
			}
			treureCeles();
			return true;
		}
		return false;
	}
	
	/**
	 * Metode que genera a partir de valors aleatoris i dfs amb backtracking
	 */
	private boolean generatorQ(){
		
	
		int x = tablero_solucionado.length;
		int y = tablero_solucionado[0].length;
		int a = 0;
		int b = 0;
		boolean trob = false;
		while(!trob) {
			iniCeldasVacias(tablero_solucionado);
			
			Random rnd = new Random();
			
			int i = 0;
			
			while (i < forats) {
				 a = rnd.nextInt(x);
				 b = rnd.nextInt(y);
				 if (tablero_solucionado[a][b] != -2 && tablero_solucionado[a][b] != -3) {
					 tablero_solucionado[a][b] = -2;
					 if (adj == Adjacencia.C) {
						 if (comQC(tablero_solucionado, a, b)) {
							 i++;
						 }
						 else {
							 tablero_solucionado[a][b] = -1;
						 }
					 }
					 else {
						 if (comQCA(tablero_solucionado, a, b)) {
							 i++;
						 }
						 else {
							 tablero_solucionado[a][b] = -1;
						 }
					 }
				 }
			}	
			
			int valor_final = super.nCeles - super.forats;
	
				
			boolean trobat = false;

			while (!trobat) {
				a = rnd.nextInt(x);
				b = rnd.nextInt(y);
				if (tablero_solucionado[a][b] == -1) {
					 tablero_solucionado[a][b] = 1;
					 trobat = true;
				}
			}
			
			
			given = new int[] {1, valor_final};
			row = tablero_solucionado.length;
			col = tablero_solucionado[0].length;
			if (buscarCami(a,b,1,valor_final)) {

				return true;
				 
			}
		
		}
		
		return false;
	}
	
	/**
	 * Metode utilitzat per generator per buscar un cami que ompli totes les celes
	 * @param x
	 * @param y
	 * @param valor
	 * @param vf
	 * @return boolea per indicar si s'ha trobat el cami amb exit o no
	 */
	private boolean buscarCami(int x, int y, int valor, int vf) {
		
		if (valor > vf) {
			return true;
		}
		
		if ( x < 0 || x > row-1 || y > col-1 || y < 0 || tablero_solucionado[x][y] == -2 || tablero_solucionado[x][y] == -3) {
			return false;
		}

		
		if (super.tablero_solucionado[x][y] != -1 && super.tablero_solucionado[x][y] != valor) {
			return false;
		}
		
		int valor_ant = super.tablero_solucionado[x][y];
		super.tablero_solucionado[x][y] = valor;
		/*Per les celes quadrades, en l'adjacencia de costat nomes te d' adjacent les celes dels quatre costats,
		 * i per l'adjacencia de costat i vertex te de adjacent les celes de les 8 direccions.
		 * */
		if (adj == Adjacencia.C) {
			int [] seq = random(4);
			
			for (int i = 0; i < seq.length; i++) {
				if (seq[i] == 0) {
					if (buscarCami(x, y-1, valor+1, vf)) {
						return true;
					}
				}
				else if (seq[i] == 1) {
					if (buscarCami(x, y+1, valor+1, vf)) {
						return true;
					}
				}
				else if (seq[i] == 2) {
					if (buscarCami(x-1, y, valor+1, vf)) {
						return true;
					}
				}
				else if (seq[i] == 3) {
					if (buscarCami(x+1, y, valor+1, vf)) {
						return true;
					}
				}
			}
			
		}
		
		else {
			int [] seq = random(4);
			
			for (int i = 0; i < seq.length; i++) {
				if (seq[i] == 0) {
					if (buscarCami(x, y-1, valor+1, vf)) {
						return true;
					}
					if (buscarCami(x-1, y-1, valor+1, vf)) {
						return true;
					}
				}
				else if (seq[i] == 1) {
					if (buscarCami(x, y+1, valor+1, vf)) {
						return true;
					}
					if (buscarCami(x+1, y-1, valor+1, vf)) {
						return true;
					}
				}
				else if (seq[i] == 2) {
					if (buscarCami(x-1, y, valor+1, vf)) {
						return true;
					}
					if (buscarCami(x-1, y+1, valor+1, vf)) {
						return true;
					}
				}
				else if (seq[i] == 3) {
					if (buscarCami(x+1, y, valor+1, vf)) {
						return true;
					}
					if (buscarCami(x+1, y+1, valor+1, vf)) {
						return true;
					}
				}
			}
		}
		

		super.tablero_solucionado[x][y] = valor_ant;
		
		return false;
	}
	
	/**
	 * Genera el tauler depenent de la topologia indicada
	 * @return boolea que indica si s'ha creat amb exit o no
	 */
	private boolean generarTauler() {
		//tauler quadrat
		if (top == Topologia.QUADRAT) {
			int x = (int) Math.sqrt(super.nCeles);
			tablero_solucionado = new int [x][x];
			return true;
		}
		return false;
	}
	
	


	
	//per celes quadrades amb adjacencai de costats 
	//comprova que totes les celes que estiguin buides adjacents al forat(x,y) tinguin almenys dos celes buides com a adjacent.
	private boolean comQC(int[][] h, int x, int y ) {
		for (int i = -1 ; i < 2; i++) {
			for (int j = -1; j < 2; j++) {
				//per cada cela de les quatre costats adjacents que no siguin forats i que estigui dins del tauler
				if (i*i != j*j && x+i >= 0 && x+i < h.length && y+j >= 0 && y+j < h.length && h[x+i][y+j] != -2) {
					int count = 0;
					int a = x+i;
					int b = y+j;
					for (int k = -1 ; k < 2; k++) {
						for (int l = -1; l < 2; l++) {
							if (k*k != l*l && a+k >= 0 && a+k < h.length && b+l >= 0 && b+l < h.length) {
								if (h[a+k][b+l] == -1) {
									count++;
								}
							}
						}
					}
					if (count < 2) {
						return false;
					}
				}
			}
		}
		return true;
	}
	
	//per celes quadrades amb adjacencai de costats i aresta
		//comprova que totes les celes que estiguin buides adjacents al forat(x,y) tinguin almenys dos celes buides com a adjacent.
		private boolean comQCA(int[][] h, int x, int y ) {
			for (int i = -1 ; i < 2; i++) {
				for (int j = -1; j < 2; j++) {
					if (!(i == 0 && j == 0) && x+i >= 0 && x+i < h.length && y+j >= 0 && y+j < h.length && h[x+i][y+j] != -2) {
						int count = 0;
						int a = x+i;
						int b = y+j;
						for (int k = -1 ; k < 2; k++) {
							for (int l = -1; l < 2; l++) {
								if (!(i == 0 && j == 0) && a+k >= 0 && a+k < h.length && b+l >= 0 && b+l < h.length) {
									if (h[a+k][b+l] == -1) {
										count++;
									}
								}
							}
						}
						if (count < 2) {
							return false;
						}
					}
				}
			}
			return true;
		}
	
}


