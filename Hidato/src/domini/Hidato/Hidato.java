package domini.Hidato;

import java.util.ArrayList;
import java.util.Random;

/**
 * Hidato es la classe que representa un hidato com a tal.
 * Conte el tauler original junt amb un possible solucio per
 * si l'usuari demana ajuda. Altres atributs son l'adjacencia
 * de l'hidato, la topologia, el numero de celes totals, plenes 
 * i buides i el nom, per poder resoldre be els metodes de
 * creacio de la instancia, de generacio d'hidatos i de resolucio 
 * d'hidatos
 * @author Qiong Kai Ye, Pau Juli� Plana, Mohamed Chait
 *
 */

public abstract class Hidato {

	protected int [] given, start;
	protected int row, col;
	
	public enum Adjacencia {
		C, CA
	}

	public enum Topologia {
		TRIANGLE, QUADRAT, RECTANGLE, OTHERS
	}
	//tablero con el hidato para resolver
	protected int [][] tablero;
	
	//tablero que contendra el hidato resuelto.
	protected int [][] tablero_solucionado;
	
	//tipo de adjacencia
	protected Adjacencia adj;
	
	public int getnCeles() {
		return nCeles;
	}

	public int getForats() {
		return forats;
	}
	//nombre unico del hidato que lo identifica
	protected String nomHidato;
	
	
	//topologia para generar el hidato
	protected Topologia top;
	
	//numero total de celdas
	protected int nCeles;
	
	//numero de forats
	protected int forats;
	
	//numero de celdas llenas por defecto
	protected int nCasellesOmplertes;
	//------------------------getters y setters----------------------------
	
	//devuelve el tablero como una matriz
	public int[][] getTablero() {
		return tablero;
	}

	//devuelve el tablero con solucion
	public int[][] getTablero_solucionado() {
		return tablero_solucionado;
	}


	public void setTablero(int[][] tablero) {
		this.tablero = new int[tablero.length][tablero[0].length];
		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero[0].length; j++) {
				this.tablero[i][j] = tablero[i][j];
			}
		}
	}

	public void setTablero_solucionado(int[][] tablero_solucionado) {
		this.tablero_solucionado = new int[tablero_solucionado.length][tablero_solucionado[0].length];
		for (int i = 0; i < tablero_solucionado.length; i++) {
			for (int j = 0; j < tablero_solucionado[0].length; j++) {
				this.tablero_solucionado[i][j] = tablero_solucionado[i][j];
			}
		}
	}

	//----------------------------------------------------------------------
	/**
	 * @return Nom de l'hidato
	 */
	public String getNomHidato() {
		return nomHidato;
	}
	/**
	 * 
	 * @return l'adjacencia de l'hidato
	 */
	public Adjacencia getAdjacencia() {
		return adj;
	}
	/**
	 * 
	 * @return la topologia de l'hidato
	 */
	public Topologia getTopologia() {
		return top;
	}

	/**
	 * Metode que crea una instancia d'hidato a partir d'un tauler donat
	 * @param adj
	 * @param tablero
	 * @param nomH
	 * @param t
	 */
	public Hidato(Adjacencia adj, int [][] tablero, String nomH, Topologia t) {
	
		nCeles = 0;
		forats = 0;
		nCasellesOmplertes = 0;
		
		this.adj = adj;
		this.tablero = tablero;
		//inicialitza el tablero solucionado amb el tablero.
		tablero_solucionado = new int[this.tablero.length][this.tablero[0].length];
		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero[0].length; j++) {
				tablero_solucionado[i][j] = this.tablero[i][j];
				if (tablero[i][j] != -3) {
					nCeles++;
					if (tablero[i][j] == -2) {
						forats++;
					}
					else if (tablero[i][j] != -1) {
						nCasellesOmplertes++;
					}
				}
			}
		}
		nomHidato = nomH;
	
		top = t;
		
	}
	
	/**
	 * Metode que crea una instancia d'hidato de zero assignant parametres 
	 * als atributs
	 * @param adj
	 * @param nomH
	 * @param t
	 * @param forats
	 * @param celes_totals
	 * @param co
	 */
	public Hidato(Adjacencia adj, String nomH, Topologia t, int forats, int celes_totals, int co) {
		
		
		this.adj = adj;
		nomHidato = nomH;
		
		top = t;
		nCeles = celes_totals;
		nCasellesOmplertes = co;
		this.forats = forats;
		
		
	}
	/**
	 * Metode que inicialitza les celes buides de l'hidato
	 * @param hid
	 */
	protected void iniCeldasVacias(int [][] hid) {
		for (int i = 0; i < hid.length; i++) {
			for (int j = 0; j < hid[0].length; j++) {
				//si es del tauler es posa a -1
				if (hid[i][j] != -3) {
					hid[i][j] = -1;
				}
			}
		}
	}
	/**
	 * Metode que al generar un hidato complet (amb totes les celes plenes)
	 * extreu celes i les deixa com a buides per complir els requisits que es demanen
	 */
	protected void treureCeles() {
		Random rnd = new Random();
		int a = 0;
		int b = 0;
		
		//numero de caselles que s'haura de treure
		int i = nCeles - nCasellesOmplertes - forats;
		while (i > 0) {
		
			a = rnd.nextInt(tablero_solucionado.length);
			b = rnd.nextInt(tablero_solucionado[0].length);
			/*Treure una casella random que no sigui -3(#), -2(*), casella inicial, casella final o ja esta treta.
			 * */
			if (tablero_solucionado[a][b] != -3 && tablero_solucionado[a][b] != 1 && tablero[a][b] != -1 &&
					tablero_solucionado[a][b] != (nCeles-forats) && tablero_solucionado[a][b] != -2) {
				tablero[a][b] = -1;
				i--;
			}
		}
	}
	/**
	 * Metode que escriu l'hidato en pantalla (nomes es fa servir per proves)
	 * @param hid
	 */
	protected void printHid(int [][] hid) {
		for (int i = 0; i < hid.length; i++) {
			for (int j = 0; j < hid[0].length; j++) {
				System.out.print(hid[i][j] + " ");
			}
			System.out.println();
		}
	}

	/**
	 * Metode que retorna el cami que possiblenent s'utilitzi per la generacio de l'hidato
	 * @param n
	 * @return vector amb valors random per generar camins diferents cada vegada
	 */
	protected int[] random(int n) {
		int [] v = new int[n]; 
		Random rnd = new Random();
		ArrayList<Integer> jaPosats = new ArrayList<Integer>(n);
		for (int i = 0; i < n; i++) {
			Integer x = rnd.nextInt(n);
			if (!jaPosats.contains(x)) {
				v[i] = x;
				jaPosats.add(x);
			}
			else {
				i--;
			}
		}
		return v;
	}

	/**
	 * Metode qu resol l'hidato i retorna si ha sigut possible o no
	 * @return true si s'ha pogut solucionar, false en cas contrari
	 */
	public abstract boolean solver();
	/**
	 * Metode que genera un hidato i retorna un boolea que diu si s'ha pogut crear amb exit o no
	 * @return true si s'ha pogut crear l'hidato, false en cas contrari
	 */
	public abstract boolean generator();
}

