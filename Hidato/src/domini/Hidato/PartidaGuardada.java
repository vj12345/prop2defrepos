package domini.Hidato;
import domini.Hidato.Hidato.Topologia;
import domini.Hidato.Hidato.Adjacencia;
import domini.Joc.Joc.Tipus;
/**
 * PartidaGuardada es un objecte que guarda hidatos amb tots els seus parametres 
 * poder carregar-los i jugar-los una altra vegada, ja siguin partides a mitgees o
 * els hidatos de la base de dades
 * @author Mohamed Chait
 *
 */
public class PartidaGuardada {
	//tablero con el hidato para resolver
	private int [][] tablero_original;
	
	//tablero que contendra el hidato 
	private int [][] tablero_guardado;
	
	private Adjacencia adj;
	
	private Topologia top;
	
	private Tipus tipus;
	
	private int tempsGastat;
	
	//identificador de la partida que se pondra en el momento de guardar partida.
	private String Id;
	
	//nom unica del hidato que el identifica
	private String nomHidato;
	
	//nom del autor
	private String nomAutor;

	/**
	 * Metode creador de PArtidaGuardada que assigna els parametres als atributs
	 * @param tablero_original
	 * @param tablero_guardado
	 * @param Id
	 * @param adj
	 * @param top
	 * @param tipus
	 * @param nomHidato
	 * @param nomAutor
	 */
	public PartidaGuardada(int [][] tablero_original,int [][] tablero_guardado,String Id,Adjacencia adj,Topologia top, Tipus tipus, String nomHidato,String nomAutor, int temps){
		this.tablero_original = tablero_original;
		this.tablero_guardado = tablero_guardado;
		this.Id = Id;
		this.adj = adj;
		this.top = top;
		this.tipus = tipus;
		this.nomHidato = nomHidato;
		this.nomAutor = nomAutor;
		this.tempsGastat = temps;
	}
	
	

	/**
	 * Asigna el tauler base a PartidaGuardada
	 * @param tablero_original
	 */
	public void setTablero_original(int[][] tablero_original) {
		this.tablero_original = tablero_original;
	}
	/**
	 * Asigna un tauler provisional a PartidaGuardada
	 * @param tablero_guardado
	 */
	public void setTablero_guardado(int[][] tablero_guardado) {
		this.tablero_guardado = tablero_guardado;
	}
	/**
	 * Asigna una adjacencia a PatidaGuardada
	 * @param adj
	 */
	public void setAdj(Adjacencia adj) {
		this.adj = adj;
	}
	/**
	 * Asigna una topologia a PartidaGuardada
	 * @param top
	 */
	public void setTop(Topologia top) {
		this.top = top;
	}
	/**
	 * Asigna un tipus a PartidaGuardada
	 * @param tipus
	 */
	public void setTipus(Tipus tipus) {
		this.tipus = tipus;
	}
	
	/**
	 * Asigna un tempsGastat a PartidaGuardada
	 * @param tipus
	 */
	public void setTempsGastat(int temps){
		tempsGastat = temps;
	}
	/**
	 * Asigna una id a PartidaGuardada
	 * @param id
	 */
	public void setId(String id) {
		Id = id;
	}
	/**
	 * Asigna un nom a PartidaGuardada
	 * @param nomHidato
	 */
	public void setNomHidato(String nomHidato) {
		this.nomHidato = nomHidato;
	}
	/**
	 * Asigna un nom d'autor a PartidaGuardada
	 * @param nomAutor
	 */
	public void setNomAutor(String nomAutor) {
		this.nomAutor = nomAutor;
	}
	/**
	 * Retorna el tauler base de PartidaGuardada
	 * @return Tauler base
	 */
	public int [][] getTableroOriginal(){
		return tablero_original;
	}
	/**
	 * Retorna el tauler provisional de PartidaGuardada
	 * @return Tauler provisional
	 */
	public int [][] getTableroGuardado(){
		return tablero_guardado;
	}
	/**
	 * Retorna la id de PartidaGuardada
	 * @return int Id
	 */
	public String getId(){
		return Id;
	}
	/**
	 * Retorna l'adjacencia de PartidaGuardada
	 * @return Adjacencia adj
	 */
	public Adjacencia getAdjacencia(){
		return adj;
	}
	/**
	 * Retorna la topologia de PartidaGuardada
	 * @return Topologia top
	 */
	public Topologia getTopologia(){
		return top;
	}
	/**
	 * Retorna el tipus de PartidaGuardada
	 * @return Tipus tipus
	 */
	public Tipus getTipus() {
		return tipus;
	}
	/**
	 * Retorna el nom d'hidato de PartidaGuardada
	 * @return String nomHidato
	 */
	public String getNomHidato() {
		return nomHidato;
	}
	/**
	 * Retorna el nom d'autor de PartidaGuardada
	 * @return String nomAutor
	 */
	public String getNomAutor() {
		return nomAutor;
	}
	
	/**
	 * Retorna el temps jugat de PartidaGuardada
	 * @return int tempsGastat
	 */
	public int getTempsGastat() {
		return tempsGastat;
	}
}

