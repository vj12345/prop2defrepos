package domini.Hidato;
import domini.Hidato.Hidato.Topologia;
import domini.Hidato.Hidato.Adjacencia;
import domini.Joc.Joc.Tipus;
import java.util.ArrayList;
import java.util.Scanner;


public class DriverPartidaGuardada {
	
	private final static String Enmarcat = "##########################################################";

	public static void main(String[] args) {
		try{
			Scanner linea=new Scanner(System.in);
			int opcio, files,columnes, nombre;
			String id,par;
			int[][] tauler_modificat;
			int[][] tauler_original;
			Adjacencia adj;
			Topologia top;
			Tipus tipus;
			PartidaGuardada pg = null;
			 Exception ex = new Exception("Has introduit malament el que et demanavem, torna-ho a provar");
			System.out.println("Benvolgut al Driver de PartidaGuardada");
			while(true){
				System.out.println(Enmarcat);
				System.out.println("Selecciona la opci� que vols efectuar amb el numero");
				System.out.println("1 -> GuardarPartida");
				System.out.println("2 -> Obtenir Tauler Original sense jugar");
				System.out.println("3 -> Obtenir Tauler Modificat ");
				System.out.println("4 -> Obtenir el identificador de la partida");
				System.out.println("5 -> Obtenir topologia");
				System.out.println("6 -> Obtenir Adjacencia");
				System.out.println("7 -> Obtenir Tipus");
				System.out.println(Enmarcat);
				opcio = linea.nextInt();
				linea.nextLine();
				if(opcio == 1) {
					System.out.println("Escriu l'identificador de la partida");
					id = linea.nextLine();
					System.out.println("Escriu l'adjacencia del hidato (Escriu C per costat o CA per costat i aresta)");
					par = linea.nextLine();
					if(par.equals("C")) adj = Adjacencia.C;
					else if(par.equals("CA")) adj = Adjacencia.CA;
					else throw ex;
					System.out.println("Escriu el tipus d'Hidato (Escriu TRIANGLE o QUADRAT o HEXAGON");
					if(par.equals("TRIANGLE")) tipus = Tipus.TRIANGLE;
					else if(par.equals("QUADRAT")) tipus = Tipus.QUADRAT;
					else if(par.equals("HEXAGON")) tipus = Tipus.HEXAGON;
					else throw ex;
					System.out.println("Escriu la topologia del hidato(Escriu TRIANGLE o QUADRAT o RECTANGLE");
					par = linea.nextLine();
					if(par.equals("TRIANGLE")) top = Topologia.TRIANGLE;
					else if(par.equals("QUADRAT")) top = Topologia.QUADRAT;
					else if(par.equals("RECTANGLE")) top = Topologia.RECTANGLE;
					else throw ex;
					System.out.println("Escriu el nombre de columnes del hidato");
					columnes = linea.nextInt();
					System.out.println("Escriu el nombre de files del hidato ");
					files = linea.nextInt();
					tauler_modificat = new int[files][columnes];
					tauler_original = new int[files][columnes];
					System.out.println("Escriu l'hidato modificat per files, Recorda que les cel�les buides es codifiquen amb un -1, les cel�les amb forat -2, "
							+ "les cel�les que no formen part del tauler amb un -3");
					for (int i = 0; i < tauler_original.length; i++) {
						for (int j = 0; j < tauler_original[0].length; j++) {
							nombre = linea.nextInt();
							tauler_original[i][j] = nombre;
						}
					}
					System.out.println("Escriu l'hidato original per files, Recorda que les cel�les buides es codifiquen amb un -1, les cel�les amb forat -2, "
							+ "les cel�les que no formen part del tauler amb un -3");
					for (int i = 0; i < tauler_modificat.length; i++) {
						for (int j = 0; j < tauler_modificat[0].length; j++) {
							nombre = linea.nextInt();
							tauler_modificat[i][j] = nombre;
						}
					}
					pg = new PartidaGuardada(tauler_original, tauler_modificat, id, adj, top, tipus, "DriverHidato", "prova",0);
				}
				if(opcio == 2) {
					int [][] tauler = pg.getTableroOriginal();
					printHid(tauler);
				}
				if(opcio == 3) {
					int [][] tauler = pg.getTableroGuardado();
					printHid(tauler);
				}
				if(opcio == 4) {
					System.out.println("L'identificador es " + pg.getId());
				}
				if(opcio == 5) {
					System.out.println("La topologia es " + pg.getTopologia());
				}
				if(opcio == 6) {
					System.out.println("L'adjacencia es " + pg.getAdjacencia());
				}
				if (opcio == 6) {
					System.out.println("El tipus es " + pg.getTipus());
				}
			}
		}
		catch (Exception e) {
			System.out.println("Hi ha hagut una excepci�");
			e.printStackTrace();
		}
	}
	private static void omplenarMatriu(int [][] hid) {
		Scanner linea=new Scanner(System.in);
		for (int i = 0; i < hid.length; i++) {
			for (int j = 0; j < hid[0].length; j++) {
				hid[i][j] = linea.nextInt();
			}
		}
	}
	private static void printHid(int [][] hid) {
		for (int i = 0; i < hid.length; i++) {
			for (int j = 0; j < hid[0].length; j++) {
				System.out.print(hid[i][j] + " ");
			}
			System.out.println();
		}
	}
}
