package domini.Hidato;

import java.util.ArrayList;
import java.util.Random;

/* IMPORTANT: La explicaci� m�s detallada esta en la classe HidatoT, que es semblant a aquesta.
 * */
public class HidatoH extends Hidato{

	/**
	 * Metode constructor que utilitza el constructor de la superclasse
	 * @param adj
	 * @param taurell
	 * @param nomH
	 * @param t
	 */
	/* Constructor per fer el solver
	 * Pre: taurell en foramt correcte.
	 * */
	public HidatoH(Adjacencia adj, int [][]taurell, String nomH, Topologia t) {
		super(adj, taurell, nomH, t);
	}
	/**
	 * Metode constructor que utilitza el constructor de la superclasse 
	 * @param adj
	 * @param nomH
	 * @param t
	 * @param forats
	 * @param celes_totals
	 * @param co
	 */
	/* Constructor per fer el generator
	 * Pre: celes_totals >= 9, co (celes omplertes per defecte) > 2 (inicial i final), forats maxim 1 per
	 * cada 9 celes, celes_totals > forats+co. Si la topologia es quadrada, l'arrel quadrada del celes_totals 
	 * ha de ser enter.
	 * */
	public HidatoH(Adjacencia adj, String nomH, Topologia t, int forats, int celes_totals, int co) {
		super(adj, nomH, t, forats, celes_totals, co);
		
	}
	/**
	 * Metode que busca el punt d'inici i utilitzant un vector amb els valors que es necessiten intenta solucionar l'hidato
	 * cridant a solverC
	 */
	public boolean solver() {
		// TODO Auto-generated method stub
		
		row = super.tablero_solucionado.length;
		col = super.tablero_solucionado[0].length;
		
		ArrayList<Integer> list = new ArrayList<Integer>(row*col);
		 
		start = new int[] {-1,-1};

		for(int i = 0; i < row; i++) {
			 for(int j = 0; j < col; j++) {
				 if (super.tablero_solucionado[i][j] != -2 && super.tablero_solucionado[i][j] != -1 && super.tablero_solucionado[i][j] != -3) {
					 list.add(super.tablero_solucionado[i][j]);
					 if (super.tablero_solucionado[i][j] == 1) {
						 start = new int[] {i, j};
					 }
				 }
			 }
		}
		
		if (start[0] == -1 || start[1] == -1) {
			return false;
		}
		
		list.sort(null);
		given = new int[list.size()];
		
		for (int i = 0; i < given.length; i++) {
			given[i] = (int) list.get(i);
		}
		if (given[given.length-1] < (super.getnCeles()-super.getForats())) {
			return false;
		}
		
		return solverC(start[0],start[1],1,0);

	}
	
	/**
	 * Metode que resol l'hidato guardat en el tablero_solucionado de forma recursiva aplicant dfs amb backtracking.
	 * @param x
	 * @param y
	 * @param valor
	 * @param givenpos
	 * @return boolea per indicar si s'ha creat amb exit o no
	 */
	private boolean solverC(int x, int y, int valor, int givenpos) {
		
		if (valor > given[given.length-1]) {
			return true;
		}
		
		if ( x < 0 || x > row-1 || y > col-1 || y < 0 || tablero_solucionado[x][y] == -2 || tablero_solucionado[x][y] == -3) {
			return false;
		}
	
		if (super.tablero_solucionado[x][y] == -1 && valor >= given[givenpos]) {
			return false;
		}
		
		if (super.tablero_solucionado[x][y] != -1 && super.tablero_solucionado[x][y] != valor) {
			return false;
		}
		
		if (super.tablero_solucionado[x][y] == given[givenpos]) {
			givenpos++;
		}
		
		int valor_ant = super.tablero_solucionado[x][y];
		super.tablero_solucionado[x][y] = valor;
		/*En les celes hexagonals el tipus de adjacencia de costat es el mateix que el de costat i aresta.
		 *Tenen com a celes adjacents les celes dels quatre costats i si esta en una linia parell
		 *tambe te d'adjacent la cela de adalt a l'esquerre i abaix a l'esquerre, mentre
		 *que si es una linia senar tindra de adjacents la cela de adalt a la dreta i abaix a la dreta.
		 * */
		for (int i = -1; i < 2; i++) {
			if (solverC(x+i, y, valor+1, givenpos)) {
				return true;
			}
		}
		for (int i = -1; i < 2; i++) {
			if (solverC(x, y+i, valor+1, givenpos)) {
				return true;
			}
		}
	
		if (x % 2 == 0) {
			
			if (solverC(x+1, y-1, valor+1, givenpos)) {
				return true;
			}
			if (solverC(x-1, y-1, valor+1, givenpos)) {
				return true;
			}
			
		}
		
		if (x % 2 != 0) {
			if (solverC(x+1, y+1, valor+1, givenpos)) {
				return true;
			}
			if (solverC(x-1, y+1, valor+1, givenpos)) {
				return true;
			}
		}
		
		super.tablero_solucionado[x][y] = valor_ant;
		
		return false;
	}
	/**
	 * Metode que genera a partir de valors aleatoris i dfs amb backtracking
	 */
	public boolean generator() {
		if (!generarTauler()) {
			return false;
		}
		if (nCasellesOmplertes < 2) {
			return false;
		}
		if (generatorH()) {
			tablero = new int[row][col];
			for (int i = 0; i < row; i++) {
				for (int j = 0; j < col; j++) {
					tablero[i][j] = tablero_solucionado[i][j];
				}
			}
			treureCeles();
			return true;
		}
		return false;
	}
	
	/**
	 * Metode que genera a partir de valors aleatoris i dfs amb backtracking
	 */
	private boolean generatorH(){
		
		int x = tablero_solucionado.length;
		int y = tablero_solucionado[0].length;
		int a = 0;
		int b = 0;
		boolean trob = false;
		while(!trob) {
			iniCeldasVacias(tablero_solucionado);
			
			Random rnd = new Random();
			
			int i = 0;
			
			while (i < super.forats) {
				 a = rnd.nextInt(x);
				 b = rnd.nextInt(y);
				 if (tablero_solucionado[a][b] != -2 && tablero_solucionado[a][b] != -3) {
					 tablero_solucionado[a][b] = -2;
					 i++;
				 }
			}	
			
			int valor_final = super.nCeles - super.forats;
	
				
			boolean trobat = false;

			while (!trobat) {
				a = rnd.nextInt(x);
				b = rnd.nextInt(y);
				if (tablero_solucionado[a][b] == -1) {
					 tablero_solucionado[a][b] = 1;
					 trobat = true;
				}
			}
			
			
			
			given = new int[] {1, valor_final};
			row = tablero_solucionado.length;
			col = tablero_solucionado[0].length;
			if (buscarCami(a,b,1,valor_final)) {
	

				return true;
				 
			}
		
		}
		
		return false;
	}
	/**
	 * Metode utilitzat per generator per buscar un cami que ompli totes les celes
	 * @param x
	 * @param y
	 * @param valor
	 * @param vf
	 * @return boolea per indicar si s'ha trobat el cami amb exit o no
	 */
	private boolean buscarCami(int x, int y, int valor, int vf) {
		
		if (valor > vf) {
			return true;
		}
		
		if ( x < 0 || x > row-1 || y > col-1 || y < 0 || tablero_solucionado[x][y] == -2 || tablero_solucionado[x][y] == -3) {
			return false;
		}

		
		if (super.tablero_solucionado[x][y] != -1 && super.tablero_solucionado[x][y] != valor) {
			return false;
		}
		
		int valor_ant = super.tablero_solucionado[x][y];
		super.tablero_solucionado[x][y] = valor;
		
		int [] seq = random(3);
		
		for (int k = 0; k < seq.length; k++) {
			
			if (seq[k] == 0) {
				for (int i = -1; i < 2; i++) {
					if (buscarCami(x+i, y, valor+1, vf)) {
						return true;
					}
				}
			} 
			else if (seq[k] == 1) { 
				for (int i = -1; i < 2; i++) {
					if (buscarCami(x, y+i, valor+1, vf)) {
						return true;
					}
				}
			}
			
			else if (seq[k] == 2) {
				if (x % 2 == 0) {
					
					if (buscarCami(x+1, y-1, valor+1, vf)) {
						return true;
					}
					if (buscarCami(x-1, y-1, valor+1, vf)) {
						return true;
					}
					
				}
				
				if (x % 2 != 0) {
					if (buscarCami(x+1, y+1, valor+1, vf)) {
						return true;
					}
					if (buscarCami(x-1, y+1, valor+1, vf)) {
						return true;
					}
				}
			}
		}
		
		super.tablero_solucionado[x][y] = valor_ant;
		
		return false;
	}
	/**
	 * Genera el tauler depenent de la topologia indicada
	 * @return boolea que indica si s'ha creat amb exit o no
	 */
	private boolean generarTauler() {
		//tauler quadrat
		if (top == Topologia.QUADRAT) {
			int x = (int) Math.sqrt(super.nCeles);
			tablero_solucionado = new int [x][x];
			return true;
		}
		return false;
	}
	
	

}
