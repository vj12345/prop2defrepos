package domini.Hidato;

import java.util.ArrayList;
import java.util.Random;

public class HidatoT extends Hidato{
	
	private boolean fila_buida = false;
	
	int count = 0;
	/**
	 * Metode constructor que utilitza el constructor de la superclasse
	 * @param adj
	 * @param taurell
	 * @param nomH
	 * @param t
	 */
	public HidatoT(Adjacencia adj, int [][]taurell, String nomH,Topologia t) {
		super(adj, taurell, nomH, t);

	}
	/**
	 * Metode constructor que utilitza el constructor de la superclasse
	 * @param adj
	 * @param nomH
	 * @param t
	 * @param forats
	 * @param celes_totals
	 * @param co
	 */
	public HidatoT(Adjacencia adj, String nomH, Topologia t, int forats, int celes_totals, int co) {
		super(adj, nomH, t, forats, celes_totals, co);
		
	}
	/**
	 * Metode que busca el punt d'inici i utilitzant un vector amb els valors que es necessiten intenta solucionar l'hidato
	 * cridant a solverC
	 */
	public boolean solver() {
		
		/*numero de files i columnes del
		 * */
		row = super.tablero_solucionado.length;
		col = super.tablero_solucionado[0].length;
		
		/*Lista que contindra tots els numeros donats per defecte
		 * */
		ArrayList<Integer> list = new ArrayList<Integer>(row*col);
		 
		/*Afegeix a la lista tots els numeros donats per defecte del tablero_solucionado i pasa 
		 * el valor de la posicio inicial al vector start.
		 * */
		start = new int[] {-1,-1};
		for(int i = 0; i < row; i++) {
			 for(int j = 0; j < col; j++) {
				 if (super.tablero_solucionado[i][j] != -2 && super.tablero_solucionado[i][j] != -1 && super.tablero_solucionado[i][j] != -3) {
					 list.add(super.tablero_solucionado[i][j]);
					 if (super.tablero_solucionado[i][j] == 1) {
						 start = new int[] {i, j};
					 }
				 }
			 }
		}
		/*No existeix la posicio inicial
		 */
		if (start[0] == -1 || start[1] == -1) {
			return false;
		}
		
		/* Ordena la llista de mes petit a mes gran
		 * */
		list.sort(null);
		
		given = new int[list.size()];
		
		/* Guarda al vector given tots els numeros donats per defecte de mes petit a mes gran.
		 * */
		for (int i = 0; i < given.length; i++) {
			given[i] = (int) list.get(i);
		}
		if (given[given.length-1] < (super.getnCeles()-super.getForats())) {
			return false;
		}
		
		/* Resol el matriu tablero i hi deixa al tablero_solucionado, si ho resol retorna cert, sino retorna fals.
		 * */
		return solverC(start[0],start[1],1,0);

	}
	/**
	 * Metode que resol l'hidato guardat en el tablero_solucionado de forma recursiva aplicant dfs amb backtracking.
	 * @param x
	 * @param y
	 * @param valor
	 * @param givenpos
	 * @return boolea per indicar si s'ha creat amb exit o no
	 */
	private boolean solverC(int x, int y, int valor, int givenpos) {
		
		/* Si el velor amb el que ha d'omplir el forat actual es m�s gran que el valor final,
		 * vol dir que ha trobat la soluci, retorna cert.
		 * */
		if (valor > given[given.length-1]) {
			return true;
		}
		
		/*Si la posicio actual esta fora del rang del matriu tablero_solucionado o 
		 * es un -2(*) o -3(#) no es pot seguir,
		 * llavors retorna false.
		 * */
		if ( x < 0 || x > row-1 || y > col-1 || y < 0 || tablero_solucionado[x][y] == -2 || tablero_solucionado[x][y] == -3) {
			return false;
		}
		
		/*Si la cela a omplir esta buida pero el valor amb el que ho hem d'omplir es igual o mes gran
		 * que el seg�ent valor donat per defecte, llavors es que no es el cami correcte.
		 * */
		if (super.tablero_solucionado[x][y] == -1 && valor >= given[givenpos]) {
			return false;
		}
		
		/*La cela a omplir ja esta omplert, pero el valor es incorrecte.
		 * */
		if (super.tablero_solucionado[x][y] != -1 && super.tablero_solucionado[x][y] != valor) {
			return false;
		}
		
		/* Si el valor de la cela es igual que el seg�ent valor donat per defecte del vectro given
		 * incrementem el givenpos.
		 * */
		if (super.tablero_solucionado[x][y] == given[givenpos]) {
			givenpos++;
		}
		
		/* Guardar el valor anterior per restaurar-lo si no troben solucio les crides recursives.
		 * */
		int valor_ant = super.tablero_solucionado[x][y];
		
		/* Asignar el tauler a omplir amb el valor acutal.
		 * */
		super.tablero_solucionado[x][y] = valor;
	
		/*Segons el tipus de adjacencia, les celes adjacents son diferents, si
		 * la adjacencia es de costat cal fer la crida recursiva per
		 * la cela de l'esquerrem , la dreta i si estem en una fila parell i columna parell hem de 
		 * visitar la cela d'abaix, si estem en una fila parell i columna senar, hem de visitar la cela de dalt,
		 * si estem en una fila senar i columna parell hem de visitar la cela de dalt, finalment si
		 * estem en una fila senar i columna senar hem de visitar la cela de abaix.
		 * */
		if (adj == Adjacencia.C) {
			for (int i = -1; i < 2; i++) {
				if (solverC(x, y+i, valor+1, givenpos)) {
					return true;
				}
			}
			/*fila parell (0, 2, 4, 6)*/
			int f;
			if (x % 2 == 0) {
				/*columna parell*/
				if (y % 2 == 0) {
					f = 1;
				}
				else {
					f = -1;
				}
			}
			else {
				if (y % 2 == 0) {
					f = -1;
				}
				else {
					f = 1;
				}
			}
			
			/*
			if (saltarLinea(x+f)) {
				x += f;
			}
	*/
			/* Crida recursiva per la cela de abaix o adalt segons la posicio actual.
			 * */
			if (solverC(x+f, y, valor+1, givenpos)) {
				return true;
			}
		
		}
		/* En la adjacencia per costat i aresta, existeix 12 celes adjacents.
		 * */
		else {
			/*Crides recursives per les celes dels 8 costats.
			 * */
			for (int i = -1; i < 2; i++) {
				for (int j = -1; j < 2; j++) {
					/*
					if (saltarLinea(x+i)) {
						x += i;
					}
					*/
					if (solverC(x+i, y+j, valor+1, givenpos)) {
						return true;
					}
				}
			}
			/* Crida per les celes que esta dos poscions a la dreta
			 * */
			if (solverC(x, y+2, valor+1, givenpos)) {
				return true;
			}
			
			/* Crida per les celes que estan dos posicions a l'esquerre
			 * */
			if (solverC(x, y-2, valor+1, givenpos)) {
				return true;
			}
			
			int f;
			if (x % 2 == 0) {
				if (y % 2 == 0) {
					f = 1;
				}
				else {
					f = -1;
				}
			}
			else {
				if (y % 2 == 0) {
					f = -1;
				}
				else {
					f = 1;
				}
			}
			/*
			if (saltarLinea(x+f)) {
				x += f;
			}
			*/
			/*Crides per les celes que estan dos posicions capa l'esquerre i dreta d' adalt o abaix segons la posicio actual.
			 * */
			if (solverC(x+f, y+2, valor+1, givenpos)) {
				return true;
			}
			if (solverC(x+f, y-2, valor+1, givenpos)) {
				return true;
			}
			
		}
		
		/* Restableix el valor anterior a les crides recursives si les crides no han trobat solucio.
		 * */
		super.tablero_solucionado[x][y] = valor_ant;
		
		return false;
	}
/*
	//retorna cert si la linia x del super.tablero es una linia buida
	private boolean saltarLinea(int x) {
		if ( tablero_solucionado.length < 0 || tablero_solucionado.length > row-1 || tablero_solucionado[0].length > col-1 || tablero_solucionado[0].length < 0) {
			return false;
		}
		for (int i = 0; i < super.tablero_solucionado[x].length; i++) {
			if (super.tablero_solucionado[x][i] != -3) {
				return false;
			}
		}
		return true;
	}
	*/
	/**
	 * Metode que genera a partir de valors aleatoris i dfs amb backtracking
	 */
	public boolean generator() {
		
		/*genera la forma del tauler, omplint amb -3 les celes que son #, i ho deixa al tauler_solucionat
		 * */
		if (!generarTauler()) {
			return false;
		}
		
		/*Comprova que el numero de caselles omplertes siguin 2
		 * */
		if (nCasellesOmplertes < 2) {
			return false;
		}
		
		/* Genera el tauler amb la solucio en tablero_solucionado i l'original al tablero.
		 * */
		if (generatorT()) {
			tablero = new int[row][col];
			for (int i = 0; i < row; i++) {
				for (int j = 0; j < col; j++) {
					tablero[i][j] = tablero_solucionado[i][j];
				}
			}
			
			/* Treu celes aleatoriament per deixa la quantitat de celes per defecte demanat.
			 * */
			treureCeles();
			
			return true;
		}
		
		return false;
	}
	/**
	 * Metode que genera a partir de valors aleatoris i dfs amb backtracking
	 */
	private boolean generatorT(){
		
		/* Tamany de fila i columna que serviran per generar posicions aleatoris
		 * */
		int x = tablero_solucionado.length;
		int y = tablero_solucionado[0].length;
		
		/* Serviran per guardar les posicions aleatoris creats
		 * */
		int a = 0;
		int b = 0;
		
		/* Va generant fins que ho aconseguiex, posible bucle infinit que s'evita 
		 * amb les precondicions del constructor.
		 * */
		boolean trob = false;
		while(!trob) {
			
			/*posa -1 a totes les celes del tablero solucionado que no siguin -3(#)
			 * */
			iniCeldasVacias(tablero_solucionado);
			
			
			/*Fica els tres forats a les tres puntes si es topologia triangular amb adjacencia de costat
			 */
			if (adj == Adjacencia.C && top == Topologia.TRIANGLE) {
				if (forats < 3) {
					return false;
				}
				
				tablero_solucionado[tablero_solucionado.length-1][tablero_solucionado[0].length-1] = -2;

				tablero_solucionado[tablero_solucionado.length-1][0] = -2;
				if (fila_buida) {
					tablero_solucionado[1][(tablero_solucionado[0].length-1)/2] = -2;
				}
				
				else {
					tablero_solucionado[0][(tablero_solucionado[0].length-1)/2] = -2;
				}
		

			}
			
			Random rnd = new Random();
			
			
			int i = 0;
			
			/* sumar 3 perque si es topologia trinagular i adjacencia de costat els tres costats de la 
			topologia triangular ha de ser forat obligatoriament
			*/
			if (top == Topologia.TRIANGLE && adj == Adjacencia.C) {
				i += 3;
			}
			
			
			/*Ficar els forats de forma aleatori.
			 */
			while (i < super.forats) {
				 a = rnd.nextInt(x);
				 b = rnd.nextInt(y);
				 if (tablero_solucionado[a][b] != -2 && tablero_solucionado[a][b] != -3) {
					 tablero_solucionado[a][b] = -2;
					 i++;
				 }
			}	
			
			/*ultim valor dels numeros donats per defecte*/
			int valor_final = super.nCeles - super.forats;
				
			boolean trobat = false;
	
			/*Busca un punt aleatori per ficar-hi el valor inicial que es 1
			 * */
			while (!trobat) {
				a = rnd.nextInt(x);
				b = rnd.nextInt(y);
				if (tablero_solucionado[a][b] == -1) {
					 tablero_solucionado[a][b] = 1;
					 trobat = true;
				}
			}
			
			
			/*Inicialitzar el vector given, el row i col necesari per fer la crida a buscarCami que
			 * intenta trobar un cami que ompli totes les celes comen�ant pel punt inicial.
			 * */
			given = new int[] {1, valor_final};
			row = super.tablero_solucionado.length;
			col = super.tablero_solucionado[0].length;
			if (buscarCami(a,b,1,valor_final)) {

				return true;
				 
			}
		
		}
		
		return false;
	}
	/**
	 * Metode utilitzat per generator per buscar un cami que ompli totes les celes
	 * @param x
	 * @param y
	 * @param valor
	 * @param vf
	 * @return boolea per indicar si s'ha trobat el cami amb exit o no
	 */
	private boolean buscarCami(int x, int y, int valor, int vf) {
		++count;
		if (count > 200) {
			count = 0;
			return false;
			//return false;
			
		}
		/*Si el valor actual es mes gran que el valor final es que ja ha omplert totes les celes.
		 * */
		if (valor > vf) {
			return true;
		}
		
		if ( x < 0 || x > row-1 || y > col-1 || y < 0 || tablero_solucionado[x][y] == -2 || tablero_solucionado[x][y] == -3) {
			return false;
		}

		
		if (super.tablero_solucionado[x][y] != -1 && super.tablero_solucionado[x][y] != valor) {
			return false;
		}
		
		int valor_ant = super.tablero_solucionado[x][y];
		super.tablero_solucionado[x][y] = valor;
		
		if (adj == Adjacencia.C) {
			
			/*Vector de 3 posicions amb contingut random per donar un ordre random al tractament de les celes adjacents
			 * perqu el cami no sigui sempre del mateix estil.
			 * */
			int [] seq = random(3);
			
			/*Tractament de les celes adjacents.
			 * */
			for (int i = 0; i < seq.length; i++) {
				if (seq[i] == 0) {
					if (buscarCami(x, y+1, valor+1, vf)) {
						return true;
					}
				}
				else if (seq[i] == 1) {
					if (buscarCami(x, y-1, valor+1, vf)) {
						return true;
					}
				}
				else if (seq[i] == 2) {
					//fila parell (0, 2, 4, 6)
					int f;
					if (x % 2 == 0) {
						if (y % 2 == 0) {
							f = 1;
						}
						else {
							f = -1;
						}
					}
					else {
						if (y % 2 == 0) {
							f = -1;
						}
						else {
							f = 1;
						}
					}
					
					/*
					if (saltarLinea(x+f)) {
						x += f;
					}
			*/
					
					if (buscarCami(x+f, y, valor+1, vf)) {
						return true;
					}
				}
			}
			
	
		}
		/*El mateix pel cas de adjacencia de costat i vertex.
		 * */
		else {
			
			int [] seq = random(3);
			
			for (int k = 0; k < seq.length; k++) {
				if (seq[k] == 0) {
					for (int i = -1; i < 2; i++) {
						for (int j = -1; j < 2; j++) {
							/*
							if (saltarLinea(x+i)) {
								x += i;
							}
							*/
							if (buscarCami(x+i, y+j, valor+1, vf)) {
								return true;
							}
						}
					}
				}
				else if (seq[k] == 1) {
					if (buscarCami(x, y+2, valor+1, vf)) {
						return true;
					}
					if (buscarCami(x, y-2, valor+1, vf)) {
						return true;
					}
				}
				else if (seq[k] == 2) {
					int f;
					if (x % 2 == 0) {
						if (y % 2 == 0) {
							f = 1;
						}
						else {
							f = -1;
						}
					}
					else {
						if (y % 2 == 0) {
							f = -1;
						}
						else {
							f = 1;
						}
					}
					/*
					if (saltarLinea(x+f)) {
						x += f;
					}
					*/
					if (buscarCami(x+f, y+2, valor+1, vf)) {
						return true;
					}
					if (buscarCami(x+f, y-2, valor+1, vf)) {
						return true;
					}
				}
			}
			
			
			
		}
		

		super.tablero_solucionado[x][y] = valor_ant;
		
		return false;
	}
	/**
	 * Genera el tauler depenent de la topologia indicada
	 * @return boolea que indica si s'ha creat amb exit o no
	 */
	private boolean generarTauler() {
		
		/*Per la topologia triangular.
		 * */
		if (top == Topologia.TRIANGLE) {
			
			/* Files i columnes necesaris per generar un matriu per encabir un triangle perfecte
			 * */
			int files = (int) Math.sqrt(nCeles);
			int columnes = 2*files-1;
			
			/*Numero de cels que no formaran part del tauler de la primero fila
			 * */
			int buides = (files-1) *2;
			
			tablero_solucionado = new int[files][columnes];
			
			/*Si les files es parell vol dir que hi haura una fila buida al principi, llavors
			 * s'incrementa files.
			 * */
			if (files % 2 == 0) {
				files++;
				fila_buida = true;
			}
			
			/*Inicialitza el tabalro_solucionado amb les mides del tauler.
			 * */
			tablero_solucionado = new int[files][columnes];
			
			/*Omple les celes que no formaran el tauler per dibuixar un triangle exacte
			 */
		
			for (int i = 0; i < files; i++) {
				for (int j = 0; j < columnes; j++) {
					if (fila_buida) {
						if (i == 0) {
							tablero_solucionado[i][j] = -3;
						}
						else {
							if (j < buides/2) {
								tablero_solucionado[i][j] = -3;
							}
							else if (j > columnes-j+2*(i-1)) {
								tablero_solucionado[i][j] = -3;
							}
						}
					}
					else {
						if (j < buides/2) {
							tablero_solucionado[i][j] = -3;
						}
						else if (j > columnes-j+2*i) {
							tablero_solucionado[i][j] = -3;
						}
						
					}
					
				}
				if (fila_buida) {
					if (i != 0) {
						buides -= 2;
					}
				}
				else {
					buides -= 2;
				}
			}
			
			return true;
		}
		return false;
	}
	
	
}
