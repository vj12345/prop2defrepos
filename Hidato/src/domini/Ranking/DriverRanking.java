package domini.Ranking;

import java.util.ArrayList;
import java.util.Scanner;


public class DriverRanking {
	
	private final static String Enmarcat = "##########################################################";
	private final static String UserTemps = "USUARI						TEMPS";

	public static void main(String[] args) {
		try{
			Scanner linea=new Scanner(System.in);
			Ranking rank = new Ranking();
			int opcio;
			System.out.println("Benvolgut al Driver del Ranking");
			while(true){
				System.out.println("Selecciona la opci� que vols efectuar amb el numero");
				System.out.println("1 -> Afegir record al ranking");
				System.out.println("2 -> Obtenir Ranking facil");
				System.out.println("3 -> Obtindre Ranking mitja");
				System.out.println("4 -> ComprovarRanking dificil");
				opcio = linea.nextInt();
				linea.nextLine();
				if(opcio == 1) {
					String entra_username;
					int time,dificultat;
					System.out.println("Escrigui el seu nom d'usuari");
					entra_username = linea.nextLine();
					System.out.println("Escrigui el temps de record que has fet ");
					time = linea.nextInt();
					System.out.println("Escrigui la dificultat del hidato (0 == Facil, 1 == Mitja, 2 ==Dificil");
					dificultat = linea.nextInt();
					rank.afegirUser(dificultat,entra_username,time);
					System.out.println("El registre s'ha completat exitosament");
				}
				if(opcio == 2) {
					System.out.println(Enmarcat);
					System.out.println("RANKING DE DIFICULTAT FACIL");
					System.out.println(UserTemps);
					ArrayList<Pair> facil = rank.getRankFacil();
					
					for(Pair p: facil)
						System.out.println(p.getUsuari() + "						" + p.getTemps());
					
					System.out.println(Enmarcat);
				}
				if(opcio == 3) {
					System.out.println(Enmarcat);
					System.out.println("RANKING DE DIFICULTAT MITJA");
					System.out.println(UserTemps);
					ArrayList<Pair> facil = rank.getRankMitja();
					
					for(Pair p: facil)
						System.out.println(p.getUsuari() + "						" + p.getTemps());
					System.out.println(Enmarcat);
				}
				if(opcio == 4) {
					System.out.println(Enmarcat);
					System.out.println("RANKING DE DIFICULTAT DIFICIL");
					System.out.println(UserTemps);
					ArrayList<Pair> facil = rank.getRankDificil();
					
					for(Pair p: facil)
						System.out.println(p.getUsuari() + "						" + p.getTemps());
					System.out.println(Enmarcat);
				}
			}
		}
		catch (Exception e) {
			System.out.println("Hi ha hagut una excepci�");
		}
	}

}

