package domini.Ranking;

import java.util.ArrayList;
import java.util.ListIterator;

public class Ranking {
	protected ArrayList<Pair> facil;
	protected ArrayList<Pair> mitja;
	protected ArrayList<Pair> dificil;
	
	//constructora
	public Ranking(){
		this.facil = new ArrayList<Pair>();
		this.mitja = new ArrayList<Pair>();
		this.dificil = new ArrayList<Pair>();
	}
	
	//Getters
	public ArrayList<Pair> getRankFacil(){
		return facil;
	}
	
	public ArrayList<Pair> getRankMitja(){
		return mitja;
	}
	public ArrayList<Pair> getRankDificil(){
		return dificil;
	}
	
	
	//Donada una dificultat, user i temps afegeix l'usuari user a la llista depenent de la dificultat donada
	// si i nomes si el temps que ha fet el mateix usuari es millor que el seu anterior record.
	// facil = 0 , mitja = 1 , dificil = 2
	public void afegirUser(int dificultat,String user,Integer temps){
		Pair par= new Pair(user,temps);
		ListIterator<Pair> it = null;
		ListIterator<Pair> it2 = null;
		Pair i;
		if(dificultat == 0){
			it = facil.listIterator();
			it2 = facil.listIterator();
		}
		else if (dificultat == 1){
			it = mitja.listIterator();
			it2 = mitja.listIterator();
		}
		else{
			it = dificil.listIterator();
			it2 = dificil.listIterator();
		}
		
		boolean esrecord=false;
		boolean trobat = false;
		while(it2.hasNext() && !trobat){
			i = it2.next();
			if(i.getUsuari().equals(user)){
				trobat = true;
				if(i.getTemps() > temps){
					it2.remove();
					esrecord = true;
				}
			}
		}
		boolean posat = false;
		if(!it.hasNext()){
			it2.add(par);
			posat = true;
		}
		
		
		int count = 0;
		it = facil.listIterator();
		
		if(dificultat == 0) it = facil.listIterator();
		else if(dificultat == 1)it = mitja.listIterator();
		else it = dificil.listIterator();
			
			
		while (it.hasNext() && !posat && ((trobat && esrecord) || !trobat)) {
		i = it.next();
		if(i.getTemps() > temps ||i.getTemps() == temps){
			if(dificultat == 0) facil.add(count, par);
			else if(dificultat == 1) mitja.add(count, par);
			else dificil.add(count, par);
			posat = true;
			}
		++count;
		}
		if((!posat && (!trobat || (trobat && esrecord)))) it.add(par);
	}
	
	/*Afegeix una nova entrada a la taula de ranking facil
	 * 
	 */
	public void AfegirEntradaFacil(String usuari, int temps){
		Pair p = new Pair( usuari,  temps);
		facil.add(p);
	}
	
	/*Afegeix una nova entrada a la taula de ranking mitja
	 * 
	 */
	public void AfegirEntradaMitjana(String usuari, int temps){
		Pair p = new Pair( usuari,  temps);
		mitja.add(p);
	}
	
	/*Afegeix una nova entrada a la taula de ranking dificil
	 * 
	 */
	public void AfegirEntradaDificil(String usuari, int temps){
		Pair p = new Pair( usuari,  temps);
		dificil.add(p);
	}
}
