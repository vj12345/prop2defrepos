package domini.Ranking;

public class Pair {
	private String usuari;
	private Integer temps;
	
	//Constructora per defecte
	public Pair(){
		this.usuari = "";
		this.temps = 0;
	}
	
	//Constructor amb els parametres usuari i temps 
	public Pair(String usuari, int temps){
		this.usuari = usuari;
		this.temps = temps;
	}
	
	//Getters i setters
	public void setUsuari(String usuari){
		this.usuari = usuari;
	}
	
	public void setTemps(int temps){
		this.temps = temps;
	}
	
	public String getUsuari(){
		return usuari;
	}
	
	public int getTemps(){
		return temps;
	}
}
