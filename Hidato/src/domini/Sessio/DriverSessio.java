package domini.Sessio;

import java.util.ArrayList;
import java.util.Scanner;
import domini.Hidato.*;

import domini.Hidato.Hidato;
import domini.Hidato.Hidato.Adjacencia;
import domini.Hidato.Hidato.Topologia;
import domini.Joc.Joc;
import domini.Joc.Joc.Tipus;
import domini.Ranking.Pair;
import domini.Ranking.Ranking;
import domini.Usuari.Usuari;

public class DriverSessio {
	private final static String Enmarcat = "###########################################################";
	private final static String UserTemps = "USUARI						TEMPS";
	public static void main(String[] args) {
		try {
			Scanner linea = new Scanner(System.in);
			Sessio sessio = Sessio.getInstance();

			int opcio, r;
			int k = 0;
			String par, topologia;
			Tipus tipus;
			Topologia top;
			Tipus tipusCela;
			Adjacencia adj;
			String dificultat,user,pasw,nh,tip;
			Exception ex = new Exception("Has introduit malament el que et demanavem, torna-ho a provar");
			System.out.println("Benvolgut al Driver de Sessio");
			System.out.println("Es obligatori registrar-se per poder jugar");
			System.out.println("Escrigui el nom d'usuari del nou compte");
			user = linea.nextLine();
			System.out.println("Escrigui la contrase�a del nou compte");
			pasw = linea.nextLine();
			sessio.registrarUser(user,pasw);
			System.out.println("El registre s'ha completat exitosament");
			boolean ini = true;
			
			while (ini) {
				System.out.println("Selecciona la opci� que vols efectuar amb el numero");
				System.out.println("1 -> Joc r�pid");
				System.out.println("2 -> Joc personalitzat");
				System.out.println("3 -> Joc de la Base de Dades");
				System.out.println("4 -> Carregar Partida");
				System.out.println("5 -> Crear Joc");
				System.out.println("6 -> Mostrar Ranking");
				System.out.println("7 -> exit");
				opcio = linea.nextInt();
				linea.nextLine();
				if (opcio == 1) {
					System.out.println("Escrigui el tipus de cela (Escriu TRIANGLE o QUADRAT o HEXAGON)");
					par = linea.nextLine();
					if (par.equals("TRIANGLE"))
						tipus = Tipus.TRIANGLE;
					else if (par.equals("QUADRAT"))
						tipus = Tipus.QUADRAT;
					else if (par.equals("HEXAGON"))
						tipus = Tipus.HEXAGON;
					else
						throw ex;
					System.out.println("Escrigui la dificultat(easy,medium,dificil)");
					dificultat = linea.nextLine();
					System.out.println("Escriu l'adjacencia del hidato (Escriu C per costat o CA per costat i aresta)");
					par = linea.nextLine();
					if (par.equals("C"))
						adj = Adjacencia.C;
					else if (par.equals("CA"))
						adj = Adjacencia.CA;
					else
						throw ex;
					System.out.println("Escriu la topologia del hidato(Escriu TRIANGLE o QUADRAT o RECTANGLE)");
					par = linea.nextLine();
					System.out.println(par);
					if (par.equals("TRIANGLE"))
						top = Topologia.TRIANGLE;
					else if (par.equals("QUADRAT"))
						top = Topologia.QUADRAT;
					else if (par.equals("RECTANGLE"))
						top = Topologia.RECTANGLE;
					else
						throw ex;
					sessio.jocRapid(tipus, top, adj, dificultat);
					
					//jugar
					
					Joc a = sessio.getJoc();
					a.startTimer();
					System.out.println("HAS DE SOLUCIONAR EL SEG�ENT HIDATO:");
					int [][]s = a.getCurrentHidato();
					printHid(s);
		
					boolean solucionat = false;
					while (! solucionat) {
						System.out.println("Selecciona la opci� que vols efectuar amb el numero");
						System.out.println("1 -> Inserir numero");
						System.out.println("2 -> Obtenir ajuda");
						System.out.println("3 -> Guardar i sortir");
						System.out.println("4 -> Sortir sense guardar");
						System.out.println("5 -> Ja he acabat!");
						opcio = linea.nextInt();
						linea.nextLine();
						
						if (opcio == 1) {
							System.out.println("Introduieix la fila, columna i valor en una linia separat per un espai");
							int x = linea.nextInt();
							int y = linea.nextInt();
							int value = linea.nextInt();
							a.modificarCasella(x, y, value);
							System.out.println("Tauler actual:");
							printHid(a.getCurrentHidato());
							linea.nextLine();
							
						}
						else if (opcio == 2) {
							System.out.println("Comprovant la correctesa del hidato actual ...");
							if (a.comprovarCorrectesa()) {
								System.out.println("El que has fet fins ara es correcte");
							}
							else {
								System.out.println("Alguns dels valors que has ficat son incorrectes");
							}
							System.out.println("Tauler actual:");
							printHid(a.getCurrentHidato());
						}
						else if (opcio == 3) {
							System.out.println("Asigna un id per guardar el hidato");
							String id = linea.nextLine();
							sessio.getUser().afegirPartida(a.guardarJoc(id, 0));
							System.out.println("Partit guardat");
							solucionat = true;
						}
						else if (opcio == 4) {
							System.out.println("Has sortit sense guardar.");
							solucionat = true;
						}
						else if (opcio == 5) {
							
							if (!solucionat) {
								boolean totple = true;
								int sol[][] = a.getCurrentHidato();
								for (int i = 0; i < sol.length; i++) {
									for (int j = 0; j < sol[0].length; j++) {
										if (sol[i][j] == -1) totple = false;
									}
								}
								
								if (totple && a.comprovarCorrectesa()) {
									System.out.println("Felicitats, has solucionat el hidato");
									solucionat = true;
									a.endTimer();
									int seg = a.getTimelapse();
									int dif = 0;
									if (dificultat.equals("easy")) dif = 0;
									else if (dificultat.equals("medium")) dif = 1;
									else if (dificultat.equals("dificil")) dif = 2;

									sessio.mostrarRanking().afegirUser(dif, user, seg);
								}
								else {
									System.out.println("Solucio incorrecte Torna a intentar");
								}
							}
						}
						
						
					}
				
					
					//System.out.println("El registre s'ha completat exitosament");
				}
				else if (opcio == 2) {
					System.out.println("Escrigui el tipus de cela (Escriu TRIANGLE o QUADRAT o HEXAGON)");
					par = linea.nextLine();
					if (par.equals("TRIANGLE"))
						tipus = Tipus.TRIANGLE;
					else if (par.equals("QUADRAT"))
						tipus = Tipus.QUADRAT;
					else if (par.equals("HEXAGON"))
						tipus = Tipus.HEXAGON;
					else
						throw ex;
			
					
					System.out.println("Escrigui el nom del hidato:");
					String nomh = linea.nextLine();
					
					System.out.println("Escriu el numero celes totals (nomes poden ser 9, 16 o 25):");
					int nc = linea.nextInt();
					linea.nextLine();
					System.out.println("Escriu el numero de forats(maxim 1 per cada 9 celes, excepte per les celes triangulars amb topologia triangular i adjacencia de costat, que seria 3 + maxim 1 per cada 9 celes):");
					int f = linea.nextInt();
					linea.nextLine();
					
					System.out.println("Escriu el numero de celes per defecte (minim 2)");
					int cd = linea.nextInt();
					linea.nextLine();
					
					System.out.println("Escriu l'adjacencia del hidato (Escriu C per costat o CA per costat i aresta)");
					par = linea.nextLine();
					if (par.equals("C"))
						adj = Adjacencia.C;
					else if (par.equals("CA"))
						adj = Adjacencia.CA;
					else
						throw ex;
					System.out.println("Escriu la topologia del hidato(Escriu TRIANGLE o QUADRAT o RECTANGLE, per celes quadrades nomes existeix la topologia quadrada, pels hexagonals nomes top. quadrada i per les triangulars nomes top. triangular )");
					par = linea.nextLine();
					System.out.println(par);
					if (par.equals("TRIANGLE"))
						top = Topologia.TRIANGLE;
					else if (par.equals("QUADRAT"))
						top = Topologia.QUADRAT;
					else if (par.equals("RECTANGLE"))
						top = Topologia.RECTANGLE;
					else
						throw ex;
					sessio.jocPersonalitzat(adj, nomh, top, f, nc, cd, tipus, sessio.getUser().obtindreUsuari());
					
					//jugar
					Joc a = sessio.getJoc();
					System.out.println("HAS DE SOLUCIONAR EL SEG�ENT HIDATO:");
					int [][]s = a.getCurrentHidato();
					printHid(s);
		
					boolean solucionat = false;
					while (! solucionat) {
						System.out.println("Selecciona la opci� que vols efectuar amb el numero");
						System.out.println("1 -> Inserir numero");
						System.out.println("2 -> Obtenir ajuda");
						System.out.println("3 -> Guardar i sortir");
						System.out.println("4 -> Sortir sense guardar");
						System.out.println("5 -> Ja he acabat!");
						opcio = linea.nextInt();
						linea.nextLine();
						
						if (opcio == 1) {
							System.out.println("Introduieix la fila, columna i valor en una linia separat per un espai");
							int x = linea.nextInt();
							int y = linea.nextInt();
							int value = linea.nextInt();
							a.modificarCasella(x, y, value);
							System.out.println("Tauler actual:");
							printHid(a.getCurrentHidato());
							linea.nextLine();
							
						}
						else if (opcio == 2) {
							System.out.println("Comprovant la correctesa del hidato actual ...");
							if (a.comprovarCorrectesa()) {
								System.out.println("El que has fet fins ara es correcte");
							}
							else {
								System.out.println("Alguns dels valors que has ficat son incorrectes");
							}
							System.out.println("Tauler actual:");
							printHid(a.getCurrentHidato());
						}
						else if (opcio == 3) {
							System.out.println("Asigna un id per guardar el hidato");
							String id = linea.nextLine();
							sessio.getUser().afegirPartida(a.guardarJoc(id, 0));
							System.out.println("Partit guardat");
							solucionat = true;
						}
						else if (opcio == 4) {
							System.out.println("Has sortit sense guardar.");
							solucionat = true;
						}
						else if (opcio == 5) {
							
							if (!solucionat) {
								boolean totple = true;
								int sol[][] = a.getCurrentHidato();
								for (int i = 0; i < sol.length; i++) {
									for (int j = 0; j < sol[0].length; j++) {
										if (sol[i][j] == -1) totple = false;
									}
								}
								
								if (totple && a.comprovarCorrectesa()) {
									System.out.println("Felicitats, has solucionat el hidato");
									solucionat = true;
								}
								else {
									System.out.println("Solucio incorrecte Torna a intentar");
								}
							}
						}
						
						
					}
				}
				else if (opcio == 3) {
					System.out.println("Escrigui el numero del joc");
					PartidaGuardada hid ;
					if(sessio.HidatosCreats.size() == 0)System.out.println("No hi ha jocs disponibles");
					else{
						for (int i = 0; i < sessio.HidatosCreats.size(); ++i) {
							System.out.println(i+"-> Nom del hidato: " + sessio.HidatosCreats.get(i).getNomHidato());
						}
						int p;
						p = linea.nextInt();
						hid = sessio.joc_BD(p);
						
						//jugar
						Joc a = sessio.getJoc();
						a.carregaHidato(hid);
						System.out.println("HAS DE SOLUCIONAR EL SEG�ENT HIDATO:");
						int [][]s = a.getCurrentHidato();
						printHid(s);
			
						boolean solucionat = false;
						while (! solucionat) {
							System.out.println("Selecciona la opci� que vols efectuar amb el numero");
							System.out.println("1 -> Inserir numero");
							System.out.println("2 -> Obtenir ajuda");
							System.out.println("3 -> Guardar i sortir");
							System.out.println("4 -> Sortir sense guardar");
							System.out.println("5 -> Ja he acabat!");
							opcio = linea.nextInt();
							linea.nextLine();
							
							if (opcio == 1) {
								System.out.println("Introduieix la fila, columna i valor en una linia separat per un espai");
								int x = linea.nextInt();
								int y = linea.nextInt();
								int value = linea.nextInt();
								a.modificarCasella(x, y, value);
								System.out.println("Tauler actual:");
								printHid(a.getCurrentHidato());
								linea.nextLine();
								
							}
							else if (opcio == 2) {
								System.out.println("Comprovant la correctesa del hidato actual ...");
								if (a.comprovarCorrectesa()) {
									System.out.println("El que has fet fins ara es correcte");
								}
								else {
									System.out.println("Alguns dels valors que has ficat son incorrectes");
								}
								System.out.println("Tauler actual:");
								printHid(a.getCurrentHidato());
							}
							else if (opcio == 3) {
								System.out.println("Asigna un id per guardar el hidato");
								String id = linea.nextLine();
								sessio.getUser().afegirPartida(a.guardarJoc(id,0));
								System.out.println("Partit guardat");
								solucionat = true;
							}
							else if (opcio == 4) {
								System.out.println("Has sortit sense guardar.");
								solucionat = true;
							}
							else if (opcio == 5) {
								
								if (!solucionat) {
									boolean totple = true;
									int sol[][] = a.getCurrentHidato();
									for (int i = 0; i < sol.length; i++) {
										for (int j = 0; j < sol[0].length; j++) {
											if (sol[i][j] == -1) totple = false;
										}
									}
									
									if (totple && a.comprovarCorrectesa()) {
										System.out.println("Felicitats, has solucionat el hidato");
										solucionat = true;
									}
									else {
										System.out.println("Solucio incorrecte Torna a intentar");
									}
								}
							}
							
							
						}
					}
					
			
				}
				else if (opcio == 4) {
					System.out.println("Tria la partida guardada");
					if(sessio.getUser().getPartidesGuardades().size() == 0)System.out.println("No hi han partides guardades");
					else{
						for(int i = 0; i < sessio.getUser().getPartidesGuardades().size(); i++){
							System.out.println(i+"-> Nom de partida: " + sessio.getUser().getPartidesGuardades().get(i).getId());
						}
					int p;
					p = linea.nextInt();
					PartidaGuardada partida = sessio.getUser().getPartidesGuardades().get(p);
					sessio.getJoc().carregaHidato(partida);
					
					//jugar
					Joc a = sessio.getJoc();
					System.out.println("HAS DE SOLUCIONAR EL SEG�ENT HIDATO:");
					int [][]s = a.getCurrentHidato();
					printHid(s);
		
					boolean solucionat = false;
					while (! solucionat) {
						System.out.println("Selecciona la opci� que vols efectuar amb el numero");
						System.out.println("1 -> Inserir numero");
						System.out.println("2 -> Obtenir ajuda");
						System.out.println("3 -> Guardar i sortir");
						System.out.println("4 -> Sortir sense guardar");
						System.out.println("5 -> Ja he acabat!");
						opcio = linea.nextInt();
						linea.nextLine();
						
						if (opcio == 1) {
							System.out.println("Introduieix la fila, columna i valor en una linia separat per un espai");
							int x = linea.nextInt();
							int y = linea.nextInt();
							int value = linea.nextInt();
							a.modificarCasella(x, y, value);
							System.out.println("Tauler actual:");
							printHid(a.getCurrentHidato());
							linea.nextLine();
							
						}
						else if (opcio == 2) {
							System.out.println("Comprovant la correctesa del hidato actual ...");
							if (a.comprovarCorrectesa()) {
								System.out.println("El que has fet fins ara es correcte");
							}
							else {
								System.out.println("Alguns dels valors que has ficat son incorrectes");
							}
							System.out.println("Tauler actual:");
							printHid(a.getCurrentHidato());
						}
						else if (opcio == 3) {
							System.out.println("Asigna un id per guardar el hidato");
							String id = linea.nextLine();
							sessio.getUser().afegirPartida(a.guardarJoc(id,0));
							System.out.println("Partit guardat");
							solucionat = true;
						}
						else if (opcio == 4) {
							System.out.println("Has sortit sense guardar.");
							solucionat = true;
						}
						else if (opcio == 5) {
							
							if (!solucionat) {
								boolean totple = true;
								int sol[][] = a.getCurrentHidato();
								for (int i = 0; i < sol.length; i++) {
									for (int j = 0; j < sol[0].length; j++) {
										if (sol[i][j] == -1) totple = false;
									}
								}
								
								if (totple && a.comprovarCorrectesa()) {
									System.out.println("Felicitats, has solucionat el hidato");
									solucionat = true;
								}
								else {
									System.out.println("Solucio incorrecte Torna a intentar");
								}
							}
							System.out.println("Tauler actual:");
							printHid(a.getCurrentHidato());
						}
						
						
					}
				
				
					
					}
				}
				else if (opcio == 5) {
					System.out.println("Introdeuix el nom del Hidato que vols crear");
					nh = linea.nextLine();
					System.out.println("Introdeuix la topologia del hidato que vols crear(Escriu TRIANGLE o QUADRAT o RECTANGLE)");
					par = linea.nextLine();
					if (par.equals("TRIANGLE"))
						top = Topologia.TRIANGLE;
					else if (par.equals("QUADRAT"))
						top = Topologia.QUADRAT;
					else if (par.equals("RECTANGLE"))
						top = Topologia.RECTANGLE;
					else
						throw ex;
					System.out.println("Introdeuix l'adjacencia del Hidato que vols crear(Escriu C per costat o CA per costat i aresta)");
					par = linea.nextLine();
					if (par.equals("CA"))
						adj = Adjacencia.CA;
					else if (par.equals("C"))
						adj = Adjacencia.C;
					else throw ex;
					System.out.println("Introdeuix el tipus de cel�la(Escriu TRIANGLE o QUADRAT o HEXAGON)");
					par = linea.nextLine();
					if (par.equals("TRIANGLE"))
						tipusCela = Tipus.TRIANGLE;
					else if (par.equals("QUADRAT"))
						tipusCela = Tipus.QUADRAT;
					else if (par.equals("HEXAGON"))
						tipusCela = Tipus.HEXAGON;
					else
						throw ex;
					int files,columnes,nombre;
					System.out.println("Introdueix el numero de files, maxim 5");
					files = linea.nextInt();
					System.out.println("Introdueix el numero de columnes, maxin 5");
					columnes = linea.nextInt();
					int[][] tauler_original = new int[files][columnes];
					System.out.println("Escriu l'hidato modificat per files i separat per un espai, Recorda que les cel�les buides es codifiquen amb un -1, les cel�les amb forat -2, "
							+ "les cel�les que no formen part del tauler amb un -3");
					for (int i = 0; i < tauler_original.length; i++) {
						for (int j = 0; j < tauler_original[0].length; j++) {
							nombre = linea.nextInt();
							tauler_original[i][j] = nombre;
						}
					}
					boolean escorrecte = false;
				
					
					PartidaGuardada hidato = sessio.getJoc().proposarHidato(tauler_original, nh, adj, top, tipusCela, nh, sessio.getUser().obtindreUsuari());
					printHid(hidato.getTableroGuardado());
					if (!hidato.equals(null)) {
						escorrecte = sessio.CrearJoc(hidato); 
						System.out.println("hidato correcte, vols veure la solucio? (S/N)");
						linea.nextLine();
						String s = linea.nextLine();
						if (s.equals("S")) {
							System.out.println("La seva solucio es:");
							printHid(sessio.getJoc().getTaulerSolucionat());
							
						}
					}

		
					if(escorrecte) System.out.println("L'hidato s'ha guardat correctament");
					else System.out.println("L'hidato es incorrecte");
				}
				else if (opcio == 6) {
						
					Ranking rank =  sessio.mostrarRanking();
					System.out.println("1 -> Obtenir Ranking facil");
					System.out.println("2 -> Obtindre Ranking mitja");
					System.out.println("3 -> ComprovarRanking dificil");
					r = linea.nextInt();
					MostrarRanking(r,rank);
					}
				else if (opcio == 7) {
					ini = false;
				}
				}
		} catch (Exception e) {
			System.out.println("Hi ha hagut una excepci�");
		}
	}

	private static void printHid(int[][] hid) {
		for (int i = 0; i < hid.length; i++) {
			for (int j = 0; j < hid[0].length; j++) {
				System.out.print(hid[i][j] + " ");
			}
			System.out.println();
		}
	}


	private static void MostrarRanking(int opcio,Ranking rank){
		if(opcio == 1) {
			System.out.println(Enmarcat);
			System.out.println("RANKING DE DIFICULTAT FACIL");
			System.out.println(UserTemps);
			ArrayList<Pair> facil = rank.getRankFacil();
			for(Pair p: facil)
				System.out.println(p.getUsuari() + "						" + p.getTemps());
			
			System.out.println(Enmarcat);
		}
		
		if(opcio == 2) {
			System.out.println(Enmarcat);
			System.out.println("RANKING DE DIFICULTAT MITJA");
			System.out.println(UserTemps);
			ArrayList<Pair> facil = rank.getRankMitja();
			
			for(Pair p: facil)
				System.out.println(p.getUsuari() + "						" + p.getTemps());
			System.out.println(Enmarcat);
		}
		if(opcio == 3) {
			System.out.println(Enmarcat);
			System.out.println("RANKING DE DIFICULTAT DIFICIL");
			System.out.println(UserTemps);
			ArrayList<Pair> facil = rank.getRankDificil();
			
			for(Pair p: facil)
				System.out.println(p.getUsuari() + "						" + p.getTemps());
			System.out.println(Enmarcat);
		}
	}
}