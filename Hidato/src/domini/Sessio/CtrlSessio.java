package domini.Sessio;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CountDownLatch;

import domini.Hidato.Hidato.Adjacencia;
import domini.Hidato.Hidato.Topologia;
import domini.Hidato.PartidaGuardada;
import domini.Joc.Joc;
import domini.Joc.Joc.Tipus;
import domini.Ranking.Pair;
import domini.Ranking.Ranking;
import domini.Usuari.Usuari;
import persistencia.CtrlPersistencia;
import persistencia.UsuariDB;

public class CtrlSessio {
	
	private CtrlPersistencia iCtrlPersistencia;
	
	Sessio sessio;

	public CtrlSessio() {
		
		sessio = Sessio.getInstance();
		iCtrlPersistencia = new CtrlPersistencia();
	}
	
	///////////////////////Ranking
	
	/*Retorna la llista de ranking segons el tipus, que pot ser 1, 2 o 3 (facil, mitja, dificil)
	 *  en una lista amb elements que es una lista de dos elements, el nom i el temps
	 *Pre: tipus = 1 ,2 o 3
	 *Post: Retorna la llista de ranking segons el tipus (facil, mitja , dificil)
	 * */
	public List<List<String>> getRanking(int tipus) {

		Ranking r = sessio.mostrarRanking();

		ArrayList<Pair> f;
		List<List<String>> aux = new ArrayList<List<String>>();;

		switch(tipus) {
			case  1: f = r.getRankFacil();
					for (Pair p : f) {
					ArrayList<String> nomTemps = new ArrayList<String>();
					nomTemps.add(p.getUsuari());
					nomTemps.add(Integer.toString(p.getTemps()));
					aux.add(nomTemps);
					}
					break;
			case 2: f = r.getRankMitja();
					for (Pair p : f) {
					ArrayList<String> nomTemps = new ArrayList<String>();
					nomTemps.add(p.getUsuari());
					nomTemps.add(Integer.toString(p.getTemps()));
					aux.add(nomTemps);
					}
					break;
			case 3: f = r.getRankDificil();
					for (Pair p : f) {
					ArrayList<String> nomTemps = new ArrayList<String>();
					nomTemps.add(p.getUsuari());
					nomTemps.add(Integer.toString(p.getTemps()));
					aux.add(nomTemps);
					}
					break;
			default: 

					break;
		}
		return aux;
	}
	
	///////////////////////////////////////Usuaris
	/*Crea un usuari nou.
	 * Pre:Cert
	 *Posr:true si s'ha pogut crear l'usuari.
	 * */
	public boolean newUsuari(String username, String password) {
		return iCtrlPersistencia.insertUsuari(username, password);
	}
	
	public boolean login(String username, String password) {
		boolean valid = false;
		valid = iCtrlPersistencia.identificar(username, password);
		if (valid) {
			sessio.registrarUser(username, password);
		}
		return valid;
	}

	/*Pre: l'usuari user ja existeix.
	 *Post: modifica la contrasenya amb newPsw si el oldPsw es correcte.
	 * */
	public boolean changePsw(String oldPsw, String newPsw) {
		if (!sessio.getUser().ComprovarPassword(oldPsw)) {
			return false;
		}
		else {
			sessio.getUser().changePsw(newPsw);
			iCtrlPersistencia.changePsw(sessio.getUser().obtindreUsuari(), newPsw);
		}
		return true;
	}
	
	
	
	////////////////////////PROPOSAR HIDATO
	PartidaGuardada hidatoProposat;
	
	//retorna true si el hidato que esta en el tauler es correcte.
	public boolean proposarHidato(int[][] tauler, String nomHid, String adjacencia, String tCela, int files, int columnes) {
		Topologia top;
		top = Topologia.OTHERS;
		
		Adjacencia adj;
		if (adjacencia.equals("COSTAT")) {
			adj = Adjacencia.C;
		}
		else {
			adj = Adjacencia.CA;
		}
		
		Tipus tipusCela;
		
		if (tCela.equals("TRIANGULAR")) {
			tipusCela = Tipus.TRIANGLE;
		}
		else if (tCela.equals("QUADRADA")) {
			tipusCela = Tipus.QUADRAT;
			
		}
		else {
			tipusCela = Tipus.HEXAGON;
		}
		
		
		hidatoProposat = sessio.getJoc().proposarHidato(tauler, nomHid, adj, top, tipusCela, nomHid, sessio.getUser().obtindreUsuari());

		if (!(hidatoProposat == null)) {
			
			return true;
		}
	
		return false;
	}
	
	public boolean guardarHidatoProposat() {
		if (!(hidatoProposat==null)) {
			return sessio.CrearJoc(hidatoProposat); 
		
		}
		
		return false;
	}
	
	public int[][] obtenirSolu() {
		return hidatoProposat.getTableroOriginal();
	}
	public int[][] obtenirSolu2() {
		return hidatoProposat.getTableroGuardado();
	}
	//comprova si el nom del hidato nomh ja existeix
	public boolean NomHidatoUsable(String nomh) {
		
		
		ArrayList<PartidaGuardada> hcreats = sessio.getHidatosCreats();
		
		for (int i = 0; i < hcreats.size(); i++) {
			if (hcreats.get(i).getNomHidato().equals(nomh)) {

				return false;
			}
		}
	
		return true;
	}
	
	
	/////////////////////////////////////JOC RAPID
	boolean trobat = false;
	//cera el jov rapid i retorna el tauler creat
	public int[][] crearJocRapid(String tCela, String top, String adj, String dif) {
		Tipus t;
		if (tCela.equals("TRIANGULAR")) {
			t = Tipus.TRIANGLE;
		}
		else if (tCela.equals("QUADRADA")) {
			t = Tipus.QUADRAT;
		}
		else {
			t = Tipus.HEXAGON;
		}
		
		Topologia topo;
		if (top.equals("TRIANGULAR")) {
			topo = Topologia.TRIANGLE;
		}
		else if (top.equals("QUADRADA")) {
			topo = Topologia.QUADRAT;
		}
		else {
			topo = Topologia.OTHERS;
		}
		
		Adjacencia adja;
		if (adj.equals("COSTAT")) {
			adja = Adjacencia.C;
		}
		else {
			adja = Adjacencia.CA;
		}
		String dificultat;
		if (dif.equals("FACIL")) {
			dificultat = "easy";
		}
		else if (dif.equals("MITJA")) {
			dificultat = "medium";
		}
		else {
			dificultat = "dificil";
		}
		
		/*
		final CountDownLatch latch = new CountDownLatch(1);
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				System.out.println("comenšant un nou intent");
				sessio.jocRapid(t, topo, adja, dificultat);
				System.out.println("acabat un nou intent");
				latch.countDown();
				timer.cancel();
			}
			
		}, 100, 500);
		
		try {
			latch.await();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		

		sessio.jocRapid(t, topo, adja, dificultat);

		
	///	sessio.jocRapid(t, topo, adja, dificultat);

		Joc a = sessio.getJoc();
		
		return a.getCurrentHidato();
	}
	
	//fica el tauler en el joc actual i comprova la seva correctesa.
	public boolean comprovarCor(int[][] board) {
		sessio.getJoc().setCurrentHidato(board);
		return sessio.getJoc().comprovarCorrectesa();
				
	}
	public int[][] obtenirSoluJoc(int[][]b) {
		sessio.getJoc().setCurrentHidato(b);
		return sessio.getJoc().getRes();
	}
	
	public void startTimer() {
		sessio.getJoc().startTimer();
	}
	public void endTiemr() {
		sessio.getJoc().endTimer();
	}
	public int getTimeElapse() {
		return sessio.getJoc().getTimelapse();
	}

	public void afegirRanking(int dif, String user, int sec) {
		sessio.afegirPuntuacio(dif, user, sec);
	}

	public String getUsername() {
		// TODO Auto-generated method stub
		return sessio.getUser().obtindreUsuari();
	}

	public void guardarPartida(String id, int temps, int[][] board) {
		sessio.getJoc().setCurrentHidato(board);
		// TODO Auto-generated method stub
		sessio.getUser().afegirPartida(sessio.getJoc().guardarJoc(id, temps));
		
		//per fer la aprova 
		sessio.getUser().printPartidesGuadades();
	}

	//mira si el id amb el que es vol guardar la partida ja esta fet servir.
	public boolean nomidPartidaGuardadaUsable(String id) {

		ArrayList<PartidaGuardada> hcreats = sessio.getUser().getPartidesGuardades();
		
		for (int i = 0; i < hcreats.size(); i++) {
			if (hcreats.get(i).getId().equals(id)) {

				return false;
			}
		}
	
		return true;
	}
	
	
	////////////////////////////JOC PERSONALITZAT
	public int[][] crearJocRapid(String tCela, String top, String adj, String nomHid, int nCeles, int forats, int defectes) {
		Tipus t;
		if (tCela.equals("TRIANGULAR")) {
			t = Tipus.TRIANGLE;
		}
		else if (tCela.equals("QUADRADA")) {
			t = Tipus.QUADRAT;
		}
		else {
			t = Tipus.HEXAGON;
		}
		
		Topologia topo;
		if (top.equals("TRIANGULAR")) {
			topo = Topologia.TRIANGLE;
		}
		else if (top.equals("QUADRADA")) {
			topo = Topologia.QUADRAT;
		}
		else {
			topo = Topologia.OTHERS;
		}
		
		Adjacencia adja;
		if (adj.equals("COSTAT")) {
			adja = Adjacencia.C;
		}
		else {
			adja = Adjacencia.CA;
		}
		
		sessio.jocPersonalitzat(adja, nomHid, topo, forats, nCeles, defectes, t, sessio.getUser().obtindreUsuari());
		
		return sessio.getJoc().getCurrentHidato();
	}
	
	
	
	///////////////////////////Joc proposat
	//retorna un arraylist on conte n vegades la combinacio de nom hidato + nom autor 
	// es igual al primer valor.
	public ArrayList<String> getHidatosProposats() {
		ArrayList<String> ret = new ArrayList<String>();
		ArrayList<PartidaGuardada> hc = sessio.getHidatosCreats();
		for (int i = 0; i < hc.size(); i++) {
			ret.add(hc.get(i).getNomHidato());
			ret.add(hc.get(i).getNomAutor());
		}
		
		return ret;
		
	}
	
	//carrega el hidato proposat que esta a la poscio index de l'arraylist de hidatos proposats i retorna el tauler a ompllir
	public int[][] crearJocProposat(int index) {
		if (index == -1) {
			index = this.getHidatosProposats().size()-2;
		}
		PartidaGuardada hid ;
		Joc a = sessio.getJoc();
		hid = sessio.joc_BD(index);
		a.carregaHidato(hid);
		return hid.getTableroOriginal();
	}

	public String getTipusCelaHidatoProposat(int index) {
		if (index == -1) {
			index = this.getHidatosProposats().size()-2;
		}
		// TODO Auto-generated method stub
		Tipus t = sessio.getHidatosCreats().get(index).getTipus();
		if (t == Tipus.HEXAGON) {
			return "HEXAGONAL";
			
		}
		else if (t == Tipus.QUADRAT) {
			return "QUADRADA";
		}
		else {
			return "TRIANGULAR";
		}
		
	}

	
	public String getAdjHidatoProposat(int index) {
		if (index == -1) {
			index = this.getHidatosProposats().size()-2;
		}
		// TODO Auto-generated method stub
		Adjacencia adj = sessio.getHidatosCreats().get(index).getAdjacencia();
		if (adj == Adjacencia.C) {
			return "COSTAT";
		}
		else {
			return "COSTAT I VERTEX";
		}
		
	}

	public ArrayList<String> getAutorsHidatosProposats() {
		// TODO Auto-generated method stub
		ArrayList<String> autors = new ArrayList<String>();
		ArrayList<PartidaGuardada> hc = sessio.getHidatosCreats();
		for (int i = 0; i < hc.size(); i++) {
			if (!autors.contains(hc.get(i).getNomAutor())) {
				autors.add(hc.get(i).getNomAutor());
			}
		}
		return autors;
	}

	//retorna la poscio del hidato amb nom nomHidato dins del arraylist de hidatos creats, si no ho trova retorna -1;
	public int findPos(String nomHidato) {
		// TODO Auto-generated method stub
		ArrayList<PartidaGuardada> hc = sessio.getHidatosCreats();
		for (int i = 0 ; i < hc.size(); i++) {
			if (hc.get(i).getNomHidato().equals(nomHidato)) {
				return i;
			}
		}
		return -1;
	}

	public boolean borrarPartidaProposat(int index) {
		// TODO Auto-generated method stub
		ArrayList<PartidaGuardada> hc = sessio.getHidatosCreats();

		if (hc.get(index).getNomAutor().equals(sessio.getUser().obtindreUsuari())) {
			sessio.getHidatosCreats().remove(index);
			return true;
		}
		return false;
	}

	/////////////////////////JOC GUARDAT
	
	//retorna els identificadors de les partides guardades.
	public ArrayList<String> getHidatosGuardats() {
		// TODO Auto-generated method stub
		ArrayList<String> ret = new ArrayList<String>();
		for(int i = 0 ; i < sessio.getUser().getPartidesGuardades().size(); i++) {
			ret.add(sessio.getUser().getPartidesGuardades().get(i).getId());
		}
		return ret;
	}

	public int[][] crearJocGuardat(int pos) {
		// TODO Auto-generated method stub
		PartidaGuardada partida = sessio.getUser().getPartidesGuardades().get(pos);
		sessio.getJoc().carregaHidato(partida);
		//Joc a = sessio.getJoc();

		return partida.getTableroOriginal();
	}

	public String getTipusCelaHidatoGuardat(int pos) {
		// TODO Auto-generated method stub
		
		Tipus t = sessio.getUser().getPartidesGuardades().get(pos).getTipus();
		if (t == Tipus.HEXAGON) {
			return "HEXAGONAL";
			
		}
		else if (t == Tipus.QUADRAT) {
			return "QUADRADA";
		}
		else {
			return "TRIANGULAR";
		}
	}

	public String getAdjHidatoGuardat(int pos) {
		// TODO Auto-generated method stub
		Adjacencia adj = sessio.getUser().getPartidesGuardades().get(pos).getAdjacencia();
		if (adj == Adjacencia.C) {
			return "COSTAT";
		}
		else {
			return "COSTAT I VERTEX";
		}
	}

	//primer s'ha de haver fet crearJocGuardat()
	public int[][] getTaulerGuardat() {
		// TODO Auto-generated method stub
	
		Joc a = sessio.getJoc();

		return a.getCurrentHidato();
	}

	//borra la partida guardadda que esta a la poscicio i
	public void borrarPartidaGuardada(int i) {
		// TODO Auto-generated method stub
		sessio.getUser().getPartidesGuardades().remove(i);
	}
	
	/* Guarda els hidatos Creats de la sessio
	 * Tablero modificat conte la solucio del tauler
	 * Pre: Cert
	Post :Retorna cert si s'han pogut guardar els hidatos
	*/
	public boolean GuardarHidatosCreats(){
		 ArrayList<PartidaGuardada> Hi;
		 Hi = sessio.getHidatosCreats();
		 for(PartidaGuardada p : Hi){
			 String tipus;
				if (p.getTipus() == Tipus.TRIANGLE) {
					tipus = "T";
				}
				else if (p.getTipus() == Tipus.QUADRAT) {
					tipus = "Q";
				}
				else {
					tipus = "H";
				}
				
				String topo;
				if (p.getTopologia() == Topologia.TRIANGLE) {
					topo = "T";
				}
				else if (p.getTopologia() == Topologia.QUADRAT) {
					topo = "Q";
				}
				else if (p.getTopologia() == Topologia.RECTANGLE) {
					topo = "R";
				}
				else topo = "O";
				
				String adja;
				if (p.getAdjacencia() == Adjacencia.C) {
					adja = "C";
				}
				else {
					adja = "CA";
				}
				String id = p.getId();
				String nomH = p.getNomHidato();
				String nomAutor = p.getNomAutor();
				int [][] tablero_original = p.getTableroOriginal();
				int [][] tablero_mod = p.getTableroGuardado();
			 iCtrlPersistencia.guardarHidatos(tipus,topo,adja,id,nomH,nomAutor,tablero_original,tablero_mod);		 
			 }
		 return true;
	}
	
	public boolean GuardarPartidesGuardades(){
		ArrayList<PartidaGuardada> Hi;
		 Hi = sessio.getUser().getPartidesGuardades();
		 for(PartidaGuardada p : Hi){
			 String tipus;
				if (p.getTipus() == Tipus.TRIANGLE) {
					tipus = "T";
				}
				else if (p.getTipus() == Tipus.QUADRAT) {
					tipus = "Q";
				}
				else {
					tipus = "H";
				}
				
				String topo;
				if (p.getTopologia() == Topologia.TRIANGLE) {
					topo = "T";
				}
				else if (p.getTopologia() == Topologia.QUADRAT) {
					topo = "Q";
				}
				else if (p.getTopologia() == Topologia.RECTANGLE) {
					topo = "R";
				}
				else topo = "O";
				
				String adja;
				if (p.getAdjacencia() == Adjacencia.C) {
					adja = "C";
				}
				else {
					adja = "CA";
				}
				String id = p.getId();
				String nomH = p.getNomHidato();
				String nomAutor = p.getNomAutor();
				int [][] tablero_original = p.getTableroOriginal();
				int [][] tablero_mod = p.getTableroGuardado();
				int temps = p.getTempsGastat();
				iCtrlPersistencia.guardarPartida(temps,tipus,topo,adja,id,nomH,nomAutor,tablero_original,tablero_mod);		 
			 }
		 return true;
	}
	
	public void CargarHidatosCreats(){
		ArrayList<String> Hidatos = iCtrlPersistencia.CargarHidatosCreats();
		ArrayList<PartidaGuardada> HidatosCreats = new ArrayList<PartidaGuardada>();
		int temps,files,colum;
		String nomH, nomAutor,id,adj,top,tipus;
		int [][] tablero_original;
		int [][] tablero_guardado;
		for(String h: Hidatos){
			PartidaGuardada partida;
			String parts[] = h.split(" ");
			nomH = parts[0];
			System.out.println("Nom Hidato " + nomH );
			nomAutor = parts[1];
			System.out.println("Nom autor " + nomAutor );
			id = parts[2];
			System.out.println("Nom id " + id );
			temps =  Integer.parseInt(parts[3]);
			System.out.println("Temps  " + temps );
			tipus = parts[4];
			System.out.println("tipus " + tipus );
			adj = parts[5];
			System.out.println("adjacencia  " + adj );
			top = parts[6];
			System.out.println("top " + top);
			files = Integer.parseInt(parts[7]);
			System.out.println("Files " + files );
			colum = Integer.parseInt(parts[8]);
			System.out.println("Columnes " + colum);
			tablero_original = new int[files][colum];
			tablero_guardado = new int[files][colum];
			int lect = 9;
			System.out.println("Es mostrara el tauler");
			for(int i = 0; i < tablero_original.length; ++ i){
				for(int j = 0; j < tablero_original[i].length; ++j){
					tablero_original[i][j] = Integer.parseInt(parts[lect]);
					++lect;
					System.out.println(tablero_original[i][j] + "  ");
				}
			}
			
			for(int i = 0; i < tablero_guardado.length; ++ i){
				for(int j = 0; j < tablero_guardado[i].length; ++j){
					tablero_guardado[i][j] = Integer.parseInt(parts[lect]);
					++lect;
					System.out.println(tablero_guardado[i][j] + "  ");
				}
			}
			Topologia topO;
			if (top.equals("T")) {
				topO = Topologia.TRIANGLE;
			}
			else if (top.equals("Q")) {
				topO = Topologia.QUADRAT;
				
			}
			else if (top.equals("R")){
				topO = Topologia.RECTANGLE;
			}
			else topO = Topologia.OTHERS;
			
			Adjacencia adjA;
			if (adj.equals("C")) {
				adjA = Adjacencia.C;
			}
			else {
				adjA = Adjacencia.CA;
			}
			
			Tipus tipusCela;
			
			if (tipus.equals("T")) {
				tipusCela = Tipus.TRIANGLE;
			}
			else if (tipus.equals("Q")) {
				tipusCela = Tipus.QUADRAT;
				
			}
			else {
				tipusCela = Tipus.HEXAGON;
			}
			partida = new PartidaGuardada(tablero_original,tablero_guardado,id,adjA,topO,tipusCela,nomH,nomAutor,temps);
			HidatosCreats.add(partida);
		}
		sessio.setHidatosCreat(HidatosCreats);
	}

	public void CargarPartidesGuardades(){
		ArrayList<String> Hidatos = iCtrlPersistencia.CargarPartidesGuardades(sessio.getNomUsuari());
		ArrayList<PartidaGuardada> Partides = new ArrayList<PartidaGuardada>();
		int temps,files,colum;
		String nomH, nomAutor,id,adj,top,tipus;
		int [][] tablero_original;
		int [][] tablero_guardado;
		for(String h: Hidatos){
			PartidaGuardada partida;
			String parts[] = h.split(" ");
			nomH = parts[0];
			System.out.println("Nom Hidato " + nomH );
			nomAutor = parts[1];
			System.out.println("Nom autor " + nomAutor );
			id = parts[2];
			System.out.println("Nom id " + id );
			temps =  Integer.parseInt(parts[3]);
			System.out.println("Temps  " + temps );
			tipus = parts[4];
			System.out.println("tipus " + tipus );
			adj = parts[5];
			System.out.println("adjacencia  " + adj );
			top = parts[6];
			System.out.println("top " + top);
			files = Integer.parseInt(parts[7]);
			System.out.println("Files " + files );
			colum = Integer.parseInt(parts[8]);
			System.out.println("Columnes " + colum);
			tablero_original = new int[files][colum];
			tablero_guardado = new int[files][colum];
			int lect = 9;
			System.out.println("Es mostrara el tauler");
			for(int i = 0; i < tablero_original.length; ++ i){
				for(int j = 0; j < tablero_original[i].length; ++j){
					tablero_original[i][j] = Integer.parseInt(parts[lect]);
					++lect;
					System.out.println(tablero_original[i][j] + "  ");
				}
			}
			
			for(int i = 0; i < tablero_guardado.length; ++ i){
				for(int j = 0; j < tablero_guardado[i].length; ++j){
					tablero_guardado[i][j] = Integer.parseInt(parts[lect]);
					++lect;
					System.out.println(tablero_guardado[i][j] + "  ");
				}
			}
			Topologia topO;
			if (top.equals("T")) {
				topO = Topologia.TRIANGLE;
			}
			else if (top.equals("Q")) {
				topO = Topologia.QUADRAT;
				
			}
			else if (top.equals("R")){
				topO = Topologia.RECTANGLE;
			}
			else topO = Topologia.OTHERS;
			
			Adjacencia adjA;
			if (adj.equals("C")) {
				adjA = Adjacencia.C;
			}
			else {
				adjA = Adjacencia.CA;
			}
			
			Tipus tipusCela;
			
			if (tipus.equals("T")) {
				tipusCela = Tipus.TRIANGLE;
			}
			else if (tipus.equals("Q")) {
				tipusCela = Tipus.QUADRAT;
				
			}
			else {
				tipusCela = Tipus.HEXAGON;
			}
			partida = new PartidaGuardada(tablero_original,tablero_guardado,id,adjA,topO,tipusCela,nomH,nomAutor,temps);
			Partides.add(partida);
		}
		sessio.PosarPartidesGuardades(Partides);
	}
	
	public void GuardarRanking(){
		ArrayList<String> facil,mitja,dificil;
		facil = new ArrayList<String>();
		mitja = new ArrayList<String>();
		dificil = new ArrayList<String>();
		ArrayList<Pair> facilPair = sessio.GetRankingFacil();
		ArrayList<Pair> MitjaPair = sessio.GetRankingMitja();
		ArrayList<Pair> DificilPair = sessio.GetRankingDificil();
		for (Pair i : facilPair){
			String ent = i.getUsuari() + " " + i.getTemps(); 
			System.out.println("Dificultat facil" + ent);
			facil.add(ent);
		}
		for (Pair i : MitjaPair){
			String ent = i.getUsuari() + " " + i.getTemps(); 
			System.out.println("Dificultat mijta" + ent);
			mitja.add(ent);
		}
		for (Pair i : DificilPair){
			String ent = i.getUsuari() + " " + i.getTemps(); 
			System.out.println("Dificultat dificil" + ent);
			dificil.add(ent);
		}
		iCtrlPersistencia.GuardarRanking(facil, mitja, dificil);
	}
	
	public void CargarRanking(){
		
		ArrayList<String> Rankings = iCtrlPersistencia.CargarRankings();

		
		sessio.mostrarRanking().getRankDificil().clear();
		sessio.mostrarRanking().getRankMitja().clear();
		sessio.mostrarRanking().getRankFacil().clear();
		
		if(Rankings.size() != 0){
			String partfa[] = Rankings.get(0).split(" ");
			for(int i = 0; i < partfa.length-1; i = i+2){
				String usuari;
				int temps;
				usuari = partfa[i];
				System.out.println("WTF WIHYYY  : " + partfa[i+1] );
				temps = Integer.parseInt(partfa[i+1]);
				System.out.println("FACIL : USUARI : " + usuari + " TEMPS : " + temps);
				sessio.AfegirEntradaFacil(usuari, temps);
			}
			
			
			String partmit[] = Rankings.get(1).split(" ");
			for(int i = 0; i < partmit.length-1; i = i+2){
				String usuari;
				int temps;
				usuari = partmit[i];
				temps = Integer.parseInt(partmit[i+1]);
				System.out.println("FACIL : USUARI : " + usuari + " TEMPS : " + temps);
				sessio.AfegirEntradaMitjana(usuari, temps);
			}
			
			String partdif[] = Rankings.get(2).split(" ");
			for(int i = 0; i < partdif.length-1; i = i+2){
				String usuari;
				int temps;
				usuari = partdif[i];
				temps = Integer.parseInt(partdif[i+1]);
				System.out.println("FACIL : USUARI : " + usuari + " TEMPS : " + temps);
				sessio.AfegirEntradaDificil(usuari, temps);
			}
		}
			
	}
}


