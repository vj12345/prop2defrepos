package domini.Sessio;

import java.util.ArrayList;

import domini.Hidato.Hidato;
import domini.Hidato.PartidaGuardada;
import domini.Joc.*;
import domini.Hidato.Hidato.Adjacencia;
import domini.Hidato.Hidato.Topologia;
import domini.Joc.Joc.Tipus;
import domini.Ranking.Pair;
import domini.Ranking.Ranking;
import domini.Usuari.Usuari;


public class Sessio {
	private static Sessio single_sessio = null; //Instancia Singleton
	private Usuari user;
	private Joc jocActual;
	protected ArrayList<PartidaGuardada> HidatosCreats;
	
	
	private Ranking rank;


	//Constructora per defecte
	public Sessio(){
		this.user = null;
		jocActual = new Joc();
		rank = new Ranking();
		HidatosCreats = new ArrayList<PartidaGuardada>(); 
	}
	
	//Metode per que la classe sessio sigui Singleton
	public static Sessio getInstance() {
        if (single_sessio == null)
            single_sessio = new Sessio();
 
        return single_sessio;
	}
	
	//Getters 
	public Usuari getUser(){
		return user;
	}
	
	public String getNomUsuari(){
		return user.obtindreUsuari();
	}
	
	public Joc getJoc(){
		return jocActual;
	}
	

	//Setters
	public void setHidatosCreat(ArrayList<PartidaGuardada> pg){
		HidatosCreats = pg;
	}
	
	
	/* Posa les partides guardades en l'usuari
	 * Pre: Existeix alguna partida Guardada
	 */
	public void PosarPartidesGuardades(ArrayList<PartidaGuardada> pg){
		user.setPartidesGuardades(pg);
	}

	//Donat el tipus, topologia, adjacencia i dificultat genera una Hidato
	public void jocRapid(Tipus tipus,Topologia top,Adjacencia adj,String dificultat){
		jocActual.crearHidatoNou(tipus,top,adj,dificultat,user.obtindreUsuari());
	}
	
	
	public void jocPersonalitzat(Adjacencia adj, String nomh, Topologia top, int forats, int celes, int plenes, Tipus tipus, String nomAutor){
		jocActual.jocPersonalitzat(adj, nomh, top, forats, celes, plenes, tipus, nomAutor);
	}
	
	//Retorna un joc que previament ha sigut guardat per l'usuari
	public PartidaGuardada joc_BD(int p){
		return HidatosCreats.get(p);
	}
	
	
	// Carrega una partida previament guardada
	public void CarregarPartida(PartidaGuardada pg){
		jocActual.carregaHidato(pg);
	}
	
	//Es genera un hidato amb els parametres pasats per l'usuari i 
	//s'afegeix a la base de dades d'hidatos creats
	public boolean CrearJoc(PartidaGuardada hcreat){
		Joc j = new Joc();
		j.carregaHidato(hcreat);
		System.out.println("en crear joc: ");
		printHid(hcreat.getTableroOriginal());
		if (j.comprovarCorrectesa()) {
			System.out.println("en crear jo2: ");
			printHid(hcreat.getTableroGuardado());
			HidatosCreats.add(hcreat);
			return true;
		}
		return false;
	}
	
	//Retorna els jocs de la base de dades
	public ArrayList<PartidaGuardada> getHidatosCreats(){
		return HidatosCreats;
	}
	
	//Retorna el ranking d'usuaris
	public Ranking mostrarRanking(){
		return rank;
	}
	
	public ArrayList<Pair> GetRankingFacil(){
		return rank.getRankFacil();
	}
	
	public ArrayList<Pair> GetRankingMitja(){
		return rank.getRankMitja();
	}
	
	public ArrayList<Pair> GetRankingDificil(){
		return rank.getRankDificil();
	}
	
	
	public void afegirPuntuacio(int dif, String user, int sec) {
		rank.afegirUser(dif, user, sec);
	}
	
	public void AfegirEntradaFacil(String usuari, int temps){
		rank.AfegirEntradaFacil(usuari, temps);
	}
	
	public void AfegirEntradaMitjana(String usuari, int temps){
		rank.AfegirEntradaMitjana(usuari, temps);
	
	}
	
	public void AfegirEntradaDificil(String usuari, int temps){
		rank.AfegirEntradaDificil(usuari, temps);
	}
	//Registra l'usuari amb els parametres user y passw
	public void registrarUser(String user,String passw){
		this.user =  new Usuari(user,passw);
	}
	private static void printHid(int[][] hid) {
		for (int i = 0; i < hid.length; i++) {
			for (int j = 0; j < hid[0].length; j++) {
				System.out.print(hid[i][j] + " ");
			}
			System.out.println();
		}
	}

}