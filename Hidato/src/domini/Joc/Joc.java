package domini.Joc;

import domini.Hidato.Hidato;
import domini.Hidato.HidatoT;
import domini.Hidato.HidatoQ;
import domini.Hidato.HidatoH;
import domini.Hidato.PartidaGuardada;
import domini.Hidato.Hidato.Topologia;
import domini.Hidato.Hidato.Adjacencia;
import java.util.Random;

/**
 * Joc gestiona l'hidato mentres mante un versio del tauler provisional per tal de poder jugar
 * Conte Un Hidato, un tauler
 * @author Pau Julio Plana
 *
 */
public class Joc {
	public enum Tipus{
		TRIANGLE, QUADRAT, HEXAGON
	}
	private Hidato hidato;
	private int[][] currentHidato;
	private Tipus tipus;
	private Adjacencia adj;
	public Tipus getTipus() {
		return tipus;
	}

	private String nomAutor;
	long startTime, endTime;
	private int tempsGastatGuardat = 0;
	public Joc() {}

	public void resoldreHidato() {
		if (hidato != null) {
			hidato.getTablero_solucionado();
		}
	}
	
	public void startTimer(){
		startTime = endTime = System.nanoTime();
	}
	
	public void endTimer(){
		endTime = System.nanoTime();
	}
	
	public int getTimelapse(){
		startTime = endTime - startTime;
		startTime = startTime / 1000000000;
		return (int)startTime+tempsGastatGuardat;
	}
	
	public void jocPersonalitzat(Adjacencia adj, String nomHidato, Topologia top, int forats, int celes, int plenes, Tipus tipus, String nomAutor){
		this.nomAutor = nomAutor;
		this.tipus = tipus;
		switch (tipus){
		case TRIANGLE:
			hidato = new HidatoT(adj, nomHidato, top, forats, celes, plenes);
			break;
		case QUADRAT:
			hidato = new HidatoQ(adj, nomHidato, top, forats, celes, plenes);
			break;
		case HEXAGON:
			hidato = new HidatoH(adj, nomHidato, top, forats, celes, plenes);
			break;
		}
		if (hidato.generator()) {
			currentHidato = hidato.getTablero();
		}
		else {
			System.out.println("No s'ha pogut crear l'hidato del joc personalitzat");
		}
	}
	
	
	public void crearHidatoNou( Tipus tipus, Topologia top, Adjacencia adj, String dif,String nomA) {

		int numCeles, numForats, numPlenes, rn;
		numCeles = numForats = numPlenes = 0;
		this.tipus = tipus;
		Random rand = new Random();
		//rand.nextInt((max - min) + 1) + min;
		//nextInt retorna un random number entre 0(inclusiu) i lo que hi ha a dins de la funcio(exlcusiu)
		rn = rand.nextInt(3) + 1;
		System.out.println("random: " + rn);
		if (tipus == Tipus.TRIANGLE && adj == Adjacencia.C && rn == 3) {
			rn =2;
		}
		switch (rn) {
		case 1:
		
			numCeles = 9;
			break;
		case 2:
			numCeles = 16;
			break;
		case 3:
			numCeles = 25;
			break;
		}
		
		this.tipus = tipus;
		switch (tipus) {
		case TRIANGLE:
			System.out.println("DIFADFSD: " + dif);

			if (dif.equals("easy")) {
				numForats = 0;
				if (adj == Adjacencia.C) {
					numForats = 3;
				}
				if (adj == Adjacencia.C) {
					numPlenes = (numCeles*5)/10;
					if (numForats < 3 && top == Topologia.TRIANGLE) {
						numForats = 3;
					}
				}
				else numPlenes = (numCeles*6)/10;
				if (numPlenes < 2) {numPlenes = 2;}
		
			//	System.out.println("numplenes: " + numPlenes);
			}
			else if (dif.equals("medium")) {
				numForats = (numCeles*2)/8;
				numPlenes = (numCeles*2)/8;
				if (numPlenes < 2) numPlenes = 2;
				
				if (adj == Adjacencia.C && top == Topologia.TRIANGLE) {
					if (numForats < 3) {
						numForats = 3;
					}
				}
				
			}

			else if (dif.equals("dificil")) {
				System.out.println("plenes:" + numPlenes + " Forats: " + numForats);

				if (adj == Adjacencia.CA)numForats = numCeles/10;
				else numForats = (numCeles*2)/10;
				if (adj == Adjacencia.C  && top == Topologia.TRIANGLE) {
					if (numForats < 3) {
						numForats = 3;
					}
				}
				numPlenes = 2;
			}
			
			System.out.println("plenes:" + numPlenes + " Forats: " + numForats);
			hidato = new HidatoT(adj, "SystemGeneratedHidato", top, numForats, numCeles, numPlenes);
			break;
		case QUADRAT:
			if (dif.equals("easy")) {
				numForats = 0;
				numPlenes = (numCeles*6)/10;
				if (numPlenes < 2) numPlenes =2;
				
			}
			else if (dif.equals("medium")) {
				numForats = (numCeles*2)/8;
				numPlenes = (numCeles*2)/8;
				if (numPlenes < 2) numPlenes =2;
			}
			else if (dif.equals("dificil")) {
				numForats = numCeles/10;
				numPlenes = 2;
			}
			hidato = new HidatoQ(adj, "SystemGeneratedHidato", top, numForats, numCeles, numPlenes);
			break;
		case HEXAGON:
			if (dif.equals("easy")) {
				numForats = 0;
				if (adj == Adjacencia.C) numPlenes = (numCeles*5)/10;
				else numPlenes = (numCeles*6)/10;
				if (numPlenes < 2) numPlenes =2;
				
			}
			else if (dif.equals("medium")) {
				numForats = (numCeles*2)/8;
				numPlenes = (numCeles*2)/8;
				if (numPlenes < 2) numPlenes =2;
			}
			else if (dif.equals("dificil")) {
				if (adj == Adjacencia.CA)numForats = numCeles/10;
				else numForats = (numCeles*2)/10;
				numPlenes = 2;
			}
			hidato = new HidatoH(adj, "SystemGeneratedHidato", top, numForats, numCeles, numPlenes);
			break;
		}
		
		//generar el tauler del hidato i guardarlo al currentHidato
		if (hidato.generator()) {
		currentHidato = hidato.getTablero();
		}
		else {
			System.out.println("No s'ha pogut crear el hidato del joc rapid");
		}
		nomAutor = nomA;
	}
	
	//retorna el hidato actual.
	public int[][] getCurrentHidato() {
		return currentHidato;
	}

	public void setCurrentHidato(int[][] currentHidato) {
		this.currentHidato = currentHidato;
	}

	public PartidaGuardada proposarHidato(int[][] tauler, String id, Adjacencia adjAlt, Topologia topAlt, Tipus tipusAlt, String nomHidatoAlt, String nomAutorAlt) {
		tipus = tipusAlt;
		nomAutor = nomAutorAlt;
		switch(tipusAlt) {
		case TRIANGLE:
			System.out.println("part1");
			for (int i = 0 ; i < tauler.length; i++) {
				

				for (int j = 0 ; j < tauler[0].length; j++) {
					System.out.print(tauler[i][j]);

				}
				System.out.print("\n");
			}
			hidato = new HidatoT(adjAlt, tauler, nomHidatoAlt, topAlt);
			break;
		case QUADRAT:
			System.out.println("part2");

			hidato = new HidatoQ(adjAlt, tauler, nomHidatoAlt, topAlt);
			break;
		case HEXAGON:
			System.out.println("part3");

			hidato = new HidatoH(adjAlt, tauler, nomHidatoAlt, topAlt);
			break;
		}
		if (hidato.solver()) {
			System.out.println("hi ha soluci");
			return new PartidaGuardada(tauler, hidato.getTablero_solucionado(), id, adjAlt, topAlt, tipusAlt,  nomHidatoAlt, nomAutorAlt, 0);
			
		}
		else {
			System.out.println("no hi ha soluci");

			return null;
		}
	}	
	
	public void carregaHidato(PartidaGuardada partida) {
		this.tipus = partida.getTipus();
		switch (tipus) {
		case TRIANGLE:
			hidato = new HidatoT(partida.getAdjacencia(), partida.getTableroOriginal(), partida.getNomHidato(), partida.getTopologia());
			break;
		case QUADRAT:
			hidato = new HidatoQ(partida.getAdjacencia(), partida.getTableroOriginal(), partida.getNomHidato(), partida.getTopologia());
			break;
		case HEXAGON:
			hidato = new HidatoH(partida.getAdjacencia(), partida.getTableroOriginal(), partida.getNomHidato(), partida.getTopologia());
			break;
		}
		currentHidato = partida.getTableroGuardado();
		tempsGastatGuardat = partida.getTempsGastat();
		nomAutor = partida.getNomAutor();
		tipus = partida.getTipus();
		adj = partida.getAdjacencia();
	}
	
	public Adjacencia getAdj() {
		return adj;
	}

	public PartidaGuardada guardarJoc(String id, int tempsGastat) {
		
		PartidaGuardada partida = new PartidaGuardada(hidato.getTablero(), currentHidato, id, hidato.getAdjacencia(), hidato.getTopologia(), tipus,  hidato.getNomHidato(), nomAutor, tempsGastat);
		return partida;
	}
	 
	public void modificarCasella(int x, int y, int value) {
		if (currentHidato[x][y] >= -1 && (value == -1 || value > 0)) currentHidato[x][y] = value;
	}
	
	//Comprova si la solucio donada es la correcta
	public boolean comprovarCorrectesa() {
		hidato.setTablero_solucionado(currentHidato);
		System.out.println("COMPROVAR CORRE: ");
		printHid(currentHidato);
		return hidato.solver();
	}
	
	public int[][] getRes() {
		hidato.setTablero_solucionado(currentHidato);
		System.out.println("COMPROVAR CORRE: ");
		printHid(currentHidato);
		if (hidato.solver()) {
			return hidato.getTablero_solucionado();
		}
		return null;
	}
	
	public int [][] getTaulerSolucionat() {
		return hidato.getTablero_solucionado();
	}
	
	private static void printHid(int[][] hid) {
		for (int i = 0; i < hid.length; i++) {
			for (int j = 0; j < hid[0].length; j++) {
				System.out.print(hid[i][j] + " ");
			}
			System.out.println();
		}
	}
//	public int getValue(int x, int)
	
}
