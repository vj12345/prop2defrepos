package domini.Usuari;

import domini.Hidato.*;
import domini.Joc.Joc.Tipus;

import java.util.*;

public class Usuari {

	private String username;
	private String password;
	private ArrayList<PartidaGuardada> partidesGuardades;
	

	//Constructora 
	public Usuari(String username,String password){
		this.username = username;
		this.password = password;
		partidesGuardades = new ArrayList<PartidaGuardada>();
	}

	
	//Retorna el username
	public String obtindreUsuari(){
		return this.username;
	}
	
	//Retorna el password
	public String obtindrePassword(){
		return this.password;
	}

	//Comprova que el password es correcte
	public boolean ComprovarPassword(String password){
		return this.password.equals(password);
	}
	
	public void changePsw(String newPsw) {
		password = newPsw;
	}
	

	//Retorna les partides guardades per l'usuari
	public ArrayList<PartidaGuardada> getPartidesGuardades() {
		return partidesGuardades;
	}
	
	/*
	 * Afegeix  les partidesGuardades d'un usuari
	 * Pre: L'usuari t� alguna partida creada
	 */
	public void setPartidesGuardades(ArrayList<PartidaGuardada> pg) {
		partidesGuardades = pg;
	}
	
	//Afegeix una partida a la llista de partides guardades
	public void afegirPartida(PartidaGuardada partida){
		partidesGuardades.add(partida);
	}
	


	public void printPartidesGuadades() {
		for (int i = 0; i < partidesGuardades.size(); i++) {
			System.out.println("tablero guardao del hidato con id " + partidesGuardades.get(i).getId() + " :");
			int[][] tab = partidesGuardades.get(i).getTableroGuardado();
			for (int x = 0; x < tab.length; x++) {
				for (int y = 0; y < tab[0].length; y++) {
					System.out.print(tab[x][y] + " ");
				}
				System.out.println("");
			}
			System.out.println("tablero original del guardao del hidato con id " + partidesGuardades.get(i).getId() + " :");
			tab = partidesGuardades.get(i).getTableroOriginal();
			for (int x = 0; x < tab.length; x++) {
				for (int y = 0; y < tab[0].length; y++) {
					System.out.print(tab[x][y] + " ");
				}
				System.out.println("");
			}
			if (partidesGuardades.get(i).getTipus() == Tipus.HEXAGON) {
				System.out.println("tipusclea : Hexaagon");

			}else if (partidesGuardades.get(i).getTipus() == Tipus.QUADRAT) {
				System.out.println("tipusclea : quadrat");

			}else if (partidesGuardades.get(i).getTipus() == Tipus.TRIANGLE) {
				System.out.println("tipusclea : triangle");

			}


		}
		
	}
	
}



