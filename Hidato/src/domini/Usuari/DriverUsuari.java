package domini.Usuari;

import java.util.Scanner; 


public class DriverUsuari {

	public static void main(String[] args) {
		try{
			Scanner linea=new Scanner(System.in);
			Usuari user = null;
			int opcio;
			System.out.println("Benvolgut al Driver d'usuari");
			while(true){
				System.out.println("Selecciona la opci� que vols efectuar amb el numero");
				System.out.println("1 -> Crear Usuari");
				System.out.println("2 -> Obtindre usuari");
				System.out.println("3 -> Obtindre password");
				System.out.println("4 -> Comprovar password");
				opcio = linea.nextInt();
				linea.nextLine();
				if(opcio == 1) {
					String entra_username;
					String entra_password = null;
					System.out.println("Escrigui el seu nom d'usuari");
					entra_username = linea.nextLine();
					System.out.println("Escrigui la seva contrasenya");
					entra_password = linea.nextLine();
					user = new Usuari(entra_username,entra_password);
					System.out.println("El registre s'ha completat exitosament");
				}
				if(opcio == 2) {
					System.out.println("El teu usuari es " + user.obtindreUsuari());
				}
				if(opcio == 3) {
					System.out.println("La teva contrasenya es " + user.obtindrePassword());
				}
				if(opcio == 4) {
					String entra_password;
					System.out.println("Escrigui la contrasenya del usuari que has creat");
					entra_password = linea.nextLine();
					boolean es_correcte = user.ComprovarPassword(entra_password);
					if (es_correcte){
						System.out.println("La contrasenya es correcta");
					}
					else {
						System.out.println("La contrasenya es  incorrecta");
					}
				}
			}
		}
		catch (Exception e) {
			System.out.println("Hi ha hagut una excepci�");
		}
	}
}
